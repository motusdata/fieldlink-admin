$(document).ready(function () {
	//adjust height
	$main = $("main");
	$sideMenu = $(".side-menu");
	$header = $("header");
	$rowHeader = $(".row-header");

	adjustContent();
	$main.show();
	$sideMenu.show();

	//side-menu
	$(".sub-menu-button").on("click", function (event) {
		event.preventDefault();
		var target = $(this).closest('li').children('ul');

		target.slideToggle();
		$(this).children("i").toggleClass("fa-plus fa-minus");
	});

	$(".side-menu-button").on("click", function (event) {
		var target = $(".side-menu");
		target.toggleClass("side-menu-show");
		$(this).toggleClass("side-menu-button-move");
	});

	//btn-close
	$("main").on("click", ".btn-close", function () {
		$(this).parent().fadeOut();
	});

	setInterval(function () {
		var $alertList = $(".alert");
		if ($alertList.length != 0) {
			setTimeout(function () {
				$alertList.fadeOut({
					done: function () {
						$alertList.remove();
					}
				});

			}, 6000);
		}
	}, 3000);

	//pagination
	$("ul.pagination li.disabled").on("click", function () {
		return false;
	});

	//header
	$topMenuButton = $(".top-menu-button");
	$(".row-header > .col:last-child").on("dropped", function () {
		$topMenuButton.show();
		adjustContent();
	});

	$(".row-header > .col:last-child").on("risen", function () {
		$topMenuButton.hide();
		adjustContent();
	});

	function adjustContent() {
		$main.css("top", $header.height() + "px");
		$sideMenu.css("top", $header.height() + "px");
	}

	$topMenuButton.on("click", function () {
		$rowHeader.toggleClass("auto-height");
		adjustContent();
	});

	var flex = new $.FlexWatch();
	flex.addWatch(".row-header > .col:last-child", ".site-title");
	flex.startWatching();
});

function showAjaxMsg(message) {
	if (message != undefined) {
		$("main").prepend(message);
	}
}