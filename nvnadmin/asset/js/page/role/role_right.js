$(document).ready(function() {
	$(".select-all").on("click", function(event) {
		event.stopPropagation();
		$(this).closest("table").find(":checkbox").prop("checked", true);
	});

	$(".select-none").on("click", function(event) {
		event.stopPropagation();
		$(this).closest("table").find(":checkbox").prop("checked", false);
	});

	$(".select-reverse").on("click", function(event) {
		event.stopPropagation();
		var list = $(this).closest("table").find(":checkbox");
		list.each(function(i) {
			this.checked = !this.checked
		});
	});


	var $tableBody = $("table tbody");
	var $selectAll = $(".select-all");
	var $selectNone = $(".select-none");
	var $selectReverse = $(".select-reverse");

	var $expandAll = $(".expand-all");
	var $collapseAll = $(".collapse-all");

	$collapseAll.on("click", function() {
		$tableBody.fadeOut("fast");
		$selectAll.hide();
		$selectNone.hide();
		$selectReverse.hide();
	});

	$expandAll.on("click", function() {
		$tableBody.fadeIn("fast");
		$selectAll.show();
		$selectNone.show();
		$selectReverse.show();
	});

	$("tr.role-header").on("click", function() {
		$(this).closest("table").find("tbody").fadeToggle("fast");
		$(this).find(".select-all").toggle();
		$(this).find(".select-none").toggle();
		$(this).find(".select-reverse").toggle();
	});

	$collapseAll.trigger("click");
});
