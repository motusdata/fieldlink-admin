$(document).ready(function() {
	$("#operatorId").on("change", function(event) {
		$("#operatorForm").submit();
	});
	
	$("#countyId").on("change", function(event) {
		$("#countyForm").submit();
	});
	
	$("#wellTypeId").on("change", function(event) {
		$("#wellTypeForm").submit();
	});

	$("#status").on("change", function(event) {
		$("#statusForm").submit();
	});
});