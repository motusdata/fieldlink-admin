module nvnadmin

go 1.14

require (
	github.com/dchest/captcha v0.0.0-20170622155422-6a29415a8364
	github.com/fsnotify/fsnotify v1.4.9
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/websocket v1.4.2
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/nats-io/nats.go v1.10.0
)
