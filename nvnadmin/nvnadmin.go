package main

import (
	"fmt"
	"log"
	"net/http"

	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/lib/watch"
	"nvnadmin/src/page/change_user"
	"nvnadmin/src/page/config"
	"nvnadmin/src/page/county"
	"nvnadmin/src/page/dashboard"
	"nvnadmin/src/page/error_log"
	"nvnadmin/src/page/gateway"
	"nvnadmin/src/page/gateway_mnf"
	"nvnadmin/src/page/login"
	"nvnadmin/src/page/logout"
	"nvnadmin/src/page/operator"
	"nvnadmin/src/page/profile"
	"nvnadmin/src/page/role"
	"nvnadmin/src/page/sensor"
	"nvnadmin/src/page/sensor_mnf"
	"nvnadmin/src/page/sensor_monit"
	"nvnadmin/src/page/sensor_type"
	"nvnadmin/src/page/user"
	"nvnadmin/src/page/welcome"
	"nvnadmin/src/page/well"
	"nvnadmin/src/page/well_type"

	"runtime/debug"
	"strings"
	"time"

	"github.com/dchest/captcha"
)

type HandleFuncType func(http.ResponseWriter, *http.Request)

func main() {
	chron()

	if app.Ini.Debug {
		watch.WatchLess()
	}

	//static constent
	fsAdmin := http.FileServer(http.Dir(app.Ini.HomeDir + "/asset/"))
	http.Handle("/asset/", http.StripPrefix("/asset/", fsAdmin))

	http.Handle("/captcha/", captcha.Server(200, 90))

	//admin
	http.HandleFunc("/change_user", makeHandle(change_user.Browse))

	http.HandleFunc("/role", makeHandle(role.Browse))
	http.HandleFunc("/role_insert", makeHandle(role.Insert))
	http.HandleFunc("/role_update", makeHandle(role.Update))
	http.HandleFunc("/role_delete", makeHandle(role.Delete))
	http.HandleFunc("/role_right", makeHandle(role.RoleRight))

	http.HandleFunc("/user", makeHandle(user.Browse))
	http.HandleFunc("/user_insert", makeHandle(user.Insert))
	http.HandleFunc("/user_update", makeHandle(user.Update))
	http.HandleFunc("/user_confirm", makeHandle(user.Confirm))
	http.HandleFunc("/user_update_pass", makeHandle(user.UpdatePass))
	http.HandleFunc("/user_delete", makeHandle(user.Delete))
	http.HandleFunc("/user_role", makeHandle(user.UserRole))
	http.HandleFunc("/user_role_revoke", makeHandle(user.UserRoleRevoke))
	http.HandleFunc("/user_role_grant", makeHandle(user.UserRoleGrant))

	http.HandleFunc("/config", makeHandle(config.Browse))
	http.HandleFunc("/config_insert", makeHandle(config.Insert))
	http.HandleFunc("/config_update", makeHandle(config.Update))
	http.HandleFunc("/config_delete", makeHandle(config.Delete))
	http.HandleFunc("/config_set", makeHandle(config.Set))

	http.HandleFunc("/login", makeHandle(login.Browse))
	http.HandleFunc("/welcome", makeHandle(welcome.Browse))
	http.HandleFunc("/logout", makeHandle(logout.Browse))

	http.HandleFunc("/profile", makeHandle(profile.Browse))
	http.HandleFunc("/profile_update", makeHandle(profile.Update))
	http.HandleFunc("/profile_update_pass", makeHandle(profile.UpdatePass))

	http.HandleFunc("/operator", makeHandle(operator.Browse))
	http.HandleFunc("/operator_insert", makeHandle(operator.Insert))
	http.HandleFunc("/operator_update", makeHandle(operator.Update))
	http.HandleFunc("/operator_delete", makeHandle(operator.Delete))

	http.HandleFunc("/county", makeHandle(county.Browse))
	http.HandleFunc("/county_insert", makeHandle(county.Insert))
	http.HandleFunc("/county_update", makeHandle(county.Update))
	http.HandleFunc("/county_delete", makeHandle(county.Delete))

	http.HandleFunc("/well_type", makeHandle(well_type.Browse))
	http.HandleFunc("/well_type_insert", makeHandle(well_type.Insert))
	http.HandleFunc("/well_type_update", makeHandle(well_type.Update))
	http.HandleFunc("/well_type_delete", makeHandle(well_type.Delete))

	http.HandleFunc("/well", makeHandle(well.Browse))
	http.HandleFunc("/well_insert", makeHandle(well.Insert))
	http.HandleFunc("/well_display", makeHandle(well.Display))
	http.HandleFunc("/well_update", makeHandle(well.Update))
	http.HandleFunc("/well_delete", makeHandle(well.Delete))

	http.HandleFunc("/gateway_mnf", makeHandle(gateway_mnf.Browse))
	http.HandleFunc("/gateway_mnf_insert", makeHandle(gateway_mnf.Insert))
	http.HandleFunc("/gateway_mnf_update", makeHandle(gateway_mnf.Update))
	http.HandleFunc("/gateway_mnf_delete", makeHandle(gateway_mnf.Delete))

	http.HandleFunc("/gateway", makeHandle(gateway.Browse))
	http.HandleFunc("/gateway_insert", makeHandle(gateway.Insert))
	http.HandleFunc("/gateway_update", makeHandle(gateway.Update))
	http.HandleFunc("/gateway_delete", makeHandle(gateway.Delete))

	http.HandleFunc("/sensor_mnf", makeHandle(sensor_mnf.Browse))
	http.HandleFunc("/sensor_mnf_insert", makeHandle(sensor_mnf.Insert))
	http.HandleFunc("/sensor_mnf_update", makeHandle(sensor_mnf.Update))
	http.HandleFunc("/sensor_mnf_delete", makeHandle(sensor_mnf.Delete))

	http.HandleFunc("/sensor_type", makeHandle(sensor_type.Browse))
	http.HandleFunc("/sensor_type_insert", makeHandle(sensor_type.Insert))
	http.HandleFunc("/sensor_type_update", makeHandle(sensor_type.Update))
	http.HandleFunc("/sensor_type_delete", makeHandle(sensor_type.Delete))

	http.HandleFunc("/sensor", makeHandle(sensor.Browse))
	http.HandleFunc("/sensor_monit", makeHandle(sensor_monit.Browse))
	http.HandleFunc("/dashboard", makeHandle(dashboard.Browse))
	http.HandleFunc("/error_log", makeHandle(error_log.Browse))

	http.HandleFunc("/", makeHandle(user.Browse))

	srv := &http.Server{
		Addr:         app.Ini.Host + ":" + app.Ini.Port,
		ReadTimeout:  7 * time.Second,
		WriteTimeout: 7 * time.Second,
	}

	fmt.Printf("starting nvnadmin server (v:1.0) on port: %s\n", app.Ini.Port)
	log.Printf("starting nvnadmin server (v:1.0) on port: %s\n", app.Ini.Port)

	var err error
	if app.Ini.UseSsl {
		err = srv.ListenAndServeTLS(app.Ini.PublicKey, app.Ini.PrivateKey)
	} else {
		err = srv.ListenAndServe()
	}

	if err != nil {
		println(err.Error())
		log.Println(err.Error())
	}
}

func makeHandle(hf HandleFuncType) HandleFuncType {
	return func(rw http.ResponseWriter, req *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				switch err := err.(type) {
				case string:
					fatalError(rw, req, err)
				case error:
					fatalError(rw, req, err.Error())
				case *app.BadRequestError:
					badRequest(rw, req)
				case *app.NotFoundError:
					notFound(rw, req)
				default:
					log.Printf("%s %v\n", "unknown error", err)
				}
			}
		}()

		if app.Ini.Cache {
			rw.Header().Add("Cache-Control", "public, max-age=3600, must-revalidate")
		} else {
			rw.Header().Add("Cache-Control", "no-cache, no-store, must-revalidate")
		}

		hf(rw, req)
	}
}

func fatalError(rw http.ResponseWriter, req *http.Request, msg string) {
	if req.Header.Get("X-Requested-With") != "" {
		list := strings.Split(string(debug.Stack()[:]), "\n")
		errorMsg := fmt.Sprintf("Fatal Error: %s", msg)

		if app.Ini.Debug {
			errorMsg += "\n" + strings.Join(list[6:], "\n")
		}

		http.Error(rw, errorMsg, 500)
		return
	}

	buf := new(util.Buf)

	buf.Add("<!DOCTYPE html>")
	buf.Add("<html lang=\"en\">")

	buf.Add("<head>")
	buf.Add("<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">")
	buf.Add("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">")
	buf.Add("<link rel=\"icon\" href=\"/asset/img/favicon.png\">")
	buf.Add("<link rel=\"stylesheet\" href=\"/asset/css/style.css\">")
	buf.Add("<title>Fatal Error</title>")
	buf.Add("</head>")

	buf.Add("<body id=\"end\">")

	buf.Add("<main>")
	buf.Add("<div class=\"row\">")

	buf.Add("<div class=\"col sm1 md2 lg3\"></div>")

	buf.Add("<div class=\"col sm10 md8 lg6\">")

	buf.Add("<div class=\"panel panel-danger\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add("<div class=\"panel-title\">Fatal Error</div>")
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")
	buf.Add("<p>%s</p>", msg)

	list := strings.Split(string(debug.Stack()[:]), "\n")
	buf.Add("<p>%s</p>", strings.Join(list[6:], "<br>"))
	buf.Add("</div>")

	buf.Add("</div>") //end of panel
	buf.Add("</div>") //end of col

	buf.Add("<div class=\"col sm1 md2 lg3\"></div>")
	buf.Add("</div>") //end of row

	buf.Add("</main>")

	buf.Add("</body>")
	buf.Add("<script src=\"/asset/js/jquery-3.2.1.js\"></script>")
	buf.Add("<script src=\"/asset/js/common.js\"></script>")
	buf.Add("</html>")

	fmt.Fprintf(rw, *buf.String())
}

func badRequest(rw http.ResponseWriter, req *http.Request) {
	if req.Header.Get("X-Requested-With") != "" {
		http.Error(rw, "Bad request: You don't have right to access this site.", 403)
		return
	}

	ctx := context.NewContext(rw, req)

	ctx.Rw.WriteHeader(http.StatusBadRequest)

	buf := util.NewBuf()
	buf.Add("<div class=\"callout callout-danger\">")
	buf.Add("<h3>Bad request</h3>")
	buf.Add("<p>You don't have right to access this site.</p>")
	buf.Add("<p>Please <a href=\"/login\">Login</a> first.</p>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	str := "end"
	ctx.AddFormat("pageId", &str)

	content.Include(ctx)
	content.Default(ctx)

	ctx.Render("end.html")
}

func notFound(rw http.ResponseWriter, req *http.Request) {
	if req.Header.Get("X-Requested-With") != "" {
		rw.WriteHeader(http.StatusNotFound)
		rw.Header().Set("Content-Type", "application/json")
		rw.Write([]byte("404 Not found."))
		return
	}

	ctx := context.NewContext(rw, req)

	ctx.Rw.WriteHeader(http.StatusNotFound)

	buf := util.NewBuf()
	buf.Add("<div class=\"callout callout-danger\">")
	buf.Add("<h3>404 Not Found.</h3>")
	buf.Add("<p>The page you requested '%s', does not exist.", req.URL.RequestURI())
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())
	str := "end"
	ctx.AddFormat("pageId", &str)

	content.Include(ctx)
	content.Default(ctx)

	ctx.Render("end.html")
}

func chron() {
	//delete expired session data.
	go func() {
		c := time.Tick(time.Duration(app.Ini.CookieExpires) * time.Second)
		for _ = range c {
			now := time.Now()
			epoch := now.Unix()

			tx, err := app.Db.Begin()
			if err != nil {
				panic(err)
			}

			sqlStr := fmt.Sprintf("delete from session where (? - expires) > %d", app.Ini.CookieExpires)
			_, err = tx.Exec(sqlStr, epoch)
			if err != nil {
				tx.Rollback()
				log.Printf("%s %s", "error while deleting expired sessions:", err.Error())
				return
			}

			tx.Commit()
		}
	}()

	//delete redundant node statistics.
	go func() {
		c := time.Tick(time.Duration(1) * time.Minute)
		for _ = range c {
			now := time.Now()
			epoch := now.Unix()

			tx, err := app.Db.Begin()
			if err != nil {
				panic(err)
			}

			sqlStr := fmt.Sprintf("delete from nodeStat where (? - recordTime) > %d", 1000)
			_, err = tx.Exec(sqlStr, epoch)
			if err != nil {
				tx.Rollback()
				log.Printf("%s %s", "error while deleting expired sessions:", err.Error())
				return
			}

			tx.Commit()
		}
	}()
}
