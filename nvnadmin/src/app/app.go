package app

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"regexp"
	"runtime"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/websocket"
	"github.com/natefinch/lumberjack"
)

type BadRequestError error
type NotFoundError error

func BadRequest() {
	panic(new(BadRequestError))
}

func NotFound() {
	panic(new(NotFoundError))
}

var Debug = true
var SiteName = "nvn maestro"

var Tmpl *template.Template
var Db *sql.DB

var TranRe *regexp.Regexp //regular expression for translation

var SocketList socketList

func init() {
	if !Debug {
		totalCpu := runtime.NumCPU()

		if totalCpu > 1 {
			totalCpu--
		}

		runtime.GOMAXPROCS(totalCpu)
	}

	readIni()
	connect()
	ReadConfig()

	log.SetOutput(&lumberjack.Logger{
		Filename:   Ini.HomeDir + "/logs/error.log",
		MaxSize:    50, // megabytes
		MaxBackups: 6,
		MaxAge:     28, //days
	})

	funcmap := template.FuncMap{
		"raw": func(str string) template.HTML {
			return template.HTML(str)
		},
	}

	//config dosyasi kullanarak bu dizini disari tasi.
	Tmpl = template.Must(template.New("").Funcs(funcmap).ParseGlob(Ini.HomeDir + "/view/*.html"))

	//socket
	SocketList = NewSocketList()
}

func connect() {
	conStr := fmt.Sprintf("%s:%s@%s(%s:%d)/%s?autocommit=false",
		Ini.DbUser, Ini.DbPassword, Ini.DbProtocol,
		Ini.DbHost, Ini.DbPort, Ini.DbName)

	db, err := sql.Open("mysql", conStr)
	if err != nil {
		panic("Could not connect to database.")
	}

	db.SetMaxIdleConns(0)
	Db = db
}

//node management
func deleteNodeStat() {
	tx, err := Db.Begin()
	if err != nil {
		log.Printf(err.Error())
		panic(err)
	}

	sqlStr := `delete from
					nodeStat`

	_, err = tx.Exec(sqlStr)
	if err != nil {
		tx.Rollback()
		log.Printf(err.Error())
		panic(err)
	}

	tx.Commit()
}

type nodeRec struct {
	NodeId int64
	Host   string
	Port   string
	Status string
}

func getAllNodeList() []*nodeRec {
	sqlStr := `select
					node.nodeId,
					node.host,
					node.port,
					node.status
				from
					node
				where
					status != 'blocked'`

	rows, err := Db.Query(sqlStr)
	if err != nil {
		log.Printf(err.Error())
		panic(err)
	}

	defer rows.Close()

	rv := make([]*nodeRec, 0, 100)
	for rows.Next() {
		rec := new(nodeRec)
		if err = rows.Scan(&rec.NodeId, &rec.Host, &rec.Port, &rec.Status); err != nil {
			log.Printf(err.Error())
			panic(err)
		}

		rv = append(rv, rec)
	}

	return rv
}

func singleJoiningSlash(a, b string) string {
	aslash := strings.HasSuffix(a, "/")
	bslash := strings.HasPrefix(b, "/")
	switch {
	case aslash && bslash:
		return a + b[1:]
	case !aslash && !bslash:
		return a + "/" + b
	}
	return a + b
}

//socket
type socketList map[string]*websocket.Conn

func NewSocketList() map[string]*websocket.Conn {
	return make(map[string]*websocket.Conn, 50)
}

func (sl socketList) Add(sessionId string, con *websocket.Conn) {
	sl[sessionId] = con
}

func (sl socketList) SendRefresh(sessionId string) {
	v, ok := sl[sessionId]

	if !ok {
		println("can not find socket connection: " + sessionId)
	}

	err := v.WriteMessage(websocket.TextMessage, []byte("refresh"))
	if err != nil {
		println(err.Error())
	}

	//there must be a way to delete broken connections here.
}
