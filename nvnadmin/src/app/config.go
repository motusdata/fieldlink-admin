package app

import (
	"strconv"
)

type ConfigType struct {
	PageLen      int64
	RulerLen     int64
	Lang         string
	SmtpHost     string
	SmtpPort     int64
	SmtpUser     string
	SmtpPassword string
}

type ConfigRec struct {
	ColumnId int64
	Enum     int64
	Name     string
	VarType  string
	Value    string
	Title    string
	Exp      string
}

var Config = new(ConfigType)

func ReadConfig() {
	sqlStr := "select configId, enum, name, type, value, title, exp from config"
	rows, err := Db.Query(sqlStr)
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	rec := new(ConfigRec)
	for rows.Next() {
		err = rows.Scan(&rec.ColumnId, &rec.Enum, &rec.Name, &rec.VarType, &rec.Value,
			&rec.Title, &rec.Exp)
		if err != nil {
			panic(err)
		}

		switch rec.Name {
		case "pageLen":
			num, err := strconv.ParseInt(rec.Value, 10, 64)
			if err != nil {
				panic(err)
			}

			Config.PageLen = num
		case "rulerLen":
			num, err := strconv.ParseInt(rec.Value, 10, 64)
			if err != nil {
				panic(err)
			}

			Config.RulerLen = num
		case "lang":
			Config.Lang = rec.Value
		case "smtpHost":
			Config.SmtpHost = rec.Value
		case "smtpPort":
			num, err := strconv.ParseInt(rec.Value, 10, 64)
			if err != nil {
				panic(err)
			}

			Config.SmtpPort = num
		case "smtpUser":
			Config.SmtpUser = rec.Value
		case "smtpPassword":
			Config.SmtpPassword = rec.Value
		default:
			panic("Unknown config variable: " + rec.Name)
		}
	}
}

func CopyConfig() *ConfigType {
	rv := *Config

	return &rv
}
