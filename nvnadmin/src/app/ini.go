package app

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

type ini struct {
	HomeDir string
	BaseUrl string
	Host    string
	Port    string

	DbProtocol string
	DbHost     string
	DbPort     int64
	DbName     string
	DbUser     string
	DbPassword string

	CookieName     string
	CookiePath     string
	CookieDomain   string
	CookieExpires  int64
	CookieSecure   bool
	CookieHttpOnly bool

	UseSsl     bool
	PrivateKey string
	PublicKey  string

	Cache bool
	Debug bool
}

var Ini = new(ini)

func readIni() {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		panic(err)
	}
	fmt.Println(dir)
	file := "/Users/jtorres/Documents/Noven/repos/fieldlink-admin/nvnadmin/nvnadmin/nvnadmin.ini"
	//file := dir + "/nvnadmin.ini"

	buf, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
		os.Exit(1)
	}

	err = json.Unmarshal(buf, Ini)
	if err != nil {
		panic(err)
		os.Exit(1)
	}
}
