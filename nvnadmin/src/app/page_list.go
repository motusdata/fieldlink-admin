package app

type right struct {
	Name string
	Exp  string
}

type rightList []*right

func NewRightList() rightList {
	return make(rightList, 0, 5)
}

func (rl *rightList) Add(name, exp string) {
	r := new(right)
	r.Name = name
	r.Exp = exp

	*rl = append(*rl, r)
}

type page struct {
	Name      string
	Title     string
	RightList []*right
}

func NewPage(name, title string, rl rightList) *page {
	rv := new(page)
	rv.Name = name
	rv.Title = title
	rv.RightList = rl

	return rv
}

var PageList []*page

func init() {
	PageList = make([]*page, 0, 50)

	//user
	rl := NewRightList()
	rl.Add("browse", "Can browse 'user' records.")
	rl.Add("insert", "Can insert new 'user' record.")
	rl.Add("update", "Can update 'user' records.")
	rl.Add("confirm", "Can confirm new 'user' accounts.")
	rl.Add("update_pass", "Can update 'user' passowrd.")
	rl.Add("delete", "Can delete 'user' records.")
	rl.Add("role_browse", "Can browse 'user' roles.")
	rl.Add("role_revoke", "Can revoke 'user' roles.")
	rl.Add("role_grant", "Can grant 'user' roles.")
	pg := NewPage("user", "Users", rl)
	PageList = append(PageList, pg)

	//role
	rl = NewRightList()
	rl.Add("browse", "Can browse 'role' records.")
	rl.Add("insert", "Can insert new 'role' record.")
	rl.Add("update", "Can update 'role' records.")
	rl.Add("delete", "Can delete 'role' records.")
	rl.Add("role_right", "Can update 'role' rights.")
	pg = NewPage("role", "Roles", rl)
	PageList = append(PageList, pg)

	//config
	rl = NewRightList()
	rl.Add("browse", "Can browse 'configuration' records.")
	rl.Add("set", "Can set 'configuration' record value.")
	pg = NewPage("config", "Configuration", rl)
	PageList = append(PageList, pg)

	//user profile
	rl = NewRightList()
	rl.Add("browse", "Can browse own 'profile' record.")
	rl.Add("update", "Can update own 'profile' record.")
	rl.Add("update_password", "Can change own password.")
	pg = NewPage("profile", "User Profile", rl)
	PageList = append(PageList, pg)

	//customer
	rl = NewRightList()
	rl.Add("browse", "Can browse 'customer' records.")
	rl.Add("insert", "Can insert new 'customer' record.")
	rl.Add("update", "Can update 'customer' records.")
	rl.Add("delete", "Can delete 'customer' records.")
	pg = NewPage("customer", "Customers", rl)
	PageList = append(PageList, pg)

	//fields
	rl = NewRightList()
	rl.Add("browse", "Can browse 'field' records.")
	rl.Add("insert", "Can insert new 'field' record.")
	rl.Add("update", "Can update 'field' records.")
	rl.Add("delete", "Can delete 'field' records.")
	pg = NewPage("field", "Fields", rl)
	PageList = append(PageList, pg)

	//assets
	rl = NewRightList()
	rl.Add("browse", "Can browse 'asset' records.")
	rl.Add("insert", "Can insert new 'asset' record.")
	rl.Add("update", "Can update 'asset' records.")
	rl.Add("delete", "Can delete 'asset' records.")
	pg = NewPage("asset", "Assets", rl)
	PageList = append(PageList, pg)

	//gateway
	rl = NewRightList()
	rl.Add("browse", "Can browse 'gateway' records.")
	rl.Add("insert", "Can insert new 'gateway' record.")
	rl.Add("update", "Can update 'gateway' records.")
	rl.Add("delete", "Can delete 'gateway' records.")
	pg = NewPage("gateway", "Gateway", rl)
	PageList = append(PageList, pg)

	//sensors
	rl = NewRightList()
	rl.Add("browse", "Can browse 'sensor' records.")
	rl.Add("insert", "Can insert new 'sensor' record.")
	rl.Add("update", "Can update 'sensor' records.")
	rl.Add("delete", "Can delete 'sensor' records.")
	pg = NewPage("sensor", "Assets", rl)
	PageList = append(PageList, pg)

	//dashboard
	rl = NewRightList()
	rl.Add("browse", "Can browse 'dashboard' records.")
	rl.Add("insert", "Can insert new 'dashboard' record.")
	rl.Add("update", "Can update 'dashboard' records.")
	rl.Add("delete", "Can delete 'dashboard' records.")
	pg = NewPage("dashboard", "Dashboard", rl)
	PageList = append(PageList, pg)

	//error log
	rl = NewRightList()
	rl.Add("browse", "Can browse 'error log' records.")
	rl.Add("delete", "Can delete 'error log' records.")
	rl.Add("delete_all", "Can delete all 'error log' records.")
	pg = NewPage("error log", "Error Log", rl)
	PageList = append(PageList, pg)
}
