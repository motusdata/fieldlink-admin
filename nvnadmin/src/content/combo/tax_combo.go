package combo

import (
	"nvnadmin/src/app"
	"nvnadmin/src/lib/tax"
	"nvnadmin/src/lib/util"
	"strconv"
)

type taxRec struct {
	name      int64
	parent    int64
	label     string
	sortOrder int64
	isAdded   bool
}

type taxComboItem struct {
	key   int64
	label string
}

type taxCombo struct {
	sqlStr string
	tax    *tax.Tax
}

func NewTaxCombo(sqlStr, defaultOption string) *taxCombo {
	rv := new(taxCombo)
	rv.tax = tax.New()
	rv.add("0", "end", 0, true, defaultOption)

	rv.sqlStr = sqlStr

	return rv
}

func (tc *taxCombo) add(name, parent string, order int64, isVisible bool, label string) {
	nameInt, err := strconv.ParseInt(name, 10, 64)
	if err != nil {
		panic(err)
	}

	tc.tax.Add(name, parent, order, isVisible, &taxComboItem{nameInt, label})
}

func (tc *taxCombo) IsEmpty() bool {
	return tc.tax.IsEmpty()
}

func (tc *taxCombo) Set() {
	rows, err := app.Db.Query(tc.sqlStr)
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	list := make(map[int64]*taxRec)

	for rows.Next() {
		rec := new(taxRec)
		if err = rows.Scan(&rec.name, &rec.parent, &rec.label, &rec.sortOrder); err != nil {
			panic(err)
		}

		if rec.name != 0 {
			list[rec.name] = rec
		}
	}

	for k, _ := range list {
		tc.addItemToTax(list, k)
	}

	tc.tax.SortChildren()
}

func (tc *taxCombo) addItemToTax(list map[int64]*taxRec, item int64) {

	if list[item].isAdded {
		return
	}

	if item == 0 {
		return
	}

	parent := strconv.FormatInt(list[item].parent, 10)

	if !tc.tax.IsExists(parent) {
		tc.addItemToTax(list, list[item].parent)
	}

	name := strconv.FormatInt(list[item].name, 10)

	tc.add(name, parent, list[item].sortOrder, true, list[item].label)
	list[item].isAdded = true
}

func (tc *taxCombo) Format(curKey int64) string {
	buf := util.NewBuf()
	isLast := make([]bool, 0, 10)
	isLast = append(isLast, true)

	tc.FormatItem("0", curKey, buf, isLast)

	return *buf.String()
}

func (tc *taxCombo) FormatItem(name string, curKey int64, buf *util.Buf, isLast []bool) {
	item := tc.tax.GetItem(name)
	data := item.Data.(*taxComboItem)

	var classStr = ""
	if data.key == curKey {
		classStr = " selected"
	}

	buf.Add("<option value=\"%s\"%s>", name, classStr)

	str := ""
	length := len(isLast)
	for i := 1; i < length; i++ {
		if isLast[i] && i < length-1 {
			str += "&nbsp;&nbsp;"
		} else if isLast[i] {
			str += "&nbsp;&#x2514;"
		} else if i == length-1 {
			str += "&nbsp;&#x251c;"
		} else {
			str += "&nbsp;&#x2502;"
		}
	}

	buf.Add(str + data.label)
	buf.Add("</option>")

	count := 1

	if tc.tax.IsParent(name) {
		isLast = append(isLast, false)

		children := tc.tax.GetChildren(name)
		length = len(children)

		for _, v := range children {
			if count == length {
				isLast[len(isLast)-1] = true
			}

			count++

			tc.FormatItem(v, curKey, buf, isLast)
		}

		isLast = isLast[0 : len(isLast)-1]
	}
}
