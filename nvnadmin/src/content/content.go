package content

import (
	"fmt"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"strings"
)

func Include(ctx *context.Ctx) {
	ctx.Include.Css("/asset/fontawesome/css/all.min.css")
	ctx.Include.Css("/asset/css/style.css")

	ctx.Include.Js("/asset/js/jquery-3.2.1.js")
	ctx.Include.Js("/asset/js/flex_watch.js")
	ctx.Include.Js("/asset/js/common.js")
}

func Default(ctx *context.Ctx) {
	HtmlTitle(ctx, "noven admin panel (internal)")
	SiteTitle(ctx)
	Footer(ctx)
}

func HtmlTitle(ctx *context.Ctx, str string) {
	ctx.AddFormat("htmlTitle", &str)
}

func SiteTitle(ctx *context.Ctx) {
	str := fmt.Sprintf("<div class=\"site-title\"><h1><span>Noven</span> Admin Portal (Internal)</h1></div>")
	ctx.AddFormat("siteTitle", &str)
}

func Footer(ctx *context.Ctx) {
	str := "<div class=\"footer\"><p>Provided by Fieldlink Systems &copy;<p></div>"
	ctx.AddFormat("footer", &str)
}

func Search(ctx *context.Ctx, urlStr string, args ...string) {
	buf := util.NewBuf()

	key := ctx.Cargo.Str("key")
	buf.Add("<form class=\"form-search\" action=\"%s\" method=\"get\">", urlStr)
	buf.Add("<input type=\"text\" name=\"key\" placeholder=\"%s\" value=\"%s\">", "search word...", key)
	buf.Add(HiddenCargo(ctx, args...))
	buf.Add("<button type=\"submit\" class=\"btn btn-primary btn-md\">%s</button>", "Search")
	buf.Add("</form>")

	ctx.AddFormat("search", buf.String())
}

func HiddenCargo(ctx *context.Ctx, args ...string) string {
	buf := util.NewBuf()
	var val string

	for _, name := range args {
		if !ctx.Cargo.IsDefault(name) {
			val = ctx.Cargo.Str(name)
			buf.Add("<input type=\"hidden\" name=\"%s\" value=\"%s\">", name, val)
		}
	}

	return *buf.String()
}

func Find(str, key string) string {
	return strings.Replace(str, key, fmt.Sprintf("<span class=\"find\">%s</span>", key), -1)
}

func PanelTitle(args ...string) string {
	return fmt.Sprintf("<div class=\"panel-title\">%s</div>",
		strings.Join(args, " <i class=\"fa fa-angle-right\"></i> "))
}

func End(ctx *context.Ctx, panelType, panelTitle, htmlTitle string, msg ...string) {
	buf := util.NewBuf()

	buf.Add("<div class=\"callout callout-%s\">", panelType)
	buf.Add("<h3>%s</h3>", panelTitle)

	for _, v := range msg {
		buf.Add("<p>%s</p>", v)
	}

	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	Include(ctx)

	SiteTitle(ctx)
	HtmlTitle(ctx, htmlTitle)
	Footer(ctx)

	str := "end"
	ctx.AddFormat("pageId", &str)

	ctx.Render("end.html")
}
