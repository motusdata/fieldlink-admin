package include

import (
	"nvnadmin/src/lib/util"
)

type Include struct {
	css []string
	js  []string
}

func NewInclude() *Include {
	rv := new(Include)
	rv.css = make([]string, 0, 10)
	rv.js = make([]string, 0, 10)

	return rv
}

func (inc *Include) Css(urlStr string) {
	inc.css = append(inc.css, urlStr)
}

func (inc *Include) FormatCss() *string {
	buf := util.NewBuf()

	for _, val := range inc.css {
		buf.Add("<link rel=\"stylesheet\" href=\"%s\">", val)
	}

	return buf.String()
}

func (inc *Include) Js(urlStr string) {
	inc.js = append(inc.js, urlStr)
}

func (inc *Include) FormatJs() *string {
	buf := util.NewBuf()

	for _, val := range inc.js {
		buf.Add("<script src=\"%s\"></script>", val)
	}

	return buf.String()
}
