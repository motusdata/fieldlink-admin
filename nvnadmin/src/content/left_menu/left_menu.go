package left_menu

import (
	"fmt"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/tax"
	"nvnadmin/src/lib/util"
	"strings"
)

type leftMenuItem struct {
	url           string
	label         string
	icon          string
	isActive      bool
	isSubmenuOpen bool
}

type leftMenu struct {
	tax *tax.Tax
}

func New() *leftMenu {
	rv := new(leftMenu)
	rv.tax = tax.New()
	rv.Add("root", "end", 0, true, "", "", "")

	return rv
}

func (lm *leftMenu) Add(name, parent string, order int64, isVisible bool, url, label, icon string) {
	lm.tax.Add(name, parent, order, isVisible, &leftMenuItem{url, label, icon, false, false})
}

func (lm *leftMenu) Set(ctx *context.Ctx, name string, isActive bool) {
	lm.Add("admin", "root", 10, ctx.IsRight("user:browse"), "/user", "Admin", "cog")
	lm.Add("user", "admin", 10, ctx.IsRight("user:browse"), "/user", "Users", "user")
	lm.Add("role", "admin", 20, ctx.IsRight("role:browse"), "/role", "Roles", "user-cog")
	lm.Add("config", "admin", 30, ctx.IsRight("config:browse"), "/config", "Configuration", "wrench")

	lm.Add("dashboard", "root", 20, ctx.IsRight("dashboard:browse"), "/dashboard", "Dashboard", "tachometer-alt")
	lm.Add("sensor_monit", "root", 30, true, "/sensor_monit", "Beaumont SW-16 Test Well", "chart-bar")

	lm.Add("device_management", "root", 40, ctx.IsRight("operator:browse"), "/operator", "Device Management", "microchip")
	lm.Add("operator", "device_management", 10, ctx.IsRight("operator:browse"), "/operator", "Operator", "user")
	lm.Add("well", "device_management", 20, ctx.IsRight("well:browse"), "/well", "Well", "oil-pumpjack")
	lm.Add("well_type", "well", 10, ctx.IsRight("well_type:browse"), "/well_type", "Well Type", "sitemap")
	lm.Add("county", "well", 20, ctx.IsRight("county:browse"), "/county", "County", "flag")

	lm.Add("gateway", "device_management", 30, ctx.IsRight("gateway:browse"), "/gateway", "Gateway", "project-diagram")
	lm.Add("gateway_mnf", "gateway", 10, ctx.IsRight("gateway_mnf:browse"), "/gateway_mnf", "Manufacturer", "copyright")

	lm.Add("sensor", "device_management", 40, ctx.IsRight("sensor:browse"), "/sensor", "Sensors", "temperature-low")
	lm.Add("sensor_type", "sensor", 10, ctx.IsRight("sensor_type:browse"), "/sensor_type", "Sensor Type", "temperature-low")
	lm.Add("sensor_mnf", "sensor", 20, ctx.IsRight("sensor_mnf:browse"), "/sensor_mnf", "Manufacturer", "copyright")

	lm.Add("error_log", "root", 50, ctx.IsRight("error_log:browse"), "/error_log", "Error Log", "bug")

	if isActive {
		lm.SetActive(name)
	}

	lm.tax.SortChildren()
	lm.Reduce("root")

	lm.Format(ctx)
}

func (lm *leftMenu) SetActive(name string) {
	allParents := lm.tax.GetAllParents(name)
	allParents = append(allParents, name)

	for _, val := range allParents {
		item := lm.tax.GetItem(val)
		data := item.Data.(*leftMenuItem)
		data.isSubmenuOpen = true

		if val == name {
			data.isActive = true
		}
	}
}

func (lm *leftMenu) Reduce(name string) {
	if lm.tax.IsParent(name) {
		children := lm.tax.GetChildren(name)
		for _, val := range children {
			lm.Reduce(val)
		}

		if lm.tax.List[name].IsVisible {
			return
		}

		if !lm.tax.IsParent(name) {
			lm.tax.Del(name)

			return
		}

		//not visible and parent
		children = lm.tax.GetChildren(name)
		firstChild := children[0]

		nameData := lm.tax.List[name].Data.(*leftMenuItem)
		firstChildData := lm.tax.List[firstChild].Data.(*leftMenuItem)

		nameData.url = firstChildData.url
		return
	}

	if !lm.tax.List[name].IsVisible {
		lm.tax.Del(name)
	}
}

func (lm *leftMenu) Format(ctx *context.Ctx) {
	rv := util.NewBuf()

	children := lm.tax.GetChildren("root")

	if len(children) == 0 {
		return
	}

	rv.Add("<div class=\"side-menu\">")
	rv.Add("<nav>")
	rv.Add("<ul>")

	for _, v := range children {
		lm.FormatItem(v, rv)
	}

	rv.Add("</ul>")
	rv.Add("</nav>")

	if ctx.IsLoggedIn() && (ctx.IsSuperUser() || ctx.IsTestUser()) {
		rv.Add("<div class=\"user-info\" title=\"teleport...\">")
		rv.Add("<a href=\"/change_user\"><strong>User:</strong> %s</a>", ctx.User.Name)
		rv.Add("</div>")
	} else {
		rv.Add("<div class=\"user-info\" title=\"teleport...\">")
		rv.Add("<a href=\"/profile\"><strong>User:</strong> %s</a>", ctx.User.Name)
		rv.Add("</div>")
	}

	rv.Add("<div class=\"logo\">")
	str := "Noven Admin Panel by Fieldlink LLC"
	rv.Add("<img src=\"/asset/img/noven_logo.jpeg\" title=\"%s\" alt=\"%s\">", str, str)
	rv.Add("</div>")

	rv.Add("</div>") //side-menu

	ctx.AddFormat("leftMenu", rv.String())
}

func (lm *leftMenu) FormatItem(name string, rv *util.Buf) {
	item := lm.tax.GetItem(name)
	data := item.Data.(*leftMenuItem)

	classList := make([]string, 0, 5)
	if data.isActive {
		classList = append(classList, "active")
	}

	icon := ""
	if data.icon != "" {
		icon = fmt.Sprintf("<i class=\"fas fa-%s fa-fw fa-2x\"></i>", data.icon)
	}

	if lm.tax.IsParent(name) {
		classList = append(classList, "sub-menu")

		subMenuShow := ""
		if data.isSubmenuOpen {
			subMenuShow = " class=\"sub-menu-show\""
		}

		classStr := ""
		if len(classList) > 0 {
			classStr = fmt.Sprintf(" class=\"%s\"", strings.Join(classList, " "))

		}

		subMenuIcon := "<i class=\"fas fa-plus fa-fw fa-2x\"></i>"
		if subMenuShow != "" {
			subMenuIcon = "<i class=\"fas fa-minus fa-fw fa-2x\"></i>"
		}

		rv.Add("<li%s>", classStr)
		rv.Add("<a href=\"%s\">%s%s<span class=\"sub-menu-button\">%s</span></a>",
			data.url, icon, data.label, subMenuIcon)
		rv.Add("<ul%s>", subMenuShow)

		children := lm.tax.GetChildren(name)
		for _, v := range children {
			lm.FormatItem(v, rv)
		}

		rv.Add("</ul>")
		rv.Add("</li>")
	} else {
		classStr := ""
		if len(classList) > 0 {
			classStr = fmt.Sprintf(" class=\"%s\"", strings.Join(classList, " "))
		}

		rv.Add("<li%s>", classStr)
		rv.Add("<a href=\"%s\">%s%s</a>", data.url, icon, data.label)
		rv.Add("</li>")
	}
}

func (lm *leftMenu) Display() {
	children := lm.tax.GetAllChildren("root")
	for _, name := range children {
		if name == "root" {
			continue
		}

		item := lm.tax.GetItem(name)
		data := item.Data.(*leftMenuItem)
		data.Display()
	}
}

func (lmItem *leftMenuItem) Display() {
	fmt.Println(lmItem.url, lmItem.label, lmItem.icon, lmItem.isActive, lmItem.isSubmenuOpen)
}
