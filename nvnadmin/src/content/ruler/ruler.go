package ruler

import (
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"strconv"
)

type ruler struct {
	PageNo    int64
	TotalRows int64
	InitUrl   string
	List      []*rulerNode
}

type rulerNode struct {
	Label  string
	UrlStr string
	Active bool
}

func NewRuler(totalRow, pageNo int64, initUrl string) *ruler {
	rv := new(ruler)
	rv.InitUrl = initUrl
	rv.PageNo = pageNo
	rv.TotalRows = totalRow

	rv.List = make([]*rulerNode, 0, 20)
	return rv
}

func (rl *ruler) Set(ctx *context.Ctx) string {
	totalPage := ctx.TotalPage(rl.TotalRows)
	if totalPage < 2 {
		return ""
	}

	rulerLen := ctx.Config.RulerLen

	start := rl.PageNo - rulerLen
	if start < 1 {
		start = 1
	}

	end := start + rulerLen*2
	if end > totalPage {
		start -= end - totalPage
		if start < 1 {
			start = 1
		}
		end = totalPage
	}

	if start > 1 {
		node := new(rulerNode)
		node.Label = "First"
		ctx.Cargo.SetInt("pn", 1)
		node.UrlStr = ctx.Cargo.HtmlUrl(rl.InitUrl, "pn")
		rl.List = append(rl.List, node)
	}

	if start > 1 {
		node := new(rulerNode)
		node.Label = "&lt;&lt;"
		ctx.Cargo.SetInt("pn", rl.PageNo-rulerLen)
		node.UrlStr = ctx.Cargo.HtmlUrl(rl.InitUrl, "pn")
		rl.List = append(rl.List, node)
	}

	for i := start; i <= end; i++ {
		node := new(rulerNode)
		if i == rl.PageNo {
			node.Active = true
		}

		node.Label = strconv.FormatInt(i, 10)
		ctx.Cargo.SetInt("pn", i)
		node.UrlStr = ctx.Cargo.HtmlUrl(rl.InitUrl, "pn")
		rl.List = append(rl.List, node)
	}

	if end < totalPage {
		node := new(rulerNode)
		node.Label = "&gt;&gt;"
		ctx.Cargo.SetInt("pn", rl.PageNo+rulerLen)
		node.UrlStr = ctx.Cargo.HtmlUrl(rl.InitUrl, "pn")
		rl.List = append(rl.List, node)
	}

	if end < totalPage {
		node := new(rulerNode)
		node.Label = "Last"
		ctx.Cargo.SetInt("pn", totalPage)
		node.UrlStr = ctx.Cargo.HtmlUrl(rl.InitUrl, "pn")
		rl.List = append(rl.List, node)
	}

	ctx.Cargo.SetInt("pn", rl.PageNo)
	return rl.Format()
}

func (rl *ruler) Format() string {
	rv := util.NewBuf()

	rv.Add("<ul class=\"pagination clear\">")

	var classStr string
	for _, val := range rl.List {
		classStr = ""
		if val.Active {
			classStr = " class=\"active\""
		}

		rv.Add("<li%s><a href=\"%s\">%s</a></li>", classStr, val.UrlStr, val.Label)
	}

	rv.Add("</ul>")

	return *rv.String()
}
