package top_menu

import (
	"fmt"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/tax"
	"nvnadmin/src/lib/util"
)

type topMenuItem struct {
	url      string
	label    string
	icon     string
	isActive bool
}

type topMenu struct {
	tax *tax.Tax
}

func New() *topMenu {
	rv := new(topMenu)
	rv.tax = tax.New()
	rv.Add("root", "end", 0, true, "", "", "")

	return rv
}

func (tm *topMenu) Add(name, parent string, order int64, isVisible bool, url, label, icon string) {
	tm.tax.Add(name, parent, order, isVisible, &topMenuItem{url, label, icon, false})
}

func (tm *topMenu) Set(ctx *context.Ctx, name string, isActive bool) {
	tm.Add("logout", "root", 10, ctx.IsLoggedIn(), "/logout", "Logout", "sign-out")
	tm.Add("profile", "root", 20, ctx.IsRight("profile:browse"), "/profile", "Profile", "user")
	tm.Add("admin_help", "root", 30, ctx.IsRight("admin_help:browse"), "/admin_help", "Admin Help", "life-saver")

	if isActive {
		tm.SetActive(name)
	}

	tm.tax.SortChildren()
	tm.Reduce("root")

	tm.Format(ctx)
}

func (tm *topMenu) SetActive(name string) {
	item := tm.tax.GetItem(name)
	data := item.Data.(*topMenuItem)
	data.isActive = true
}

func (tm *topMenu) Reduce(name string) {
	if tm.tax.IsParent(name) {
		children := tm.tax.GetChildren(name)
		for _, val := range children {
			tm.Reduce(val)
		}

		if tm.tax.List[name].IsVisible {
			return
		}

		if !tm.tax.IsParent(name) {
			tm.tax.Del(name)

			return
		}

		//not visible and parent
		children = tm.tax.GetChildren(name)
		firstChild := children[0]

		nameData := tm.tax.List[name].Data.(*topMenuItem)
		firstChildData := tm.tax.List[firstChild].Data.(*topMenuItem)

		nameData.url = firstChildData.url
		return
	}

	if !tm.tax.List[name].IsVisible {
		tm.tax.Del(name)
	}
}

func (tm *topMenu) Format(ctx *context.Ctx) {
	rv := util.NewBuf()

	children := tm.tax.GetChildren("root")

	if len(children) == 0 {
		return
	}

	rv.Add("<div class=\"top-menu\">")

	first := true
	sep := ""
	for _, v := range children {
		item := tm.tax.GetItem(v)
		data := item.Data.(*topMenuItem)

		if first {
			first = false
		} else {
			sep = " | "
		}

		class := ""
		if data.isActive {
			class = " class=\"active\""
		}

		icon := ""
		if data.icon != "" {
			icon = fmt.Sprintf("<i class=\"fa fa-%s fa-2x fa-fw\"></i>", data.icon)
		}

		rv.Add("%s<a href=\"%s\"%s>%s%s</a>", sep, data.url, class, icon, data.label)
	}

	rv.Add("</div>")

	ctx.AddFormat("topMenu", rv.String())
}
