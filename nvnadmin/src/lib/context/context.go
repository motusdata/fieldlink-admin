package context

import (
	"encoding/json"
	"net/http"
	"net/url"
	"nvnadmin/src/app"
	"nvnadmin/src/content/include"
	"nvnadmin/src/lib/util"
	"strconv"
)

type Ctx struct {
	Config    *app.ConfigType
	Include   *include.Include
	msg       *message
	output    map[string]*string
	Rw        http.ResponseWriter
	Req       *http.Request
	SessionId string
	User      *userRec
	RightList map[string]bool
	Session   *sessionType
	Cargo     cargoList
	url       *url.URL
}

func NewContext(rw http.ResponseWriter, req *http.Request) *Ctx {
	ctx := new(Ctx)
	ctx.Config = app.CopyConfig()
	ctx.msg = new(message)

	ctx.Include = include.NewInclude()

	ctx.output = make(map[string]*string, 50)

	ctx.Rw = rw
	ctx.Req = req

	//start session
	ctx.Session = ctx.NewSession()
	ctx.readSession()

	if ctx.SessionId != "" {
		ctx.SetUserInfo()
		ctx.setUserRightList()
	}

	ctx.Cargo = ctx.NewCargo()

	parsedUrl, err := url.ParseRequestURI(ctx.Req.RequestURI)
	if err != nil {
		panic("Can not parse request url.")
	}

	ctx.url = parsedUrl

	ctx.readMessages()
	ctx.clearMessages()

	return ctx
}

func (ctx *Ctx) ReadCargo() {
	values, _ := url.ParseQuery(ctx.url.RawQuery)

	for k, _ := range ctx.Cargo {
		str := values.Get(k)
		if str != "" {
			ctx.Cargo.SetConvert(k, str)
		}
	}
}

func (ctx *Ctx) AddFormat(name string, str *string) {
	if _, ok := ctx.output[name]; ok {
		panic("Formatted content already exists: " + name)
	}

	ctx.output[name] = str
}

func (ctx *Ctx) Redirect(urlStr string) {
	ctx.saveMessages()
	ctx.SaveSession()

	http.Redirect(ctx.Rw, ctx.Req, urlStr, 302)
}

func (ctx *Ctx) Render(pageTmp string) {
	ctx.SaveSession()

	ctx.AddFormat("css", ctx.Include.FormatCss())
	ctx.AddFormat("js", ctx.Include.FormatJs())

	messageStr := ctx.FormatSiteMessages()
	if len(*messageStr) > 0 {
		ctx.AddFormat("message", messageStr)
	}

	ctx.AddFormat("lang", &ctx.Config.Lang)

	err := app.Tmpl.ExecuteTemplate(ctx.Rw, pageTmp, ctx.output)
	if err != nil {
		panic(err)
	}
}

func (ctx *Ctx) RenderAjax() {
	ajaxStatus := "success"
	ctx.AddFormat("status", &ajaxStatus)

	messageStr := ctx.FormatSiteMessages()
	if len(*messageStr) > 0 {
		ctx.AddFormat("message", messageStr)
	}

	ctx.AddFormat("lang", &ctx.Config.Lang)

	jsonStr, err := json.Marshal(ctx.output)
	if err != nil {
		panic(err.Error())
	}

	ctx.Rw.Header().Set("Content-Type", "application/json")
	ctx.Rw.Header().Set("Content-Length", strconv.Itoa(len(jsonStr)))
	ctx.Rw.Write(jsonStr)
}

func (ctx *Ctx) RenderAjaxError() {
	ajaxStatus := "error"
	ctx.AddFormat("status", &ajaxStatus)

	messageStr := ctx.FormatSiteMessages()
	if len(*messageStr) > 0 {
		ctx.AddFormat("message", messageStr)
	}

	ctx.AddFormat("lang", &ctx.Config.Lang)

	jsonStr, err := json.Marshal(ctx.output)
	if err != nil {
		panic(err.Error())
	}

	ctx.Rw.Header().Set("Content-Type", "application/json")
	ctx.Rw.Header().Set("Content-Length", strconv.Itoa(len(jsonStr)))
	ctx.Rw.Write(jsonStr)
}

func (ctx *Ctx) TotalPage(totalRows int64) int64 {
	var r int64
	if totalRows%ctx.Config.PageLen > 0 {
		r = 1
	} else {
		r = 0
	}

	totalPage := totalRows/ctx.Config.PageLen + r

	return totalPage
}

func (ctx *Ctx) TouchPageNo(pageNo, totalRows int64) int64 {
	totalPage := ctx.TotalPage(totalRows)
	if pageNo < 1 {
		return 1
	} else if pageNo > totalPage {
		return totalPage
	}

	return pageNo
}

//messages
type message struct {
	errorList   []string
	warningList []string
	infoList    []string
	successList []string
}

func (ctx *Ctx) Error(str string) {
	ctx.msg.errorList = append(ctx.msg.errorList, str)
}

func (ctx *Ctx) Warning(str string) {
	ctx.msg.warningList = append(ctx.msg.warningList, str)
}

func (ctx *Ctx) Info(str string) {
	ctx.msg.infoList = append(ctx.msg.infoList, str)
}

func (ctx *Ctx) Success(str string) {
	ctx.msg.successList = append(ctx.msg.successList, str)
}

func (ctx *Ctx) FormatSiteMessages() *string {

	buf := util.NewBuf()

	if len(ctx.msg.errorList) > 0 {
		buf.Add("<div class=\"alert alert-danger\">")
		buf.Add("<button type=\"button\" class=\"btn-close\">")
		buf.Add("<span>&times;</span>")
		buf.Add("</button>")

		for _, val := range ctx.msg.errorList {
			buf.Add("<strong>Error:</strong> %s<br>", val)
		}

		buf.Add("</div>")
	}

	if len(ctx.msg.warningList) > 0 {
		buf.Add("<div class=\"alert alert-warning\">")
		buf.Add("<button type=\"button\" class=\"btn-close\">")
		buf.Add("<span>&times;</span>")
		buf.Add("</button>")

		for _, val := range ctx.msg.warningList {
			buf.Add("<strong>Warning:</strong> %s<br>", val)
		}

		buf.Add("</div>")
	}

	if len(ctx.msg.infoList) > 0 {
		buf.Add("<div class=\"alert alert-info\">")
		buf.Add("<button type=\"button\" class=\"btn-close\">")
		buf.Add("<span>&times;</span>")
		buf.Add("</button>")

		for _, val := range ctx.msg.infoList {
			buf.Add("<strong>Info:</strong> %s<br>", val)
		}

		buf.Add("</div>")
	}

	if len(ctx.msg.successList) > 0 {
		buf.Add("<div class=\"alert alert-success\">")
		buf.Add("<button type=\"button\" class=\"btn-close\">")
		buf.Add("<span>&times;</span>")
		buf.Add("</button>")

		for _, val := range ctx.msg.successList {
			buf.Add("<strong>Success:</strong> %s<br>", val)
		}

		buf.Add("</div>")
	}

	return buf.String()
}
