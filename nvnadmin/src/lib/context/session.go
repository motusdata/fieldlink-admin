package context

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/lib/util"
	"time"
)

type sessionType struct {
	ErrorList      []string
	WarningList    []string
	InfoList       []string
	SuccessList    []string
	UserId         int64
	SensorReadTime int64
}

func (ctx *Ctx) NewSession() *sessionType {
	rv := new(sessionType)

	return rv
}

func (ctx *Ctx) CreateSession() {
	now := time.Now()
	epoch := now.Unix()
	expire := now.Add(time.Duration(app.Ini.CookieExpires) * time.Second)
	randString := util.RandString(32)

	cookie := http.Cookie{
		Name:     app.Ini.CookieName,
		Value:    randString,
		Path:     app.Ini.CookiePath,
		Domain:   app.Ini.CookieDomain,
		Expires:  expire,
		Secure:   app.Ini.CookieSecure,
		HttpOnly: app.Ini.CookieHttpOnly,
	}

	http.SetCookie(ctx.Rw, &cookie)

	jsonData, err := json.Marshal(ctx.Session)
	if err != nil {
		panic(err)
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `insert into
					session(sessionId, expires, data)
					values(?, ?, ?)`

	_, err = tx.Exec(sqlStr, randString, epoch, jsonData)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	tx.Commit()
	ctx.SessionId = randString
}

func (ctx *Ctx) readSession() {
	cookie, ok := ctx.Req.Cookie(app.Ini.CookieName)
	if ok != nil {
		return
	}

	sqlStr := `select
					expires,
					data
				from
					session
				where
					sessionId = ?`

	row := app.Db.QueryRow(sqlStr, cookie.Value)

	var expires int64
	var data []byte

	err := row.Scan(&expires, &data)
	if err != nil {
		if err == sql.ErrNoRows {
			return
		}

		panic(err)
	}

	err = json.Unmarshal(data, &ctx.Session)
	if err != nil {
		panic(err)
	}

	ctx.SessionId = cookie.Value
}

func (ctx *Ctx) SaveSession() {
	jsonData, err := json.Marshal(ctx.Session)
	if err != nil {
		panic(err)
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `update session
					set data = ?
				where
					sessionId = ?`

	_, err = tx.Exec(sqlStr, jsonData, ctx.SessionId)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	tx.Commit()
}

func (ctx *Ctx) DeleteSession() {
	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `delete from
					session
				where
					sessionId = ?`

	_, err = tx.Exec(sqlStr, ctx.SessionId)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	tx.Commit()
}

func (ctx *Ctx) readMessages() {
	if ctx.Session.ErrorList != nil {
		ctx.msg.errorList = ctx.Session.ErrorList
	}

	if ctx.Session.WarningList != nil {
		ctx.msg.warningList = ctx.Session.WarningList
	}

	if ctx.Session.InfoList != nil {
		ctx.msg.infoList = ctx.Session.InfoList
	}

	if ctx.Session.SuccessList != nil {
		ctx.msg.successList = ctx.Session.SuccessList
	}
}

func (ctx *Ctx) clearMessages() {
	ctx.Session.ErrorList = nil
	ctx.Session.WarningList = nil
	ctx.Session.InfoList = nil
	ctx.Session.SuccessList = nil
}

func (ctx *Ctx) saveMessages() {
	if ctx.msg.errorList != nil {
		ctx.Session.ErrorList = ctx.msg.errorList
	}

	if ctx.msg.warningList != nil {
		ctx.Session.WarningList = ctx.msg.warningList
	}

	if ctx.msg.infoList != nil {
		ctx.Session.InfoList = ctx.msg.infoList
	}

	if ctx.msg.successList != nil {
		ctx.Session.SuccessList = ctx.msg.successList
	}
}
