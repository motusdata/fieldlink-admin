package context

import (
	"database/sql"
	"nvnadmin/src/app"
)

type userRec struct {
	UserId   int64
	Name     string
	Login    string
	Email    string
	Password string
	Status   string
}

type roleRightRec struct {
	RoleRight string
}

func (ctx *Ctx) SetUserInfo() {
	sqlStr := `select
					userId,
					name,
					login,
					name,
					email,
					status
				from
					user
				where
					userId = ?`

	row := app.Db.QueryRow(sqlStr, ctx.Session.UserId)

	rec := new(userRec)
	err := row.Scan(&rec.UserId, &rec.Name, &rec.Login, &rec.Name, &rec.Email, &rec.Status)
	if err != nil {
		if err == sql.ErrNoRows {
			panic("Could not read user info.")
		}

		panic(err)
	}

	ctx.User = rec
}

func (ctx *Ctx) setUserRightList() {
	sqlStr := `select
					pageRight
				from
					user,
					userRole,
					roleRight
				where
					user.userId = userRole.userId and
					userRole.roleId = roleRight.roleId and
					user.userId = ?`

	rows, err := app.Db.Query(sqlStr, ctx.Session.UserId)
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	ctx.RightList = make(map[string]bool, 50)
	for rows.Next() {
		rec := new(roleRightRec)
		if err = rows.Scan(&rec.RoleRight); err != nil {
			panic(err)
		}

		ctx.RightList[rec.RoleRight] = true
	}
}

func (ctx *Ctx) IsRight(right string) bool {
	if ctx.SessionId == "" {
		return false
	}

	if ctx.User.Status == "blocked" {
		return false
	}

	if ctx.RightList[right] || ctx.User.Login == "superuser" {
		return true
	}

	return false
}

func (ctx *Ctx) IsLoggedIn() bool {
	return ctx.SessionId != ""
}

func (ctx *Ctx) IsSuperUser() bool {
	return ctx.User != nil && ctx.User.Login == "superuser"
}

func (ctx *Ctx) IsTestUser() bool {
	return ctx.User != nil && ctx.User.Login == "testuser"
}
