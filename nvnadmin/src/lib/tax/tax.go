package tax

import (
	"sort"
)

type child struct {
	Name  string
	Order int64
}

type childList []*child

func (cl *childList) Add(name string, order int64) {
	*cl = append(*cl, &child{Name: name, Order: order})
}

func (cl *childList) del(name string) {
	pos := -1
	for key, val := range *cl {
		if val.Name == name {
			pos = key
			break
		}
	}

	if pos != -1 {
		*cl = append((*cl)[0:pos], (*cl)[pos+1:]...)
	}
}

func (cl childList) Len() int {
	return len(cl)
}

func (cl childList) Swap(i, j int) {
	cl[i], cl[j] = cl[j], cl[i]
}

func (cl childList) Less(i, j int) bool {
	return cl[i].Order < cl[j].Order
}

type TaxItem struct {
	name      string
	parent    string
	children  childList
	order     int64
	IsVisible bool
	Data      interface{}
}

func newTaxItem(name, parent string, order int64, isVisible bool, data interface{}) *TaxItem {
	rv := new(TaxItem)
	rv.name = name
	rv.parent = parent
	rv.order = order
	rv.IsVisible = isVisible
	rv.Data = data
	rv.children = make(childList, 0, 10)

	return rv
}

type Tax struct {
	List map[string]*TaxItem
}

func New() *Tax {
	rv := new(Tax)
	rv.List = make(map[string]*TaxItem, 50)

	return rv
}

func (tax *Tax) Add(name, parent string, order int64, isVisible bool, data interface{}) {
	if _, ok := tax.List[name]; ok {
		panic("Tax item already exists: " + name)
	}

	if _, ok := tax.List[parent]; !ok && parent != "end" {
		panic("Parent item does not exist: " + parent)
	}

	item := newTaxItem(name, parent, order, isVisible, data)
	if parent != "end" {
		tax.List[parent].children.Add(name, order)
	}

	tax.List[name] = item
}

func (tax *Tax) IsExists(name string) bool {
	_, ok := tax.List[name]

	return ok
}

func (tax *Tax) IsEmpty() bool {
	return len(tax.List) == 0
}

func (tax *Tax) Del(name string) {
	if _, ok := tax.List[name]; !ok {
		panic("Item does not exist: " + name)
	}

	parent := tax.List[name].parent
	tax.List[parent].children.del(name)
	delete(tax.List, name)
}

func (tax *Tax) GetItem(name string) *TaxItem {
	item, ok := tax.List[name]
	if !ok {
		panic("Tax item does not exists: " + name)
	}

	return item
}

func (tax *Tax) GetAllParents(name string) []string {
	if _, ok := tax.List[name]; !ok {
		panic("Tax item does not exists: " + name)
	}

	rv := make([]string, 0, 10)
	for parent := tax.List[name].parent; parent != "end"; parent = tax.List[parent].parent {
		rv = append(rv, parent)
	}

	return rv
}

func (tax *Tax) IsParent(name string) bool {
	item, ok := tax.List[name]
	if !ok {
		panic("Tax item does not exists: " + name)
	}

	if len(item.children) > 0 {
		return true
	}

	return false
}

func (tax *Tax) GetChildren(name string) []string {
	item, ok := tax.List[name]
	if !ok {
		panic("Tax item does not exists: " + name)
	}

	rv := make([]string, 0, 10)
	for _, val := range item.children {
		rv = append(rv, val.Name)
	}

	return rv
}

func (tax *Tax) GetAllChildren(name string) []string {
	rv := make([]string, 0, 10)

	tax.getAllChildrenR(name, &rv)

	return rv
}

func (tax *Tax) getAllChildrenR(name string, rv *[]string) {
	*rv = append(*rv, name)

	for _, val := range tax.List[name].children {
		tax.getAllChildrenR(val.Name, rv)
	}
}

func (tax *Tax) SortChildren() {
	for key, val := range tax.List {
		if tax.IsParent(key) {
			sort.Sort(val.children)
		}
	}
}
