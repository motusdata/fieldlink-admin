package util

import (
	"errors"
	"regexp"
	"strings"
)

func IsValidEmail(str string) error {
	re := regexp.MustCompile("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@" +
		"(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
	res := re.MatchString(str)
	if !res {
		return errors.New("Invalid e-mail address.")
	}

	return nil
}

func IsValidPassword(str string) error {
	if len(str) < 6 {
		return errors.New("Password is too short. It must contain at least 6 characters.")
	}

	re := regexp.MustCompile("[a-z0-9!#$%&]+")
	res := re.MatchString(str)
	if !res {
		return errors.New("Invalid character in password. Allowed characters:|| 'a-z0-9!#$%&'")
	}

	return nil
}

func IsValidIdentifier(str string) bool {
	rv, err := regexp.MatchString("^[a-zA-Z]{1}[a-zA-Z0-9]*", str)
	if err != nil {
		panic(err)
	}

	return rv
}

func IsValidIp(str string) bool {
	str = strings.Trim(str, " ")

	re, _ := regexp.Compile(`^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$`)
	if re.MatchString(str) {
		return true
	}
	return false
}
