package util

import (
	"errors"
	"time"
)

var TimeFormat string = "02-01-2006 15:04:05"
var DateFormat string = "02-01-2006"

//date functions
func Now() int64 {
	timeStr := time.Now().Format(TimeFormat)
	rv, _ := TimeStrToInt64(timeStr)
	return rv
}

func Int64ToTimeStr(epoch int64) string {
	rv := time.Unix(epoch, 0)
	return rv.UTC().Format(TimeFormat)
}

func TimeStrToInt64(timeStr string) (int64, error) {
	rv, err := time.Parse(TimeFormat, timeStr)
	if err != nil {
		return 0, errors.New("Could not parse the time value.")
	}

	return rv.Unix(), nil
}

func IsValidTimeStr(timeStr string) bool {
	epoch, err := TimeStrToInt64(timeStr)
	if err != nil {
		return false
	}

	str := Int64ToTimeStr(epoch)

	return timeStr == str
}

func Int64ToDateStr(epoch int64) string {
	rv := time.Unix(epoch, 0)
	return rv.UTC().Format(DateFormat)
}

func DateStrToInt64(timeStr string) (int64, error) {
	rv, err := time.Parse(DateFormat, timeStr)
	if err != nil {
		return 0, errors.New("Could not parse the time value.")
	}

	return rv.Unix(), nil
}

func IsValidDateStr(dateStr string) bool {
	epoch, err := DateStrToInt64(dateStr)
	if err != nil {
		return false
	}

	str := Int64ToDateStr(epoch)

	return dateStr == str
}
