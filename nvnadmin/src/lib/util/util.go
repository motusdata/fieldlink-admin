package util

import (
	"crypto/rand"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"html"
	"io"
	"regexp"
	"strconv"
	"strings"
)

type Buf struct {
	data     []string
	delList  []int
	isDelete bool
	str      string
}

func NewBuf() *Buf {
	rv := new(Buf)
	rv.data = make([]string, 0, 20)
	rv.delList = make([]int, 0, 20)
	rv.isDelete = true

	return rv
}

func (buf *Buf) Add(format string, args ...interface{}) {
	if format == "" {
		return
	}

	str := format
	if len(args) > 0 {
		str = fmt.Sprintf(format, args...)
	}

	buf.data = append(buf.data, str)
}

func (buf *Buf) AddLater(format string, args ...interface{}) {
	buf.Add(format, args...)
	buf.delList = append(buf.delList, len(buf.data)-1)
}

func (buf *Buf) Forge() {
	buf.isDelete = false
}

func (buf *Buf) clearDelList() {
	for _, v := range buf.delList {
		buf.data[v] = ""
	}
}

func (buf *Buf) String() *string {
	if buf.isDelete {
		buf.clearDelList()
	}

	buf.str = strings.Join(buf.data, "\n")
	return &buf.str
}

func (buf *Buf) StringSep(sep string) *string {
	if buf.isDelete {
		buf.clearDelList()
	}

	buf.str = strings.Join(buf.data, sep)
	return &buf.str
}

func (buf *Buf) Len() int {
	return len(buf.data)
}

func (buf *Buf) IsEmpty() bool {
	return len(buf.data) == 0
}

func RandString(size int) string {
	buf := make([]byte, size)

	if _, err := rand.Read(buf); err != nil {
		panic(err)
	}

	rv := base64.URLEncoding.EncodeToString(buf)[:size]
	rv = strings.Replace(rv, "_", "", -1)
	rv = strings.Replace(rv, "-", "", -1)
	return rv
}

func ScrStr(str string) string {
	return html.EscapeString(str)
}

func DbStr(v string) string {
	buf := make([]byte, len(v)*2)
	pos := 0

	for i := 0; i < len(v); i++ {
		c := v[i]
		switch c {
		case '\x00':
			buf[pos] = '\\'
			buf[pos+1] = '0'
			pos += 2
		case '\n':
			buf[pos] = '\\'
			buf[pos+1] = 'n'
			pos += 2
		case '\r':
			buf[pos] = '\\'
			buf[pos+1] = 'r'
			pos += 2
		case '\x1a':
			buf[pos] = '\\'
			buf[pos+1] = 'Z'
			pos += 2
		case '\'':
			buf[pos] = '\\'
			buf[pos+1] = '\''
			pos += 2
		case '"':
			buf[pos] = '\\'
			buf[pos+1] = '"'
			pos += 2
		case '\\':
			buf[pos] = '\\'
			buf[pos+1] = '\\'
			pos += 2
		default:
			buf[pos] = c
			pos += 1
		}
	}

	return string(buf[:pos])
}

func PasswordHash(str string) string {
	h := sha1.New()
	io.WriteString(h, str+"987$#sd")
	return fmt.Sprintf("%X", h.Sum(nil))
}

var intRe *regexp.Regexp = regexp.MustCompile("(\\d+)(\\d{3})")

func FormatInt(num int64) string {
	numStr := strconv.FormatInt(num, 10)

	for {
		rv := intRe.ReplaceAllString(numStr, "$1.$2")
		if rv == numStr {
			return rv
		}

		numStr = rv
	}
}
