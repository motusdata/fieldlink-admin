package watch

import (
	"fmt"
	"os/exec"
)

func refreshChrome() {

	cur, err := exec.Command("xdotool", "getwindowfocus").Output()
	if err != nil {
		fmt.Printf("output: %s error: %s", cur, err.Error())
	}

	out, err := exec.Command("xdotool", "search", "--onlyvisible", "--class", "Chrome",
		"windowfocus", "key", "F5").Output()
	if err != nil {
		fmt.Printf("output: %s error: %s", out, err.Error())
	}

	out, err = exec.Command("xdotool", "windowactivate", string(cur)).Output()
	if err != nil {
		fmt.Printf("output: %s error: %s", out, err.Error())
	}

	fmt.Printf("browser refreshed. %s\n", out)
}
