package watch

import (
	"fmt"
	"io/ioutil"
	"nvnadmin/src/app"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	"github.com/fsnotify/fsnotify"
)

var lessWatcher *fsnotify.Watcher

func WatchLess() {
	lessWatcher, _ = fsnotify.NewWatcher()

	if err := filepath.Walk(app.Ini.HomeDir+"/style/less", addLessWatchDir); err != nil {
		panic(err.Error())
	}

	var isActive bool

	go func() {
		for _ = range time.Tick(700 * time.Millisecond) {
			isActive = true
		}
	}()

	go func() {
		for {
			select {
			case event := <-lessWatcher.Events:
				fmt.Printf("%s - %s\n", event.Name, event.Op)
				if isActive {
					compileLess()
					isActive = false
				}
			case err := <-lessWatcher.Errors:
				fmt.Println(err.Error())
			}
		}
	}()

}

func addLessWatchDir(path string, fi os.FileInfo, err error) error {

	if fi.Mode().IsDir() {
		return lessWatcher.Add(path)
	}

	return nil
}

func compileLess() {
	source := app.Ini.HomeDir + "/style/less/style.less"
	target := app.Ini.HomeDir + "/asset/css/style.css"

	cmd := exec.Command("lessc", source, target)
	stderr, err := cmd.StderrPipe()
	if err != nil {
		println("1. " + err.Error())
	}

	if err := cmd.Start(); err != nil {
		println("2. " + err.Error())
	}

	slurp, err := ioutil.ReadAll(stderr)
	if err != nil {
		println("3. " + err.Error())
	}

	println(string(slurp))

	if err := cmd.Wait(); err != nil {
		println("4. " + err.Error())
	}

	out, err := cmd.Output()
	if err != nil {
		println("5. " + err.Error())
	}

	fmt.Printf("less compiled. %s\n", out)
	refreshChrome()
}
