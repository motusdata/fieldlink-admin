package change_user

import (
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/page/user/user_lib"
)

func Browse(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsLoggedIn() || (!ctx.IsSuperUser() && !ctx.IsTestUser()) {
		app.BadRequest()
	}

	if ctx.Session.UserId == 69 {
		ctx.Session.UserId = 30 //superuser
	} else {
		ctx.Session.UserId = 69 //test
	}

	rec, err := user_lib.GetUserRec(ctx.User.UserId)
	if err != nil {
		panic(err)
	}

	ctx.User.UserId = rec.UserId
	ctx.User.Name = rec.Name
	ctx.User.Login = rec.Login
	ctx.User.Email = rec.Email
	ctx.User.Password = rec.Password
	ctx.User.Status = rec.Status

	ctx.Redirect("/welcome")
}
