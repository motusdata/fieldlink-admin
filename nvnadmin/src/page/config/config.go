package config

import (
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/config/config_lib"
)

func Browse(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("config:browse") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.ReadCargo()

	content.Include(ctx)
	content.Default(ctx)

	browseMid(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "config", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}

func browseMid(ctx *context.Ctx) {

	configList := config_lib.GetConfigList()
	if len(configList) == 0 {
		ctx.Warning("Empty list.")
	}

	insertRight := ctx.IsRight("config:insert")
	updateRight := ctx.IsRight("config:update")
	deleteRight := ctx.IsRight("config:delete")
	setRight := ctx.IsRight("config:set")

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Configuration"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	if insertRight {
		buf.Add("<div class=\"cmd\">")
		urlStr := ctx.Cargo.HtmlUrl("/config_insert")
		buf.Add("<a href=\"%s\" class=\"btn btn-primary btn-md\" title=\"New record.\">New</a>", urlStr)
		buf.Add("</div>")
	}

	buf.Add("<table>")

	buf.Add("<thead>")
	buf.Add("<tr>")
	buf.Add("<th>Title</th>")
	buf.Add("<th>Value</th>")

	if updateRight || deleteRight {
		buf.Add("<th>Enum</th>")
	}

	buf.Add("<th>Explanation</th>")

	if setRight || updateRight || deleteRight {
		buf.Add("<th class=\"cmd cmd-table\">Command</th>")
	}

	buf.Add("</tr>")
	buf.Add("</thead>")

	buf.Add("<tbody>")

	var name, title, value, exp string
	for _, row := range configList {
		ctx.Cargo.SetInt("id", row.ConfigId)

		name = util.ScrStr(row.Name)
		title = util.ScrStr(row.Title)
		value = util.ScrStr(row.Value)
		exp = util.ScrStr(row.Exp)

		buf.Add("<tr>")
		buf.Add("<td title=\"%s\">%s</td>", name, title)

		if !ctx.IsSuperUser() && name == "smtpPassword" {
			buf.Add("<td>*******</td>")
		} else {
			buf.Add("<td>%s</td>", value)
		}

		if updateRight || deleteRight {
			buf.Add("<td>%d</td>", row.Enum)
		}

		buf.Add("<td>%s</td>", exp)

		if setRight || updateRight || deleteRight {
			buf.Add("<td class=\"cmd cmd-table\">")

			if setRight {
				urlStr := ctx.Cargo.HtmlUrl("/config_set", "id")
				buf.Add("<a href=\"%s\" class=\"btn btn-default btn-sm\" title=\"Set value.\">Set</a>", urlStr)
			}

			if updateRight {
				urlStr := ctx.Cargo.HtmlUrl("/config_update", "id")
				buf.Add("<a href=\"%s\" class=\"btn btn-default btn-sm\" title=\"Edit record.\">Edit</a>", urlStr)
			}

			if deleteRight {
				urlStr := ctx.Cargo.HtmlUrl("/config_delete", "id")
				buf.Add("<a href=\"%s\" class=\"btn btn-danger btn-sm\" title=\"Delete record.\">Delete</a>", urlStr)
			}

			buf.Add("</td>")
		}

		buf.Add("</tr>")
	}

	buf.Add("</tbody>")
	buf.Add("</table>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())
}
