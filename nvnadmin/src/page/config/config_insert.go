package config

import (
	"fmt"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"strconv"

	"github.com/go-sql-driver/mysql"
)

func Insert(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsLoggedIn() || !ctx.IsSuperUser() {
		app.BadRequest()
	}

	if ctx.Req.Method == "GET" {
		insertForm(ctx)
		return
	}

	enum, err := strconv.ParseInt(ctx.Req.PostFormValue("enum"), 10, 64)
	if err != nil {
		enum = 0
	}

	name := ctx.Req.PostFormValue("name")
	varType := ctx.Req.PostFormValue("type")
	value := ctx.Req.PostFormValue("value")
	title := ctx.Req.PostFormValue("title")
	exp := ctx.Req.PostFormValue("exp")

	if name == "" || varType == "" || value == "" || title == "" || exp == "" {
		ctx.Warning("You have left one or more fields empty.")
		insertForm(ctx)
		return
	}

	if !util.IsValidIdentifier(name) {
		ctx.Warning(fmt.Sprintf("Invalid variable name. Variable name pattern: %s",
			"^[a-zA-Z]{1}[a-zA-Z0-9]*"))
		insertForm(ctx)
		return
	}

	if varType != "int" && varType != "string" {
		ctx.Warning("Invalid variable type. Must be 'int' or 'string'.")
		insertForm(ctx)
		return
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `insert into
					config(configId, enum, name, type, value, title, exp)
					values(null, ?, ?, ?, ?, ?, ?)`

	_, err = tx.Exec(sqlStr, enum, name, varType, value, title, exp)
	if err != nil {
		tx.Rollback()
		if err, ok := err.(*mysql.MySQLError); ok {
			if err.Number == 1062 {
				ctx.Warning("Duplicate record.")
				insertForm(ctx)
				return
			}
		}

		panic(err)
	}

	tx.Commit()

	app.ReadConfig()
	ctx.Success("Record has been saved.")
	ctx.Redirect(ctx.Cargo.HtmlUrl("/config"))
}

func insertForm(ctx *context.Ctx) {
	content.Include(ctx)

	var enum int64
	var name, varType, value, title, exp string
	var err error
	if ctx.Req.Method == "POST" {
		enum, err = strconv.ParseInt(ctx.Req.PostFormValue("enum"), 10, 64)
		if err != nil {
			enum = 0
		}

		name = ctx.Req.PostFormValue("name")
		varType = ctx.Req.PostFormValue("type")
		value = ctx.Req.PostFormValue("value")
		title = ctx.Req.PostFormValue("title")
		exp = ctx.Req.PostFormValue("exp")
	} else {
		enum = 0
		name = ""
		varType = ""
		value = ""
		title = ""
		exp = ""
	}

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Configuration", "New Record"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	buf.Add("<a href=\"/config\" class=\"btn btn-warning btn-md\"\">Back</a>")
	buf.Add("</div>")

	urlStr := ctx.Cargo.HtmlUrl("/config_insert")
	buf.Add("<form action=\"%s\" method=\"post\">", urlStr)

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"name\">Variable Name:</label>")
	buf.Add("<input type=\"text\" name=\"name\" id=\"name\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"100\" tabindex=\"1\" autofocus>", util.ScrStr(name))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"type\">Variable Type:</label>")
	buf.Add("<input type=\"text\" name=\"type\" id=\"type\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"10\" tabindex=\"2\">", util.ScrStr(varType))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"value\">Value:</label>")
	buf.Add("<input type=\"text\" name=\"value\" id=\"value\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"250\" tabindex=\"3\">", util.ScrStr(value))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"title\">Title:</label>")
	buf.Add("<input type=\"text\" name=\"title\" id=\"title\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"100\" tabindex=\"4\">", util.ScrStr(title))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"exp\">Explanation:</label>")
	buf.Add("<input type=\"text\" name=\"exp\" id=\"exp\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"250\" tabindex=\"5\">", util.ScrStr(exp))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"enum\">Enumeration:</label>")
	buf.Add("<input type=\"text\" name=\"enum\" id=\"enum\" class=\"form-control\""+
		" value=\"%d\" maxlength=\"11\" tabindex=\"6\">", enum)
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<button type=\"submit\" class=\"btn btn-primary\" tabindex=\"7\">Submit</button>")
	buf.Add("<button type=\"reset\" class=\"btn btn-default\" tabindex=\"8\">Reset</button>")
	buf.Add("</div>")

	buf.Add("</form>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "config", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
