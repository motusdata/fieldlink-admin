package config_lib

import (
	"errors"
	"nvnadmin/src/app"
	"strconv"
)

type ConfigRec struct {
	ConfigId int64
	Enum     int64
	Name     string
	Type     string
	Value    string
	Title    string
	Exp      string
}

type checkFunc func(string) error

var CheckList = make(map[string]checkFunc, 10)

func init() {
	CheckList["pageLen"] = checkPageLen
	CheckList["rulerLen"] = checkRulerLen
	CheckList["lang"] = checkLang
}

func GetConfigRec(id int64) (*ConfigRec, error) {
	sqlStr := `select
					configId,
					enum,
					name,
					type,
					value,
					title,
					exp
				from
					config
				where
					configId = ?`

	row := app.Db.QueryRow(sqlStr, id)

	rec := new(ConfigRec)
	if err := row.Scan(&rec.ConfigId, &rec.Enum, &rec.Name, &rec.Type,
		&rec.Value, &rec.Title, &rec.Exp); err != nil {
		return nil, err
	}

	return rec, nil
}

func GetConfigList() []*ConfigRec {
	sqlStr := `select
					configId,
					enum,
					name,
					type,
					value,
					title,
					exp
				from
					config
				order by
					enum`

	rows, err := app.Db.Query(sqlStr)
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	rv := make([]*ConfigRec, 0, 100)
	for rows.Next() {
		rec := new(ConfigRec)
		if err = rows.Scan(&rec.ConfigId, &rec.Enum, &rec.Name, &rec.Type,
			&rec.Value, &rec.Title, &rec.Exp); err != nil {
			panic(err)
		}

		rv = append(rv, rec)
	}

	return rv
}

//check functions
func checkPageLen(val string) error {
	pageLen, err := strconv.ParseInt(val, 10, 64)
	if err == nil && pageLen >= 5 && pageLen <= 100 {
		return nil
	}

	return errors.New("Invalid page length. Please enter a number between 10-100.")
}

func checkRulerLen(val string) error {
	rulerLen, err := strconv.ParseInt(val, 10, 64)
	if err == nil && rulerLen >= 2 && rulerLen <= 10 {
		return nil
	}

	return errors.New("Invalid ruler length. Please enter a number between 2-10.")
}

func checkLang(val string) error {
	if val == "en" || val == "tr" {
		return nil
	}

	return errors.New("Invalid language. Language must be 'en' or 'tr'.")
}
