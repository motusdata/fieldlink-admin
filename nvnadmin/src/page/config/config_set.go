package config

import (
	"database/sql"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/config/config_lib"
)

func Set(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("config:set") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.ReadCargo()

	id := ctx.Cargo.Int("id")
	rec, err := config_lib.GetConfigRec(id)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Warning("Record could not be found.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/config"))
			return
		}

		panic(err)
	}

	if ctx.Req.Method == "GET" {
		setForm(ctx, rec)
		return
	}

	value := ctx.Req.PostFormValue("value")

	if value == "" {
		ctx.Warning("You have left one or more fields empty.")
		setForm(ctx, rec)
		return
	}

	if val, ok := config_lib.CheckList[rec.Name]; ok {
		if err := val(value); err != nil {
			ctx.Warning(err.Error())
			setForm(ctx, rec)
			return
		}
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `update config set
					value = ?
				where
					configId = ?`

	res, err := tx.Exec(sqlStr, value, id)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	if n, _ := res.RowsAffected(); n == 0 {
		tx.Rollback()
		ctx.Warning("You did not change the record.")
		setForm(ctx, rec)
		return
	}

	tx.Commit()

	app.ReadConfig()
	ctx.Success("Record has been changed.")
	ctx.Redirect(ctx.Cargo.HtmlUrl("/config"))
}

func setForm(ctx *context.Ctx, rec *config_lib.ConfigRec) {
	content.Include(ctx)

	var value string
	if ctx.Req.Method == "POST" {
		value = ctx.Req.PostFormValue("value")
	} else {
		value = rec.Value
	}

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Configuration", "Set Value"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	buf.Add("<a href=\"/config\" class=\"btn btn-warning btn-md\"\">Back</a>")
	buf.Add("</div>")

	urlStr := ctx.Cargo.HtmlUrl("/config_set", "id")
	buf.Add("<form action=\"%s\" method=\"post\">", urlStr)

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"value\">Value:</label>")
	buf.Add("<input type=\"text\" name=\"value\" id=\"value\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"250\" tabindex=\"1\" autofocus>", util.ScrStr(value))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<button type=\"submit\" class=\"btn btn-primary\" tabindex=\"2\">Submit</button>")
	buf.Add("<button type=\"reset\" class=\"btn btn-default\" tabindex=\"3\">Reset</button>")
	buf.Add("</div>")

	buf.Add("</form>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "config", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
