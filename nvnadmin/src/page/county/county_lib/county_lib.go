package county_lib

import (
	"nvnadmin/src/app"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
)

type CountyRec struct {
	CountyId int64
	Name      string
}

func GetCountyRec(id int64) (*CountyRec, error) {
	sqlStr := `select
					county.countyId,
					county.name
				from
					county
				where
					countyId = ?`

	row := app.Db.QueryRow(sqlStr, id)

	rec := new(CountyRec)
	err := row.Scan(&rec.CountyId, &rec.Name)
	if err != nil {
		return nil, err
	}

	return rec, nil
}

func CountCounty(key string) int64 {
	sqlBuf := util.NewBuf()
	sqlBuf.Add("select count(*) from county")

	if key != "" {
		key = util.DbStr(key)
		sqlBuf.Add("where (county.name like('%%%s%%'))", key)
	}

	row := app.Db.QueryRow(*sqlBuf.String())

	var rv int64
	err := row.Scan(&rv)
	if err != nil {
		panic(err)
	}

	return rv
}

func GetCountyPage(ctx *context.Ctx, key string, pageNo int64) []*CountyRec {
	sqlBuf := util.NewBuf()
	sqlBuf.Add(`select
					county.countyId,
					county.name
				from
					county`)

	if key != "" {
		key = util.DbStr(key)
		sqlBuf.Add("where (county.name like('%%%s%%'))", key)
	}

	sqlBuf.Add("order by county.name")

	start := (pageNo - 1) * ctx.Config.PageLen
	sqlBuf.Add("limit %d, %d", start, ctx.Config.PageLen)

	rows, err := app.Db.Query(*sqlBuf.String())
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	rv := make([]*CountyRec, 0, 100)
	for rows.Next() {
		rec := new(CountyRec)
		if err = rows.Scan(&rec.CountyId, &rec.Name); err != nil {
			panic(err)
		}

		rv = append(rv, rec)
	}

	return rv
}
