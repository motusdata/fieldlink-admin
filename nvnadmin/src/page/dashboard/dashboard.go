package dashboard

import (
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
)

func Browse(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("dashboard:browse") {
		app.BadRequest()
	}

	content.Include(ctx)

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Dashboard"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"callout callout-info\">")
	buf.Add("<h3>Please be informed:</h3>")
	buf.Add("<p>This function will be implemented soon.</p>")
	buf.Add("</div>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "dashboard", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")

	ctx.ReadCargo()
}
