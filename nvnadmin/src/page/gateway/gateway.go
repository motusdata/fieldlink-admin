package gateway

import (
	"fmt"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/combo"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/ruler"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/gateway/gateway_lib"
)

func Browse(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if ctx.Req.URL.Path != "/" && ctx.Req.URL.Path != "/gateway" {
		app.NotFound()
	}

	if !ctx.IsRight("gateway:browse") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddInt("operatorId", -1)
	ctx.Cargo.AddInt("wellId", -1)
	ctx.ReadCargo()

	content.Include(ctx)

	browseMid(ctx)

	content.Default(ctx)

	content.Search(ctx, "/gateway")

	lmenu := left_menu.New()
	lmenu.Set(ctx, "gateway", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}

func browseMid(ctx *context.Ctx) {
	ctx.Include.Js("/asset/js/page/gateway/gateway.js")

	key := ctx.Cargo.Str("key")
	pageNo := ctx.Cargo.Int("pn")
	operatorId := ctx.Cargo.Int("operatorId")
	wellId := ctx.Cargo.Int("wellId")

	operatorCombo := combo.NewCombo("select operatorId, name from operator order by name", "All Operators.")
	operatorCombo.Set()

	if operatorCombo.IsEmpty() {
		ctx.Warning("Operator list is empty. You should enter at least one operator.")
	}

	var sqlStr string
	if operatorId != -1 {
		sqlStr = fmt.Sprintf("select wellId, name from well where operatorId = %d order by name", operatorId)
	} else {
		sqlStr = "select wellId, name from well order by name"
	}

	wellCombo := combo.NewCombo(sqlStr, "All Wells.")
	wellCombo.Set()

	if wellCombo.IsEmpty() {
		ctx.Warning("Well list is empty. You should enter at least one well.")
	}

	totalRows := gateway_lib.CountGateway(key, operatorId)
	if totalRows == 0 {
		ctx.Warning("Empty list.")
	}

	pageNo = ctx.TouchPageNo(pageNo, totalRows)

	insertRight := ctx.IsRight("gateway:insert")
	updateRight := ctx.IsRight("gateway:update")
	deleteRight := ctx.IsRight("gateway:delete")

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")
	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Gateways"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")

	if insertRight {
		urlStr := ctx.Cargo.HtmlUrl("/gateway_insert", "key", "pn", "operatorId")
		buf.Add("<a href=\"%s\" class=\"btn btn-primary btn-md\" title=\"New record.\">New</a>", urlStr)
	}

	//operatorId form
	buf.Add("<form id=\"operatorForm\" class=\"form form-inline\" action=\"/gateway\" method=\"get\">")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<select name=\"operatorId\" id=\"operatorId\" class=\"form-control\" title=\"Select operator\">")

	buf.Add(operatorCombo.Format(operatorId))

	buf.Add("</select>")
	buf.Add("</div>")
	buf.Add(content.HiddenCargo(ctx, "key"))
	buf.Add("</form>")

	//wellId form
	buf.Add("<form id=\"wellForm\" class=\"form form-inline\" action=\"/gateway\" method=\"get\">")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<select name=\"wellId\" id=\"wellId\" class=\"form-control\" title=\"Select well\">")

	buf.Add(wellCombo.Format(wellId))

	buf.Add("</select>")
	buf.Add("</div>")
	buf.Add(content.HiddenCargo(ctx, "key", "operatorId"))
	buf.Add("</form>")

	buf.Add("</div>") //cmd

	buf.Add("<table>")
	buf.Add("<thead>")
	buf.Add("<tr>")
	buf.Add("<th class=\"fixed-zero\">Id</th>")
	buf.Add("<th>Operator</th>")
	buf.Add("<th>Well</th>")
	buf.Add("<th>Sku</th>")
	buf.Add("<th>Mac</th>")
	buf.Add("<th>Version</th>")
	buf.Add("<th>Manufacturer</th>")

	if updateRight || deleteRight {
		buf.Add("<th class=\"cmd cmd-table\">%s</th>", "Command")
	}

	buf.Add("</tr>")
	buf.Add("</thead>")

	buf.Add("<tbody>")

	if totalRows > 0 {
		gatewayList := gateway_lib.GetGatewayPage(ctx, key, pageNo, operatorId)

		var sku, mac, version, gatewayMnfName, wellName, operatorName string
		for _, row := range gatewayList {
			ctx.Cargo.SetInt("id", row.GatewayId)

			operatorName = util.ScrStr(row.OperatorName)
			wellName = util.ScrStr(row.WellName)
			sku = util.ScrStr(row.Sku)
			mac = util.ScrStr(row.Mac)
			version = util.ScrStr(row.Version)
			gatewayMnfName = util.ScrStr(row.GatewayMnfName)

			if key != "" {
				sku = content.Find(sku, key)
				mac = content.Find(mac, key)
			}

			buf.Add("<tr>")
			buf.Add("<td>%d</td>", row.GatewayId)
			buf.Add("<td>%s</td>", operatorName)
			buf.Add("<td>%s</td>", wellName)
			buf.Add("<td>%s</td>", sku)
			buf.Add("<td>%s</td>", mac)
			buf.Add("<td>%s</td>", version)
			buf.Add("<td>%s</td>", gatewayMnfName)

			if updateRight || deleteRight {
				buf.Add("<td class=\"cmd cmd-table\">")

				if updateRight {
					urlStr := ctx.Cargo.HtmlUrl("/gateway_update", "id", "key", "pn", "operatorId")
					buf.Add("<a href=\"%s\" class=\"btn btn-default btn-sm\" title=\"Edit record.\">Edit</a>", urlStr)
				}

				if deleteRight {
					urlStr := ctx.Cargo.HtmlUrl("/gateway_delete", "id", "key", "pn", "operatorId")
					buf.Add("<a href=\"%s\" class=\"btn btn-danger btn-sm\" title=\"Delete record.\">Delete</a>", urlStr)
				}

				buf.Add("</td>")
			}

			buf.Add("</tr>")
		}
	}

	buf.Add("</tbody>")
	buf.Add("</table>")

	if totalRows > 0 {
		ruler := ruler.NewRuler(totalRows, pageNo, ctx.Cargo.HtmlUrl("/gateway", "key"))
		buf.Add(ruler.Set(ctx))
	}

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())
}
