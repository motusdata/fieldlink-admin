package gateway

import (
	"database/sql"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/gateway/gateway_lib"
)

func Delete(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("gateway:delete") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddStr("confirm", "no")
	ctx.Cargo.AddInt("operatorId", -1)
	ctx.ReadCargo()

	id := ctx.Cargo.Int("id")
	rec, err := gateway_lib.GetGatewayRec(id)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Warning("Record could not be found.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/gateway", "key", "pn", "operatorId"))
			return
		}

		panic(err)
	}

	if ctx.Cargo.Str("confirm") != "yes" {
		deleteConfirm(ctx, rec)
		return
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `delete from
					gateway
				where
					gatewayId = ?`

	res, err := tx.Exec(sqlStr, id)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	if n, _ := res.RowsAffected(); n == 0 {
		tx.Rollback()
		ctx.Warning("Record could not be found.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/gateway", "key", "pn", "operatorId"))
		return
	}

	tx.Commit()

	ctx.Success("Record has been deleted.")
	ctx.Redirect(ctx.Cargo.HtmlUrl("/gateway", "key", "pn", "operatorId"))
}

func deleteConfirm(ctx *context.Ctx, rec *gateway_lib.GatewayRec) {
	content.Include(ctx)

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Gateways", "Delete Record"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	urlStr := ctx.Cargo.HtmlUrl("/gateway", "key", "pn", "operatorId")
	buf.Add("<a href=\"%s\" class=\"btn btn-warning btn-md\"\">Back</a>", urlStr)
	buf.Add("</div>")

	buf.Add("<table>")
	buf.Add("<tbody>")
	buf.Add("<tr><th class=\"fixed-middle\">SKU:</th><td>%s</td></tr>", util.ScrStr(rec.Sku))
	buf.Add("<tr><th class=\"fixed-middle\">Mac:</th><td>%s</td></tr>", util.ScrStr(rec.Mac))

	buf.Add("</tbody>")
	buf.Add("</table>")

	buf.Add("<div class=\"callout callout-danger\">")
	buf.Add("<h3>Please confirm:</h3>")
	buf.Add("<p>Do you ready want to delete this record?</p>")
	buf.Add("</div>")

	ctx.Cargo.SetStr("confirm", "yes")
	urlStr = ctx.Cargo.HtmlUrl("/gateway_delete", "id", "key", "pn", "confirm", "operatorId")

	buf.Add("<div class=\"cmd\">")
	buf.Add("<a href=\"%s\" class=\"btn btn-danger btn-md\">Yes</a>", urlStr)
	buf.Add("</div>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "gateway", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
