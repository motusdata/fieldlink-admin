package gateway_lib

import (
	"nvnadmin/src/app"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
)

type GatewayRec struct {
	GatewayId      int64
	Sku            string
	Mac            string
	Version        string
	GatewayMnfId   int64
	GatewayMnfName string
	WellId         int64
	WellName       string
	OperatorId     int64
	OperatorName   string
}

func GetGatewayRec(id int64) (*GatewayRec, error) {
	sqlStr := `select
					gateway.gatewayId,
					gateway.sku,
					gateway.mac,
					gateway.version,
					gatewayMnf.gatewayMnfId,
					gatewayMnf.name,
					gateway.wellId,
					well.name,
					well.operatorId,
					operator.name
				from
					operator,
					well,
					gateway,
					gatewayMnf
				where
					gatewayId = ? and
					well.operatorId = operator.operatorId and
					well.wellId = gateway.wellId and
					gateway.gatewayMnfId = gatewayMnf.gatewayMnfId`

	row := app.Db.QueryRow(sqlStr, id)

	rec := new(GatewayRec)
	err := row.Scan(&rec.GatewayId, &rec.Sku, &rec.Mac, &rec.Version,
		&rec.GatewayMnfId, &rec.GatewayMnfName, &rec.WellId, &rec.WellName,
		&rec.OperatorId, &rec.OperatorName)
	if err != nil {
		return nil, err
	}

	return rec, nil
}

func CountGateway(key string, operatorId int64) int64 {
	sqlBuf := util.NewBuf()
	sqlBuf.Add(`select
					count(*)
				from
					operator,
					well,
					gateway
				where
					well.operatorId = operator.operatorId and
					gateway.wellId = well.wellId`)

	conBuf := util.NewBuf()

	if operatorId != -1 {
		conBuf.Add("(operator.operatorId = %d)", operatorId)
	}

	if key != "" {
		key = util.DbStr(key)
		sqlBuf.Add("((gateway.sku like('%%%s%%')) or"+
			"(gateway.mac like('%%%s%%')))", key, key)
	}

	if !conBuf.IsEmpty() {
		sqlBuf.Add("and")
		sqlBuf.Add(*conBuf.StringSep(" and "))
	}

	row := app.Db.QueryRow(*sqlBuf.String())

	var rv int64
	err := row.Scan(&rv)
	if err != nil {
		panic(err)
	}

	return rv
}

func GetGatewayPage(ctx *context.Ctx, key string, pageNo, operatorId int64) []*GatewayRec {
	sqlBuf := util.NewBuf()
	sqlBuf.Add(`select
					gateway.gatewayId,
					gateway.sku,
					gateway.mac,
					gateway.version,
					gatewayMnf.gatewayMnfId,
					gatewayMnf.name,
					gateway.wellId,
					well.name,
					well.operatorId,
					operator.name
				from
					operator,
					well,
					gateway,
					gatewayMnf
				where
					well.operatorId = operator.operatorId and
					well.wellId = gateway.wellId and
					gateway.gatewayMnfId = gatewayMnf.gatewayMnfId`)

	conBuf := util.NewBuf()

	if operatorId != -1 {
		conBuf.Add("(operator.operatorId = %d)", operatorId)
	}

	if key != "" {
		key = util.DbStr(key)
		sqlBuf.Add("((gateway.sku like('%%%s%%')) or"+
			"(gateway.mac like('%%%s%%')))", key, key)
	}

	if !conBuf.IsEmpty() {
		sqlBuf.Add("and")
		sqlBuf.Add(*conBuf.StringSep(" and "))
	}

	sqlBuf.Add("order by gateway.sku")

	start := (pageNo - 1) * ctx.Config.PageLen
	sqlBuf.Add("limit %d, %d", start, ctx.Config.PageLen)

	rows, err := app.Db.Query(*sqlBuf.String())
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	rv := make([]*GatewayRec, 0, 100)
	for rows.Next() {
		rec := new(GatewayRec)
		err = rows.Scan(&rec.GatewayId, &rec.Sku, &rec.Mac, &rec.Version,
			&rec.GatewayMnfId, &rec.GatewayMnfName, &rec.WellId, &rec.WellName,
			&rec.OperatorId, &rec.OperatorName)

		if err != nil {
			panic(err)
		}

		rv = append(rv, rec)
	}

	return rv
}
