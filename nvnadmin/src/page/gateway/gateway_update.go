package gateway

import (
	"database/sql"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/combo"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/gateway/gateway_lib"
	"strconv"
)

func Update(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("gateway:update") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddInt("operatorId", -1)
	ctx.ReadCargo()

	id := ctx.Cargo.Int("id")
	rec, err := gateway_lib.GetGatewayRec(id)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Warning("Record could not be found.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/gateway", "key", "pn", "operatorId"))
			return
		}

		panic(err)
	}

	if ctx.Req.Method == "GET" {
		updateForm(ctx, rec)
		return
	}

	formWellId, err := strconv.ParseInt(ctx.Req.PostFormValue("formWellId"), 10, 64)
	if err != nil {
		formWellId = -1
	}

	gatewayMnfId, err := strconv.ParseInt(ctx.Req.PostFormValue("gatewayMnfId"), 10, 64)
	if err != nil {
		formWellId = -1
	}

	sku := ctx.Req.PostFormValue("sku")
	mac := ctx.Req.PostFormValue("mac")
	version := ctx.Req.PostFormValue("version")

	if formWellId == -1 || gatewayMnfId == -1 || sku == "" || mac == "" || version == "" {
		ctx.Warning("You have left one or more fields empty.")
		updateForm(ctx, rec)
		return
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `update gateway set
					wellId = ?,
					gatewayMnfId = ?,
					sku = ?,
					mac = ?,
					version = ?
				where
					gatewayId = ?`

	res, err := tx.Exec(sqlStr, formWellId, gatewayMnfId, sku, mac, version, id)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	if n, _ := res.RowsAffected(); n == 0 {
		tx.Rollback()
		ctx.Warning("You did not change the record.")
		updateForm(ctx, rec)
		return
	}

	tx.Commit()

	ctx.Success("Record has been changed.")
	ctx.Redirect(ctx.Cargo.HtmlUrl("/gateway", "key", "pn", "operatorId"))
}

func updateForm(ctx *context.Ctx, rec *gateway_lib.GatewayRec) {
	content.Include(ctx)

	var sku, mac, version string
	var formWellId, gatewayMnfId int64
	var err error

	if ctx.Req.Method == "POST" {
		formWellId, err = strconv.ParseInt(ctx.Req.PostFormValue("formWellId"), 10, 64)
		if err != nil {
			formWellId = -1
		}

		gatewayMnfId, err = strconv.ParseInt(ctx.Req.PostFormValue("gatewayMnfId"), 10, 64)
		if err != nil {
			formWellId = -1
		}

		sku = ctx.Req.PostFormValue("sku")
		mac = ctx.Req.PostFormValue("mac")
		version = ctx.Req.PostFormValue("version")
	} else {
		formWellId = rec.WellId
		gatewayMnfId = rec.GatewayId
		sku = rec.Sku
		mac = rec.Mac
		version = rec.Version
	}

	wellCombo := combo.NewCombo("select wellId, name from well order by name", "Select Well")
	wellCombo.Set()

	if wellCombo.IsEmpty() {
		ctx.Warning("Well table is empty. You should enter at least one well first.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/gateway", "key", "pn", "operatorId"))
		return
	}

	gatewayMnfCombo := combo.NewCombo("select gatewayMnfId, name from gatewayMnf order by name", "Select Manufacturer")
	gatewayMnfCombo.Set()

	if gatewayMnfCombo.IsEmpty() {
		ctx.Warning("Manufacturer table is empty. You should enter at least one manufacturer first.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/gateway", "key", "pn", "operatorId"))
		return
	}

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Gateways", "Update Record"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	urlStr := ctx.Cargo.HtmlUrl("/gateway", "key", "pn", "operatorId")
	buf.Add("<a href=\"%s\" class=\"btn btn-warning btn-md\"\">Back</a>", urlStr)
	buf.Add("</div>")

	urlStr = ctx.Cargo.HtmlUrl("/gateway_update", "id", "key", "pn", "operatorId")
	buf.Add("<form action=\"%s\" method=\"post\">", urlStr)

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"formWellId\">Well:</label>")
	buf.Add("<select name=\"formWellId\" id=\"formWellId\" class=\"form-control\" tabindex=\"1\" autofocus>")

	buf.Add(wellCombo.Format(formWellId))

	buf.Add("</select>")
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"gatewayMnfId\">Manufacturer:</label>")
	buf.Add("<select name=\"gatewayMnfId\" id=\"gatewayMnfId\" class=\"form-control\" tabindex=\"2\">")

	buf.Add(gatewayMnfCombo.Format(gatewayMnfId))

	buf.Add("</select>")
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"sku\">SKU:</label>")
	buf.Add("<input type=\"text\" name=\"sku\" id=\"sku\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"50\" tabindex=\"3\">", util.ScrStr(sku))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"mac\">Mac:</label>")
	buf.Add("<input type=\"text\" name=\"mac\" id=\"mac\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"50\" tabindex=\"4\">", util.ScrStr(mac))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"version\">Version:</label>")
	buf.Add("<input type=\"text\" name=\"version\" id=\"version\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"50\" tabindex=\"5\">", util.ScrStr(version))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<button type=\"submit\" class=\"btn btn-primary\" tabindex=\"6\">Submit</button>")
	buf.Add("<button type=\"reset\" class=\"btn btn-default\" tabindex=\"7\">Reset</button>")
	buf.Add("</div>")

	buf.Add("</form>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "gateway", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
