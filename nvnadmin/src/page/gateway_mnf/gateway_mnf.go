package gateway_mnf

import (
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/ruler"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/gateway_mnf/gateway_mnf_lib"
)

func Browse(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if ctx.Req.URL.Path != "/" && ctx.Req.URL.Path != "/gateway_mnf" {
		app.NotFound()
	}

	if !ctx.IsRight("gateway_mnf:browse") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.ReadCargo()

	content.Include(ctx)

	browseMid(ctx)

	content.Default(ctx)

	content.Search(ctx, "/gateway_mnf")

	lmenu := left_menu.New()
	lmenu.Set(ctx, "gateway_mnf", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}

func browseMid(ctx *context.Ctx) {
	key := ctx.Cargo.Str("key")
	pageNo := ctx.Cargo.Int("pn")

	totalRows := gateway_mnf_lib.CountGatewayMnf(key)
	if totalRows == 0 {
		ctx.Warning("Empty list.")
	}

	pageNo = ctx.TouchPageNo(pageNo, totalRows)

	insertRight := ctx.IsRight("gateway_mnf:insert")
	updateRight := ctx.IsRight("gateway_mnf:update")
	deleteRight := ctx.IsRight("gateway_mnf:delete")

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")
	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Gateway Manufacturers"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	if insertRight || updateRight {
		buf.Add("<div class=\"cmd\">")

		if insertRight {
			urlStr := ctx.Cargo.HtmlUrl("/gateway_mnf_insert", "key", "pn")
			buf.Add("<a href=\"%s\" class=\"btn btn-primary btn-md\" title=\"New record.\">New</a>", urlStr)
		}

		buf.Add("<span id=\"lastUpdate\"></span>")

		buf.Add("</div>")
	}

	buf.Add("<table>")
	buf.Add("<thead>")
	buf.Add("<tr>")
	buf.Add("<th class=\"fixed-middle\">Name</th>")

	if updateRight || deleteRight {
		buf.Add("<th class=\"cmd cmd-table\">%s</th>", "Command")
	}

	buf.Add("</tr>")
	buf.Add("</thead>")

	buf.Add("<tbody>")

	if totalRows > 0 {
		gatewayMnfList := gateway_mnf_lib.GetGatewayMnfPage(ctx, key, pageNo)

		var name string
		for _, row := range gatewayMnfList {
			ctx.Cargo.SetInt("id", row.GatewayMnfId)

			name = util.ScrStr(row.Name)

			if key != "" {
				name = content.Find(name, key)
			}

			buf.Add("<tr>")
			buf.Add("<td>%s</td>", name)

			if updateRight || deleteRight {
				buf.Add("<td class=\"cmd cmd-table\">")

				if updateRight {
					urlStr := ctx.Cargo.HtmlUrl("/gateway_mnf_update", "id", "key", "pn")
					buf.Add("<a href=\"%s\" class=\"btn btn-default btn-sm\" title=\"Edit record.\">Edit</a>", urlStr)
				}

				if deleteRight {
					urlStr := ctx.Cargo.HtmlUrl("/gateway_mnf_delete", "id", "key", "pn")
					buf.Add("<a href=\"%s\" class=\"btn btn-danger btn-sm\" title=\"Delete record.\">Delete</a>", urlStr)
				}

				buf.Add("</td>")
			}

			buf.Add("</tr>")
		}
	}

	buf.Add("</tbody>")
	buf.Add("</table>")

	if totalRows > 0 {
		ruler := ruler.NewRuler(totalRows, pageNo, ctx.Cargo.HtmlUrl("/gateway_mnf", "key"))
		buf.Add(ruler.Set(ctx))
	}

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())
}
