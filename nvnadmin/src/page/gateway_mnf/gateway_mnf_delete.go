package gateway_mnf

import (
	"database/sql"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/gateway_mnf/gateway_mnf_lib"
)

func Delete(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("gateway_mnf:delete") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddStr("confirm", "no")
	ctx.ReadCargo()

	id := ctx.Cargo.Int("id")
	rec, err := gateway_mnf_lib.GetGatewayMnfRec(id)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Warning("Record could not be found.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/gateway_mnf", "key", "pn"))
			return
		}

		panic(err)
	}

	if ctx.Cargo.Str("confirm") != "yes" {
		deleteConfirm(ctx, rec)
		return
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `delete from
					gatewayMnf
				where
					gatewayMnfId = ?`

	res, err := tx.Exec(sqlStr, id)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	if n, _ := res.RowsAffected(); n == 0 {
		tx.Rollback()
		ctx.Warning("Record could not be found.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/gateway_mnf", "key", "pn"))
		return
	}

	tx.Commit()

	ctx.Success("Record has been deleted.")
	ctx.Redirect(ctx.Cargo.HtmlUrl("/gateway_mnf", "key", "pn"))
}

func deleteConfirm(ctx *context.Ctx, rec *gateway_mnf_lib.GatewayMnfRec) {
	content.Include(ctx)

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Gateway Manufacturers", "Delete Record"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	urlStr := ctx.Cargo.HtmlUrl("/gateway_mnf", "key", "pn")
	buf.Add("<a href=\"%s\" class=\"btn btn-warning btn-md\"\">Back</a>", urlStr)
	buf.Add("</div>")

	buf.Add("<table>")
	buf.Add("<tbody>")
	buf.Add("<tr><th class=\"fixed-middle\">Name:</th><td>%s</td></tr>", util.ScrStr(rec.Name))

	buf.Add("</tbody>")
	buf.Add("</table>")

	buf.Add("<div class=\"callout callout-danger\">")
	buf.Add("<h3>Please confirm:</h3>")
	buf.Add("<p>Do you ready want to delete this record?</p>")
	buf.Add("</div>")

	ctx.Cargo.SetStr("confirm", "yes")
	urlStr = ctx.Cargo.HtmlUrl("/gateway_mnf_delete", "id", "key", "pn", "confirm")

	buf.Add("<div class=\"cmd\">")
	buf.Add("<a href=\"%s\" class=\"btn btn-danger btn-md\">Yes</a>", urlStr)
	buf.Add("</div>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "gateway_mnf", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
