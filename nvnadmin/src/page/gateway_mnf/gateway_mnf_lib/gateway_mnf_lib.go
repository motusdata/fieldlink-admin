package gateway_mnf_lib

import (
	"nvnadmin/src/app"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
)

type GatewayMnfRec struct {
	GatewayMnfId int64
	Name         string
}

func GetGatewayMnfRec(id int64) (*GatewayMnfRec, error) {
	sqlStr := `select
					gatewayMnf.gatewayMnfId,
					gatewayMnf.name
				from
					gatewayMnf
				where
					gatewayMnfId = ?`

	row := app.Db.QueryRow(sqlStr, id)

	rec := new(GatewayMnfRec)
	err := row.Scan(&rec.GatewayMnfId, &rec.Name)
	if err != nil {
		return nil, err
	}

	return rec, nil
}

func CountGatewayMnf(key string) int64 {
	sqlBuf := util.NewBuf()
	sqlBuf.Add("select count(*) from gatewayMnf")

	if key != "" {
		key = util.DbStr(key)
		sqlBuf.Add("where (gatewayMnf.name like('%%%s%%'))", key)
	}

	row := app.Db.QueryRow(*sqlBuf.String())

	var rv int64
	err := row.Scan(&rv)
	if err != nil {
		panic(err)
	}

	return rv
}

func GetGatewayMnfPage(ctx *context.Ctx, key string, pageNo int64) []*GatewayMnfRec {
	sqlBuf := util.NewBuf()
	sqlBuf.Add(`select
					gatewayMnf.gatewayMnfId,
					gatewayMnf.name
				from
					gatewayMnf`)

	if key != "" {
		key = util.DbStr(key)
		sqlBuf.Add("where (gatewayMnf.name like('%%%s%%'))", key)
	}

	sqlBuf.Add("order by gatewayMnf.name")

	start := (pageNo - 1) * ctx.Config.PageLen
	sqlBuf.Add("limit %d, %d", start, ctx.Config.PageLen)

	rows, err := app.Db.Query(*sqlBuf.String())
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	rv := make([]*GatewayMnfRec, 0, 100)
	for rows.Next() {
		rec := new(GatewayMnfRec)
		if err = rows.Scan(&rec.GatewayMnfId, &rec.Name); err != nil {
			panic(err)
		}

		rv = append(rv, rec)
	}

	return rv
}
