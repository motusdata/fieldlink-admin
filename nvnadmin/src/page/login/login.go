package login

import (
	"database/sql"
	"net/http"
	"nvnadmin/src/content"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/user/user_lib"

	"github.com/dchest/captcha"
)

func Browse(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if ctx.IsLoggedIn() {
		ctx.Warning("You are already logged in.")
		ctx.Redirect("/welcome")
		return
	}

	if ctx.Req.Method == "GET" {
		loginForm(ctx)
		return
	}

	login := ctx.Req.PostFormValue("login")
	password := ctx.Req.PostFormValue("password")
	captchaId := ctx.Req.PostFormValue("captchaId")
	captchaAnswer := ctx.Req.PostFormValue("captchaAnswer")

	if login == "" || password == "" {
		ctx.Warning("You have left one or more fields empty.")
		loginForm(ctx)
		return
	}

	if !captcha.VerifyString(captchaId, captchaAnswer) {
		ctx.Warning("Wrong security answer. Please try again.")
		loginForm(ctx)
		return
	}

	password = util.PasswordHash(password)
	rec, err := user_lib.GetUserRecByLogin(login, password)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Warning("Wrong user name or password. Please try again.")
			loginForm(ctx)
			return
		}

		panic(err)
	}

	if rec.Status == "blocked" {
		ctx.Warning("Your account is not active. Please contact administrator to have your account activated.")
		loginForm(ctx)
		return
	}

	ctx.Session.UserId = rec.UserId
	ctx.CreateSession()
	ctx.Redirect("/welcome")
}

func loginForm(ctx *context.Ctx) {
	content.Include(ctx)

	var login, password string
	if ctx.Req.Method == "POST" {
		login = ctx.Req.PostFormValue("login")
		password = ctx.Req.PostFormValue("password")
	} else {
		login = ""
		password = ""
	}

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")
	buf.Add("<div class=\"panel-image\">")
	buf.Add("<img src=\"/asset/img/noven_logo.jpeg\" alt=\"\">")
	buf.Add("</div>")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Noven Admin Portal Login"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<form action=\"/login\" method=\"post\">")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"login\">Login Name:</label>")
	buf.Add("<input type=\"text\" name=\"login\" id=\"login\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"100\" tabindex=\"1\" autofocus>", util.ScrStr(login))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"password\">Password:</label>")
	buf.Add("<input type=\"password\" name=\"password\" id=\"password\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"30\" tabindex=\"2\">", util.ScrStr(password))
	buf.Add("</div>")

	captchaId := captcha.NewLen(4)

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"captchaAnswer\">Security Question:</label>")
	buf.Add("<input type=\"text\" name=\"captchaAnswer\" id=\"captchaAnswer\"" +
		" class=\"form-control\" value=\"\" maxlength=\"10\" tabindex=\"3\">")
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<p><img src=\"/captcha/%s.png\" alt=\"Captcha image.\"></p>", captchaId)
	buf.Add("<input type=hidden name=captchaId value=\"%s\">", captchaId)
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<button type=\"submit\" class=\"btn btn-primary\" tabindex=\"4\">Submit</button>")
	buf.Add("<button type=\"reset\" class=\"btn btn-default\" tabindex=\"5\">Reset</button>")
	buf.Add("</div>")

	buf.Add("</form>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	str := "loginPage"
	ctx.AddFormat("pageId", &str)

	content.Default(ctx)

	ctx.Render("login.html")
}
