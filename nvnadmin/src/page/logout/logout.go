package logout

import (
	"fmt"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/lib/context"
)

func Browse(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsLoggedIn() {
		app.BadRequest()
	}

	ctx.DeleteSession()
	content.End(ctx, "success", "Goodbye", app.SiteName,
		fmt.Sprintf("<p>%s</p>", "Your session has been closed."),
		fmt.Sprintf("<p><a href=\"/login\">Please login.</a></p>"))
}
