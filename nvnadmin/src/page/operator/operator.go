package operator

import (
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/ruler"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/operator/operator_lib"
)

func Browse(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if ctx.Req.URL.Path != "/" && ctx.Req.URL.Path != "/operator" {
		app.NotFound()
	}

	if !ctx.IsRight("operator:browse") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.ReadCargo()

	content.Include(ctx)

	browseMid(ctx)

	content.Default(ctx)

	content.Search(ctx, "/operator")

	lmenu := left_menu.New()
	lmenu.Set(ctx, "operator", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}

func browseMid(ctx *context.Ctx) {
	key := ctx.Cargo.Str("key")
	pageNo := ctx.Cargo.Int("pn")

	totalRows := operator_lib.CountOperator(key)
	if totalRows == 0 {
		ctx.Warning("Empty list.")
	}

	pageNo = ctx.TouchPageNo(pageNo, totalRows)

	insertRight := ctx.IsRight("operator:insert")
	updateRight := ctx.IsRight("operator:update")
	deleteRight := ctx.IsRight("operator:delete")

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")
	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Operators"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	if insertRight || updateRight {
		buf.Add("<div class=\"cmd\">")

		if insertRight {
			urlStr := ctx.Cargo.HtmlUrl("/operator_insert", "key", "pn")
			buf.Add("<a href=\"%s\" class=\"btn btn-primary btn-md\" title=\"New record.\">New</a>", urlStr)
		}

		buf.Add("<span id=\"lastUpdate\"></span>")

		buf.Add("</div>")
	}

	buf.Add("<table>")
	buf.Add("<thead>")
	buf.Add("<tr>")
	buf.Add("<th class=\"fixed-middle\">Name</th>")
	buf.Add("<th class=\"fixed-zero\">Status</th>")

	if updateRight || deleteRight {
		buf.Add("<th class=\"cmd cmd-table\">%s</th>", "Command")
	}

	buf.Add("</tr>")
	buf.Add("</thead>")

	buf.Add("<tbody>")

	if totalRows > 0 {
		operatorList := operator_lib.GetOperatorPage(ctx, key, pageNo)

		var name, status string
		for _, row := range operatorList {
			ctx.Cargo.SetInt("id", row.OperatorId)

			name = util.ScrStr(row.Name)
			status = util.ScrStr(row.Status)

			if key != "" {
				name = content.Find(name, key)
			}

			buf.Add("<tr>")
			buf.Add("<td>%s</td>", name)

			buf.Add("<td>%s</td>", operator_lib.StatusToLabel(status))

			if updateRight || deleteRight {
				buf.Add("<td class=\"cmd cmd-table\">")

				if updateRight {
					urlStr := ctx.Cargo.HtmlUrl("/operator_update", "id", "key", "pn")
					buf.Add("<a href=\"%s\" class=\"btn btn-default btn-sm\" title=\"Edit record.\">Edit</a>", urlStr)
				}

				if deleteRight {
					urlStr := ctx.Cargo.HtmlUrl("/operator_delete", "id", "key", "pn")
					buf.Add("<a href=\"%s\" class=\"btn btn-danger btn-sm\" title=\"Delete record.\">Delete</a>", urlStr)
				}

				buf.Add("</td>")
			}

			buf.Add("</tr>")
		}
	}

	buf.Add("</tbody>")
	buf.Add("</table>")

	if totalRows > 0 {
		ruler := ruler.NewRuler(totalRows, pageNo, ctx.Cargo.HtmlUrl("/operator", "key"))
		buf.Add(ruler.Set(ctx))
	}

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())
}
