package operator_lib

import (
	"nvnadmin/src/app"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
)

type OperatorRec struct {
	OperatorId int64
	Name       string
	Status     string
}

func GetOperatorRec(id int64) (*OperatorRec, error) {
	sqlStr := `select
					operator.operatorId,
					operator.name,
					operator.status
				from
					operator
				where
					operatorId = ?`

	row := app.Db.QueryRow(sqlStr, id)

	rec := new(OperatorRec)
	err := row.Scan(&rec.OperatorId, &rec.Name, &rec.Status)
	if err != nil {
		return nil, err
	}

	return rec, nil
}

func CountOperator(key string) int64 {
	sqlBuf := util.NewBuf()
	sqlBuf.Add("select count(*) from operator")

	if key != "" {
		key = util.DbStr(key)
		sqlBuf.Add("where (operator.name like('%%%s%%'))", key)
	}

	row := app.Db.QueryRow(*sqlBuf.String())

	var rv int64
	err := row.Scan(&rv)
	if err != nil {
		panic(err)
	}

	return rv
}

func GetOperatorPage(ctx *context.Ctx, key string, pageNo int64) []*OperatorRec {
	sqlBuf := util.NewBuf()
	sqlBuf.Add(`select
					operator.operatorId,
					operator.name,
					operator.status
				from
					operator`)

	if key != "" {
		key = util.DbStr(key)
		sqlBuf.Add("where (operator.name like('%%%s%%'))", key)
	}

	sqlBuf.Add("order by operator.name")

	start := (pageNo - 1) * ctx.Config.PageLen
	sqlBuf.Add("limit %d, %d", start, ctx.Config.PageLen)

	rows, err := app.Db.Query(*sqlBuf.String())
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	rv := make([]*OperatorRec, 0, 100)
	for rows.Next() {
		rec := new(OperatorRec)
		if err = rows.Scan(&rec.OperatorId, &rec.Name, &rec.Status); err != nil {
			panic(err)
		}

		rv = append(rv, rec)
	}

	return rv
}

func StatusToLabel(status string) string {
	switch status {
	case "active":
		return "<span class=\"label label-success label-sm\">Active</span>"
	case "passive":
		return "<span class=\"label label-danger label-sm\">Passive</span>"
	}

	return "<span class=\"label label-default label-sm\">Unknown</span>"
}
