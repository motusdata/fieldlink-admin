package operator

import (
	"database/sql"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/combo"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/operator/operator_lib"
)

func Update(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("operator:update") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.ReadCargo()

	id := ctx.Cargo.Int("id")
	rec, err := operator_lib.GetOperatorRec(id)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Warning("Record could not be found.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/operator", "key", "pn"))
			return
		}

		panic(err)
	}

	if ctx.Req.Method == "GET" {
		updateForm(ctx, rec)
		return
	}

	name := ctx.Req.PostFormValue("name")
	formStatus := ctx.Req.PostFormValue("formStatus")

	if name == "" || formStatus == "default" {
		ctx.Warning("You have left one or more fields empty.")
		updateForm(ctx, rec)
		return
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `update operator set
					name = ?,
					status = ?
				where
					operatorId = ?`

	res, err := tx.Exec(sqlStr, name, formStatus, id)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	if n, _ := res.RowsAffected(); n == 0 {
		tx.Rollback()
		ctx.Warning("You did not change the record.")
		updateForm(ctx, rec)
		return
	}

	tx.Commit()

	ctx.Success("Record has been changed.")
	ctx.Redirect(ctx.Cargo.HtmlUrl("/operator", "key", "pn"))
}

func updateForm(ctx *context.Ctx, rec *operator_lib.OperatorRec) {
	content.Include(ctx)

	var name, formStatus string
	if ctx.Req.Method == "POST" {
		name = ctx.Req.PostFormValue("name")
		formStatus = ctx.Req.PostFormValue("formStatus")
	} else {
		name = rec.Name
		formStatus = rec.Status
	}

	statusCombo := combo.NewEnumCombo()
	statusCombo.Add("default", "Select Status")
	statusCombo.Add("active", "Active")
	statusCombo.Add("passive", "Passive")

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Operator", "Update Record"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	urlStr := ctx.Cargo.HtmlUrl("/operator", "key", "pn")
	buf.Add("<a href=\"%s\" class=\"btn btn-warning btn-md\"\">Back</a>", urlStr)
	buf.Add("</div>")

	urlStr = ctx.Cargo.HtmlUrl("/operator_update", "id", "key", "pn")
	buf.Add("<form action=\"%s\" method=\"post\">", urlStr)

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"name\">Name:</label>")
	buf.Add("<input type=\"text\" name=\"name\" id=\"name\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"50\" tabindex=\"1\" autofocus>", util.ScrStr(name))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"formStatus\">Status:</label>")
	buf.Add("<select name=\"formStatus\" id=\"formStatus\" class=\"form-control\" tabindex=\"2\">")

	buf.Add(statusCombo.Format(formStatus))

	buf.Add("</select>")
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<button type=\"submit\" class=\"btn btn-primary\" tabindex=\"3\">Submit</button>")
	buf.Add("<button type=\"reset\" class=\"btn btn-default\" tabindex=\"4\">Reset</button>")
	buf.Add("</div>")

	buf.Add("</form>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "operator", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
