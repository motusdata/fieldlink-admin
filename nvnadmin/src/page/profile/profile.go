package profile

import (
	"database/sql"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/user/user_lib"
)

func Browse(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("profile:browse") {
		app.BadRequest()
	}

	rec, err := user_lib.GetUserRec(ctx.User.UserId)
	if err != nil {
		if err == sql.ErrNoRows {
			panic("User record could not be found.")
		}

		panic(err)
	}

	displayProfile(ctx, rec)
}

func displayProfile(ctx *context.Ctx, rec *user_lib.UserRec) {

	buf := util.NewBuf()

	updateRight := ctx.IsRight("profile:update")
	updatePasswordRight := ctx.IsRight("profile:update_password")

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("User Profile"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	if updateRight || updatePasswordRight {
		buf.Add("<div class=\"cmd\">")

		if updateRight {
			urlStr := "/profile_update"
			buf.Add("<a href=\"%s\" class=\"btn btn-default btn-md\" title=\"Edit record.\">Edit</a>", urlStr)
		}

		if updatePasswordRight {
			urlStr := "/profile_update_pass"
			buf.Add("<a href=\"%s\" class=\"btn btn-default btn-md\" title=\"Change user password.\">Change Password</a>", urlStr)
		}

		buf.Add("</div>")
	}

	buf.Add("<table>")
	buf.Add("<tbody>")
	buf.Add("<tr><th class=\"fixed-middle\">User Name:</th><td>%s</td></tr>", util.ScrStr(rec.Name))
	buf.Add("<tr><th>Login Name:</th><td>%s</td></tr>", util.ScrStr(rec.Login))
	buf.Add("<tr><th>Email:</th><td>%s</td></tr>", util.ScrStr(rec.Email))

	buf.Add("</tbody>")
	buf.Add("</table>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Include(ctx)
	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "root", false)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "profile", true)

	ctx.Render("default.html")
}
