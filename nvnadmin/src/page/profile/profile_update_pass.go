package profile

import (
	"database/sql"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/user/user_lib"
)

func UpdatePass(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("profile:update_password") {
		app.BadRequest()
	}

	rec, err := user_lib.GetUserRec(ctx.User.UserId)
	if err != nil {
		if err == sql.ErrNoRows {
			panic("User record could not be found.")
		}

		panic(err)
	}

	if ctx.Req.Method == "GET" {
		updatePassForm(ctx, rec)
		return
	}

	cur_password := ctx.Req.PostFormValue("cur_password")
	new_password := ctx.Req.PostFormValue("new_password")
	re_new_password := ctx.Req.PostFormValue("re_new_password")

	if cur_password == "" || new_password == "" || re_new_password == "" {
		ctx.Warning("You have left one or more fields empty.")
		updatePassForm(ctx, rec)
		return
	}

	if util.PasswordHash(cur_password) != rec.Password {
		ctx.Warning("You have entered wrong current password. Please try again.")
		updatePassForm(ctx, rec)
		return
	}

	if err := util.IsValidPassword(new_password); err != nil {
		ctx.Warning(err.Error())
		updatePassForm(ctx, rec)
		return
	}

	if new_password == cur_password {
		ctx.Warning("You have entered your old password as new password.")
		updatePassForm(ctx, rec)
		return
	}

	if new_password != re_new_password {
		ctx.Warning("New password and retyped new password mismatch.")
		updatePassForm(ctx, rec)
		return
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	new_password = util.PasswordHash(new_password)
	sqlStr := `update user set
					password = ?
				where
					userId = ?`

	_, err = tx.Exec(sqlStr, new_password, ctx.User.UserId)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	tx.Commit()

	ctx.Success("Record has been changed.")
	ctx.Redirect(ctx.Cargo.HtmlUrl("/profile"))
}

func updatePassForm(ctx *context.Ctx, rec *user_lib.UserRec) {
	content.Include(ctx)

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("User Profile", "Update Password"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	buf.Add("<a href=\"/profile\" class=\"btn btn-warning btn-md\"\">Back</a>")
	buf.Add("</div>")

	urlStr := ctx.Cargo.HtmlUrl("/profile_update_pass")
	buf.Add("<form action=\"%s\" method=\"post\">", urlStr)

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"cur_password\">Current Password:</label>")
	buf.Add("<input type=\"password\" name=\"cur_password\" id=\"cur_password\"" +
		" class=\"form-control\" value=\"\" maxlength=\"30\" tabindex=\"1\" autofocus>")
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"new_password\">New Password:</label>")
	buf.Add("<input type=\"password\" name=\"new_password\" id=\"new_password\"" +
		" class=\"form-control\" value=\"\" maxlength=\"30\" tabindex=\"2\">")
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"re_new_password\">Retype New Password:</label>")
	buf.Add("<input type=\"password\" name=\"re_new_password\" id=\"re_new_password\"" +
		" class=\"form-control\" value=\"\" maxlength=\"30\" tabindex=\"3\">")
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<button type=\"submit\" class=\"btn btn-primary\" tabindex=\"4\">Submit</button>")
	buf.Add("<button type=\"reset\" class=\"btn btn-default\" tabindex=\"5\">Reset</button>")
	buf.Add("</div>")

	buf.Add("</form>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "root", false)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "profile", true)

	ctx.Render("default.html")
}
