package role

import (
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/role/role_lib"
)

func Browse(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("role:browse") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.ReadCargo()

	browseMid(ctx)

	content.Include(ctx)
	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "role", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}

func browseMid(ctx *context.Ctx) {
	roleList := role_lib.GetRoleList()

	if len(roleList) == 0 {
		ctx.Warning("Empty list.")
	}

	insertRight := ctx.IsRight("role:insert")
	updateRight := ctx.IsRight("role:update")
	deleteRight := ctx.IsRight("role:delete")
	roleRightRight := ctx.IsRight("role:role_right")

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")
	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Roles"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	if insertRight {
		buf.Add("<div class=\"cmd\">")
		urlStr := ctx.Cargo.HtmlUrl("/role_insert")
		buf.Add("<a href=\"%s\" class=\"btn btn-primary btn-md\" title=\"New record.\">New</a>", urlStr)
		buf.Add("</div>")
	}

	buf.Add("<table>")
	buf.Add("<thead>")
	buf.Add("<tr>")
	buf.Add("<th>Role Name</th>")
	buf.Add("<th>Explanation</th>")

	if updateRight || deleteRight || roleRightRight {
		buf.Add("<th class=\"cmd cmd-table\">Command</th>")
	}

	buf.Add("</tr>")
	buf.Add("</thead>")

	buf.Add("<tbody>")

	var name, exp string
	for _, row := range roleList {
		ctx.Cargo.SetInt("id", row.RoleId)

		name = util.ScrStr(row.Name)
		exp = util.ScrStr(row.Exp)

		buf.Add("<tr>")
		buf.Add("<td>%s</td>", name)
		buf.Add("<td>%s</td>", exp)

		if updateRight || deleteRight || roleRightRight {
			buf.Add("<td class=\"cmd cmd-table\">")

			if updateRight {
				urlStr := ctx.Cargo.HtmlUrl("/role_update", "id")
				buf.Add("<a href=\"%s\" class=\"btn btn-default btn-sm\" title=\"Edit record.\">Edit</a>", urlStr)
			}

			if roleRightRight {
				urlStr := ctx.Cargo.HtmlUrl("/role_right", "id")
				buf.Add("<a href=\"%s\" class=\"btn btn-default btn-sm\" title=\"Edit role rights\">Role Rights</a>", urlStr)
			}

			if deleteRight {
				urlStr := ctx.Cargo.HtmlUrl("/role_delete", "id")
				buf.Add("<a href=\"%s\" class=\"btn btn-danger btn-sm\" title=\"Delete record.\">Delete</a>", urlStr)
			}

			buf.Add("</td>")
		}

		buf.Add("</tr>")
	}

	buf.Add("</tbody>")
	buf.Add("</table>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())
}
