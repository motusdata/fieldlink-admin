package role

import (
	"database/sql"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/role/role_lib"

	"github.com/go-sql-driver/mysql"
)

func Delete(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("role:delete") {
		app.BadRequest()
	}

	ctx.Cargo.AddStr("confirm", "no")
	ctx.Cargo.AddInt("id", -1)
	ctx.ReadCargo()

	id := ctx.Cargo.Int("id")
	rec, err := role_lib.GetRoleRec(id)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Warning("Record could not be found.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/role"))
			return
		}

		panic(err)
	}

	if (rec.Name == "Admin" || rec.Name == "Tahminci") && !ctx.IsSuperUser() {
		ctx.Warning("Role 'Admin' can not be deleted.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/role"))
		return
	}

	if ctx.Cargo.Str("confirm") != "yes" {
		deleteConfirm(ctx, rec)
		return
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `delete from
					role
				where
					roleId = ?`

	res, err := tx.Exec(sqlStr, id)
	if err != nil {
		tx.Rollback()

		if err, ok := err.(*mysql.MySQLError); ok {
			if err.Number == 1451 {
				ctx.Warning("Could not delete parent record.")
				ctx.Redirect("/role")
				return
			}
		}

		panic(err)
	}

	if n, _ := res.RowsAffected(); n == 0 {
		tx.Rollback()
		ctx.Warning("Record could not be found.")
		ctx.Redirect("/role")
		return
	}

	tx.Commit()

	ctx.Success("Record has been deleted.")
	ctx.Redirect("/role")
}

func deleteConfirm(ctx *context.Ctx, rec *role_lib.RoleRec) {
	content.Include(ctx)

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Roles", "Delete Record"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	buf.Add("<a href=\"/role\" class=\"btn btn-warning btn-md\"\">Back</a>")
	buf.Add("</div>")

	buf.Add("<table>")
	buf.Add("<tbody>")
	buf.Add("<tr><th class=\"fixed-middle\">Role Name:</th><td>%s</td></tr>", util.ScrStr(rec.Name))
	buf.Add("<tr><th>Explanation:</th><td>%s</td></tr>", util.ScrStr(rec.Exp))
	buf.Add("</tbody>")
	buf.Add("</table>")

	buf.Add("<div class=\"callout callout-danger\">")
	buf.Add("<h3>Please confirm:</h3>")
	buf.Add("<p>Do you ready want to delete this record?</p>")
	buf.Add("</div>")

	ctx.Cargo.SetStr("confirm", "yes")
	urlStr := ctx.Cargo.HtmlUrl("/role_delete", "id", "confirm")

	buf.Add("<div class=\"cmd\">")
	buf.Add("<a href=\"%s\" class=\"btn btn-danger btn-md\">Yes</a>", urlStr)
	buf.Add("</div>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "role", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
