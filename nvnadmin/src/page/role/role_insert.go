package role

import (
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"

	"github.com/go-sql-driver/mysql"
)

func Insert(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("role:insert") {
		app.BadRequest()
	}

	ctx.Cargo.AddStr("key", "")
	ctx.ReadCargo()

	if ctx.Req.Method == "GET" {
		insertForm(ctx)
		return
	}

	name := ctx.Req.PostFormValue("name")
	exp := ctx.Req.PostFormValue("exp")

	if name == "" || exp == "" {
		ctx.Warning("You have left one or more fields empty.")
		insertForm(ctx)
		return
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `insert into
					role(roleId, name, exp)
					values(null, ?, ?)`

	_, err = tx.Exec(sqlStr, name, exp)
	if err != nil {
		tx.Rollback()

		if err, ok := err.(*mysql.MySQLError); ok {
			if err.Number == 1062 {
				ctx.Warning("Duplicate record.")
				insertForm(ctx)
				return
			}
		}

		panic(err)
	}

	tx.Commit()

	ctx.Success("Record has been saved.")
	ctx.Redirect("/role")
}

func insertForm(ctx *context.Ctx) {
	content.Include(ctx)

	var name, exp string
	if ctx.Req.Method == "POST" {
		name = ctx.Req.PostFormValue("name")
		exp = ctx.Req.PostFormValue("exp")
	} else {
		name = ""
		exp = ""
	}

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Roles", "New Record"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	buf.Add("<a href=\"/role\" class=\"btn btn-warning btn-md\"\">Back</a>")
	buf.Add("</div>")

	urlStr := ctx.Cargo.HtmlUrl("/role_insert", "key")
	buf.Add("<form action=\"%s\" method=\"post\">", urlStr)

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"name\">Role Name:</label>")
	buf.Add("<input type=\"text\" name=\"name\" id=\"name\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"100\" tabindex=\"1\" autofocus>", util.ScrStr(name))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"exp\">Explanation:</label>")
	buf.Add("<input type=\"text\" name=\"exp\" id=\"exp\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"250\" tabindex=\"2\">", util.ScrStr(exp))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<button type=\"submit\" class=\"btn btn-primary\" tabindex=\"3\">Submit</button>")
	buf.Add("<button type=\"reset\" class=\"btn btn-default\" tabindex=\"4\">Reset</button>")
	buf.Add("</div>")

	buf.Add("</form>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "role", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
