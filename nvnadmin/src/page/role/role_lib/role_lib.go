package role_lib

import (
	"nvnadmin/src/app"
)

type RoleRec struct {
	RoleId int64
	Name   string
	Exp    string
}

type UserRoleRec struct {
	RoleId int64
	Name   string
	Exp    string
}

type RoleRightRec struct {
	RoleId    int64
	PageRight string
}

func GetRoleRec(id int64) (*RoleRec, error) {
	sqlStr := `select
					*
				from
					role
				where
					roleId = ?`

	row := app.Db.QueryRow(sqlStr, id)

	rec := new(RoleRec)
	err := row.Scan(&rec.RoleId, &rec.Name, &rec.Exp)
	if err != nil {
		return nil, err
	}

	return rec, nil
}

func GetRoleList() []*RoleRec {
	sqlStr := `select
					roleId,
					name,
					exp
				from
					role
				order by
					name`

	rows, err := app.Db.Query(sqlStr)
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	rv := make([]*RoleRec, 0, 20)
	for rows.Next() {
		rec := new(RoleRec)
		if err = rows.Scan(&rec.RoleId, &rec.Name, &rec.Exp); err != nil {
			panic(err)
		}

		rv = append(rv, rec)
	}

	return rv
}

func CountRoleByUser(id int64) (int64, error) {
	sqlStr := `select
					count(*)
				from
					role,
					userRole
				where
					role.roleId = userRole.roleId and
					userRole.userId = ?`

	rows := app.Db.QueryRow(sqlStr, id)

	var rv int64
	if err := rows.Scan(&rv); err != nil {
		return 0, err
	}

	return rv, nil
}

func GetRoleByUser(id int64) ([]*UserRoleRec, error) {
	sqlStr := `select
					role.roleId,
					role.name,
					role.exp
				from
					role, userRole
				where
					role.roleId = userRole.roleId and
					userRole.userId = ?
				order by
					name`

	rows, err := app.Db.Query(sqlStr, id)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	rv := make([]*UserRoleRec, 0, 10)
	for rows.Next() {
		rec := new(UserRoleRec)
		if err = rows.Scan(&rec.RoleId, &rec.Name, &rec.Exp); err != nil {
			return nil, err
		}

		rv = append(rv, rec)
	}

	return rv, nil
}

func GetRoleRight(id int64) (map[string]bool, error) {
	sqlStr := `select
					roleId,
					pageRight
				from
					roleRight
				where
					roleId = ?
				order by
					pageRight`

	rows, err := app.Db.Query(sqlStr, id)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	rv := make(map[string]bool, 50)
	for rows.Next() {
		rec := new(RoleRightRec)
		err = rows.Scan(&rec.RoleId, &rec.PageRight)
		if err != nil {
			return nil, err
		}

		rv[rec.PageRight] = true
	}

	return rv, nil
}
