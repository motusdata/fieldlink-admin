package role

import (
	"database/sql"
	"fmt"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/role/role_lib"
	"strings"
)

func RoleRight(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("role:role_right") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.ReadCargo()

	id := ctx.Cargo.Int("id")
	rec, err := role_lib.GetRoleRec(id)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Warning("Record could not be found.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/role"))
			return
		}

		panic(err)
	}

	if rec.Name == "Admin" && !ctx.IsSuperUser() {
		ctx.Warning("Role 'Admin' can not be updated.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/role"))
		return
	}

	if ctx.Req.Method == "GET" {
		roleRightForm(ctx, rec)
		return
	}

	if err := ctx.Req.ParseForm(); err != nil {
		panic(err)
	}

	rightList := ctx.Req.Form["right"]

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := new(util.Buf)
	sqlStr.Add("delete from roleRight where roleId = ?")
	res, err := tx.Exec(*sqlStr.String(), id)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	if len(rightList) > 0 {
		sqlStr = new(util.Buf)
		sqlStr.Add("insert into roleRight(roleId, pageRight)")

		values := make([]string, 0, 60)
		for _, val := range rightList {
			values = append(values, fmt.Sprintf("(%d, '%s')", id, util.DbStr(val)))
		}

		sqlStr.Add(" values" + strings.Join(values, ",\n"))

		res, err = tx.Exec(*sqlStr.String())
		if err != nil {
			tx.Rollback()
			panic(err)
		}
	}

	tx.Commit()

	if n, _ := res.RowsAffected(); n == 0 {
		tx.Rollback()
		ctx.Warning("You did not change the record.")
		roleRightForm(ctx, rec)
		return
	}

	ctx.Success("Role rights has been changed.")
	ctx.Redirect("/role")
}

func roleRightForm(ctx *context.Ctx, rec *role_lib.RoleRec) {
	content.Include(ctx)
	ctx.Include.Js("/asset/js/page/role/role_right.js")

	id := ctx.Cargo.Int("id")

	roleRightList, err := role_lib.GetRoleRight(id)
	if err != nil {
		panic(err)
	}

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Roles", "Role Rights", rec.Name))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	buf.Add("<a href=\"/role\" class=\"btn btn-warning btn-md\"\">Back</a>")
	buf.Add("<button type=\"button\" class=\"expand-all btn btn-default btn-md\">Expand All</button>")
	buf.Add("<button type=\"button\" class=\"collapse-all btn btn-default btn-md\">collapse All</button>")
	buf.Add("</div>")

	urlStr := ctx.Cargo.HtmlUrl("/role_right", "id")
	buf.Add("<form class=\"form-role\" action=\"%s\" method=\"post\">", urlStr)

	tabIndex := 1
	for _, page := range app.PageList {
		buf.Add("<table>")
		buf.Add("<thead>")
		buf.Add("<tr class=\"role-header\">")
		buf.Add("<th>%s</th>", page.Title)
		buf.Add("<th>")
		buf.Add("<button type=\"button\" class=\"select-all btn btn-default btn-sm\">All</button>")
		buf.Add("<button type=\"button\" class=\"select-none btn btn-default btn-sm\">None</button>")
		buf.Add("<button type=\"button\" class=\"select-reverse btn btn-default btn-sm\">Reverse</button>")
		buf.Add("</th>")
		buf.Add("</tr>")
		buf.Add("</thead>")

		buf.Add("<tbody>")
		buf.Add("<tr>")
		buf.Add("<th class=\"fixed-middle\">Right Name</th>")
		buf.Add("<th>Explanation</th>")
		buf.Add("</tr>")

		for _, right := range page.RightList {
			buf.Add("<tr>")

			key := page.Name + ":" + right.Name
			buf.Add("<td>")

			var class string
			if roleRightList[key] {
				class = " checked"
			} else {
				class = ""
			}

			buf.Add("<label><input type=\"checkbox\" name=\"right\" value=\"%s\" tabindex=\"%d\" %s>%s</label>",
				key, tabIndex, class, right.Name)
			tabIndex += 1
			buf.Add("</td>")

			buf.Add("<td>%s</td>", right.Exp)
			buf.Add("</tr>")
		}

		buf.Add("</tbody>")
		buf.Add("</table>")
	}

	buf.Add("<div class=\"form-group\">")
	tabIndex++
	buf.Add("<button type=\"submit\" class=\"btn btn-primary\" tabindex=\"%d\">Submit</button>", tabIndex)

	tabIndex++
	buf.Add("<button type=\"reset\" class=\"btn btn-default\" tabindex=\"%d\">Reset</button>", tabIndex)
	buf.Add("</div>")

	buf.Add("</form>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())
	str := "page-role-right"
	ctx.AddFormat("pageId", &str)

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "role", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
