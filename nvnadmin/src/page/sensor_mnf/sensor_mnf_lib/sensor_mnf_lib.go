package sensor_mnf_lib

import (
	"nvnadmin/src/app"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
)

type SensorMnfRec struct {
	SensorMnfId int64
	Name        string
}

func GetSensorMnfRec(id int64) (*SensorMnfRec, error) {
	sqlStr := `select
					sensorMnf.sensorMnfId,
					sensorMnf.name
				from
					sensorMnf
				where
					sensorMnfId = ?`

	row := app.Db.QueryRow(sqlStr, id)

	rec := new(SensorMnfRec)
	err := row.Scan(&rec.SensorMnfId, &rec.Name)
	if err != nil {
		return nil, err
	}

	return rec, nil
}

func CountSensorMnf(key string) int64 {
	sqlBuf := util.NewBuf()
	sqlBuf.Add("select count(*) from sensorMnf")

	if key != "" {
		key = util.DbStr(key)
		sqlBuf.Add("where (sensorMnf.name like('%%%s%%'))", key)
	}

	row := app.Db.QueryRow(*sqlBuf.String())

	var rv int64
	err := row.Scan(&rv)
	if err != nil {
		panic(err)
	}

	return rv
}

func GetSensorMnfPage(ctx *context.Ctx, key string, pageNo int64) []*SensorMnfRec {
	sqlBuf := util.NewBuf()
	sqlBuf.Add(`select
					sensorMnf.sensorMnfId,
					sensorMnf.name
				from
					sensorMnf`)

	if key != "" {
		key = util.DbStr(key)
		sqlBuf.Add("where (sensorMnf.name like('%%%s%%'))", key)
	}

	sqlBuf.Add("order by sensorMnf.name")

	start := (pageNo - 1) * ctx.Config.PageLen
	sqlBuf.Add("limit %d, %d", start, ctx.Config.PageLen)

	rows, err := app.Db.Query(*sqlBuf.String())
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	rv := make([]*SensorMnfRec, 0, 100)
	for rows.Next() {
		rec := new(SensorMnfRec)
		if err = rows.Scan(&rec.SensorMnfId, &rec.Name); err != nil {
			panic(err)
		}

		rv = append(rv, rec)
	}

	return rv
}
