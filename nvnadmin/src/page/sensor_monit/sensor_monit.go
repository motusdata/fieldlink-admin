package sensor_monit

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/ruler"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/sensor_monit/sensor_monit_lib"
	"time"

	"github.com/nats-io/nats.go"
)

var acquisitionStatus bool
var nc *nats.Conn
var sub *nats.Subscription
var acquisitionStartTime int64

func Browse(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("sensor_monit:browse") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddStr("start", "false")
	ctx.Cargo.AddStr("stop", "false")
	ctx.Cargo.AddStr("filter", "default")
	ctx.ReadCargo()

	var err error
	if ctx.Cargo.Str("start") == "true" {
		if !acquisitionStatus {
			err = startAcquisition()
			if err != nil {
				ctx.Error("Can not start acquisition.")
				ctx.Error(err.Error())
			} else {
				acquisitionStartTime = time.Now().UTC().Unix()
				acquisitionStatus = true
			}
		}
	} else if ctx.Cargo.Str("stop") == "true" {
		if acquisitionStatus {
			err = stopAcquisition()
			if err != nil {
				ctx.Error("Can not stop acquisition.")
				ctx.Error(err.Error())
			}

			acquisitionStatus = false
		}
	}

	content.Include(ctx)
	ctx.Include.Js("./asset/js/page/sensor_monit/sensor_monit.js")

	browseMid(ctx)

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "sensor_monit", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}

func browseMid(ctx *context.Ctx) {
	pageNo := ctx.Cargo.Int("pn")
	filter := ctx.Cargo.Str("filter")

	totalRows := sensor_monit_lib.CountSensorMonit(filter)
	if totalRows == 0 {
		ctx.Warning("Empty list.")
	}

	pageNo = ctx.TouchPageNo(pageNo, totalRows)

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")
	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Beaumont SW-16 Test Well"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	oldStart := ctx.Cargo.Str("start")
	oldStop := ctx.Cargo.Str("stop")

	ctx.Cargo.SetStr("start", "true")
	ctx.Cargo.SetStr("stop", "true")

	buf.Add("<div class=\"cmd\">")

	var urlStr string
	if !acquisitionStatus {
		urlStr = ctx.Cargo.HtmlUrl("/sensor_monit", "pn", "start", "filter")
		buf.Add("<a href=\"%s\" class=\"btn btn-success btn-md\"\">Start Acquisition</a>", urlStr)
	} else {
		urlStr = ctx.Cargo.HtmlUrl("/sensor_monit", "pn", "stop", "filter")
		buf.Add("<a href=\"%s\" class=\"btn btn-danger btn-md\"\">Stop Acquisition</a>", urlStr)
	}

	if acquisitionStatus {
		buf.Add("&nbsp;&nbsp;&nbsp;acquisiton started at: %s", util.Int64ToTimeStr(acquisitionStartTime))
	}

	buf.Add("</div>") //cmd

	ctx.Cargo.SetStr("start", oldStart)
	ctx.Cargo.SetStr("stop", oldStop)

	buf.Add("<table>")

	oldFilter := ctx.Cargo.Str("filter")

	buf.Add("<thead>")
	buf.Add("<tr>")

	ctx.Cargo.SetStr("filter", "default")
	urlStr = ctx.Cargo.HtmlUrl("/sensor_monit", "filter")
	buf.Add("<th><a href=\"%s\">readTime</a></th>", urlStr)

	ctx.Cargo.SetStr("filter", "rawMv")
	urlStr = ctx.Cargo.HtmlUrl("/sensor_monit", "filter")
	buf.Add("<th><a href=\"%s\">rawMv</a></th>", urlStr)

	ctx.Cargo.SetStr("filter", "rawUvVoltage")
	urlStr = ctx.Cargo.HtmlUrl("/sensor_monit", "filter")
	buf.Add("<th><a href=\"%s\">rawUvVoltage</a></th>", urlStr)

	ctx.Cargo.SetStr("filter", "calibratedDataLbf")
	urlStr = ctx.Cargo.HtmlUrl("/sensor_monit", "filter")
	buf.Add("<th><a href=\"%s\">calibratedDataLbf</a></th>", urlStr)

	ctx.Cargo.SetStr("filter", "loadCell")
	urlStr = ctx.Cargo.HtmlUrl("/sensor_monit", "filter")
	buf.Add("<th><a href=\"%s\">loadCell</a></th>", urlStr)

	ctx.Cargo.SetStr("filter", "casingPressure")
	urlStr = ctx.Cargo.HtmlUrl("/sensor_monit", "filter")
	buf.Add("<th><a href=\"%s\">casingPressure</a></th>", urlStr)

	ctx.Cargo.SetStr("filter", "tubingPressure")
	urlStr = ctx.Cargo.HtmlUrl("/sensor_monit", "filter")
	buf.Add("<th><a href=\"%s\">tubingPressure</a></th>", urlStr)

	ctx.Cargo.SetStr("filter", "tankLevel")
	urlStr = ctx.Cargo.HtmlUrl("/sensor_monit", "filter")
	buf.Add("<th><a href=\"%s\">tankLevel</a></th>", urlStr)

	buf.Add("</tr>")
	buf.Add("</thead>")

	ctx.Cargo.SetStr("filter", oldFilter)

	buf.Add("<tbody>")

	if totalRows > 0 {
		sensorMonitList := sensor_monit_lib.GetSensorMonitPage(ctx, pageNo, filter)

		var isFirst bool = true
		var className string

		var oldSessionReadTime int64 = ctx.Session.SensorReadTime

		for _, row := range sensorMonitList {
			if isFirst {
				ctx.Session.SensorReadTime = row.ReadTime
				isFirst = false
			}

			if row.ReadTime > oldSessionReadTime {
				className = " class =\"lastRead\""
			} else {
				className = ""
			}

			buf.Add("<tr%s>", className)
			buf.Add("<td>%s</td>", util.Int64ToTimeStr(row.ReadTime))
			buf.Add("<td>%d</td>", row.RawMv)
			buf.Add("<td>%d</td>", row.RawUvVoltage)
			buf.Add("<td>%0.4f</td>", row.CalibratedDataLbf)
			buf.Add("<td>%0.4f</td>", row.LoadCell)
			buf.Add("<td>%0.4f</td>", row.CasingPressure)
			buf.Add("<td>%0.4f</td>", row.TubingPressure)
			buf.Add("<td>%0.4f</td>", row.TankLevel)
			buf.Add("</tr>")
		}
	}

	buf.Add("</tbody>")
	buf.Add("</table>")

	if totalRows > 0 {
		ruler := ruler.NewRuler(totalRows, pageNo, ctx.Cargo.HtmlUrl("/sensor_monit"))
		buf.Add(ruler.Set(ctx))
	}

	buf.Add("</div>")
	buf.Add("</div>")

	str := "sensorMonitPage"
	ctx.AddFormat("pageId", &str)

	ctx.AddFormat("midContent", buf.String())
}

func int64ToNill(n sql.NullInt64) string {
	if n.Valid {
		return fmt.Sprintf("%d", n.Int64)
	}

	return "nil"
}

func float64ToNill(n sql.NullFloat64) string {
	if n.Valid {
		return fmt.Sprintf("%0.4f", n.Float64)
	}

	return "nil"
}

func startAcquisition() error {
	var err error
	nc, err = nats.Connect("nats://138.197.185.183:4222")
	if err != nil {
		return err
	}

	sub, err = nc.Subscribe("g145:data", func(msg *nats.Msg) {
		saveData(msg)
	})

	if err != nil {
		return err
	}

	err = nc.Publish("g145:startAcquisition", []byte("true"))
	if err != nil {
		return err
	}

	return nil
}

func stopAcquisition() error {
	if nc == nil {
		panic("nil value for nats connection")
	}

	err := nc.Publish("g145:stopAcquisition", []byte("true"))
	if err != nil {
		return err
	}

	err = sub.Unsubscribe()
	if err != nil {
		return err
	}

	nc.Close()

	return nil
}

type payload struct {
	RawMv             int64   `json:"rawMw"`
	RawUvVoltage      int64   `json:"rawUvVoltage"`
	CalibratedDataLbf float64 `json:"calibratedDataLbf"`
	LoadCell          float64 `json:"loadCell"`
	CasingPressure    float64 `json:"casingPressure"`
	TubingPressure    float64 `json:"tubingPressure"`
	TankLevel         float64 `json:"tankLevel"`
	ReadTime          int64   `json:"readTime"`
}

func saveData(msg *nats.Msg) error {
	var pl payload

	err := json.Unmarshal(msg.Data, &pl)
	if err != nil {
		return err
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `insert into
						sensorMonit(sensorMonitId, readTime, rawMv, rawUvVoltage,
							calibratedDataLbf, loadCell, casingPressure, tubingPressure,
							tankLevel)
						values(null, ?, ?, ?, ?, ?, ?, ?, ?)`

	_, err = tx.Exec(sqlStr, pl.ReadTime, pl.RawMv, pl.RawUvVoltage, pl.CalibratedDataLbf,
		pl.LoadCell, pl.CasingPressure, pl.TubingPressure, pl.TankLevel)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	tx.Commit()

	return nil
}
