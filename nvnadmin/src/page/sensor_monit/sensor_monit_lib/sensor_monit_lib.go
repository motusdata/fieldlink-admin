package sensor_monit_lib

import (
	"nvnadmin/src/app"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
)

type SensorMonitRec struct {
	SensorMonitId     int64
	ReadTime          int64
	RawMv             int64
	RawUvVoltage      int64
	CalibratedDataLbf float64
	LoadCell          float64
	CasingPressure    float64
	TubingPressure    float64
	TankLevel         float64
}

func GetSensorMonitRec(id int64) (*SensorMonitRec, error) {
	sqlStr := `select
					sensorMonit.sensorMonitId,
					sensorMonit.readTime,
					sensorMonit.rawMv,
					sensorMonit.rawUvVoltage,
					sensorMonit.calibratedDataLbf,
					sensorMonit.loadCell,
					sensorMonit.casingPressure,
					sensorMonit.tubingPressure,
					sensorMonit.tankLevel
				from
					sensorMonit
				where
					sensorMonitId = ?`

	row := app.Db.QueryRow(sqlStr, id)

	rec := new(SensorMonitRec)
	err := row.Scan(&rec.SensorMonitId, &rec.ReadTime, &rec.RawMv,
		&rec.RawUvVoltage, &rec.CalibratedDataLbf, &rec.LoadCell,
		&rec.CasingPressure, &rec.TubingPressure, &rec.TankLevel)

	if err != nil {
		return nil, err
	}

	return rec, nil
}

func CountSensorMonit(filter string) int64 {
	sqlBuf := util.NewBuf()
	sqlBuf.Add("select count(*) from sensorMonit")

	if filter != "default" {
		sqlBuf.Add("where sensorMonit.%s != 0", filter)
	}

	row := app.Db.QueryRow(*sqlBuf.String())

	var rv int64
	err := row.Scan(&rv)
	if err != nil {
		panic(err)
	}

	return rv
}

func GetSensorMonitPage(ctx *context.Ctx, pageNo int64, filter string) []*SensorMonitRec {
	sqlBuf := util.NewBuf()
	sqlBuf.Add(`select
					sensorMonit.sensorMonitId,
					sensorMonit.readTime,
					sensorMonit.rawMv,
					sensorMonit.rawUvVoltage,
					sensorMonit.calibratedDataLbf,
					sensorMonit.loadCell,
					sensorMonit.casingPressure,
					sensorMonit.tubingPressure,
					sensorMonit.tankLevel
				from
					sensorMonit`)

	if filter != "default" {
		sqlBuf.Add("where sensorMonit.%s != 0", filter)
	}

	sqlBuf.Add("order by sensorMonit.readTime desc")

	start := (pageNo - 1) * ctx.Config.PageLen
	sqlBuf.Add("limit %d, %d", start, ctx.Config.PageLen)

	rows, err := app.Db.Query(*sqlBuf.String())
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	rv := make([]*SensorMonitRec, 0, 100)
	for rows.Next() {
		rec := new(SensorMonitRec)
		err = rows.Scan(&rec.SensorMonitId, &rec.ReadTime, &rec.RawMv,
			&rec.RawUvVoltage, &rec.CalibratedDataLbf, &rec.LoadCell,
			&rec.CasingPressure, &rec.TubingPressure, &rec.TankLevel)

		if err != nil {
			panic(err)
		}

		rv = append(rv, rec)
	}

	return rv
}
