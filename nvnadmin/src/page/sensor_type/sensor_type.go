package sensor_type

import (
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/ruler"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/sensor_type/sensor_type_lib"
)

func Browse(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if ctx.Req.URL.Path != "/" && ctx.Req.URL.Path != "/sensor_type" {
		app.NotFound()
	}

	if !ctx.IsRight("sensor_type:browse") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.ReadCargo()

	content.Include(ctx)

	browseMid(ctx)

	content.Default(ctx)

	content.Search(ctx, "/sensor_type")

	lmenu := left_menu.New()
	lmenu.Set(ctx, "sensor_type", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}

func browseMid(ctx *context.Ctx) {
	key := ctx.Cargo.Str("key")
	pageNo := ctx.Cargo.Int("pn")

	totalRows := sensor_type_lib.CountSensorType(key)
	if totalRows == 0 {
		ctx.Warning("Empty list.")
	}

	pageNo = ctx.TouchPageNo(pageNo, totalRows)

	insertRight := ctx.IsRight("sensor_type:insert")
	updateRight := ctx.IsRight("sensor_type:update")
	deleteRight := ctx.IsRight("sensor_type:delete")

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")
	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Sensor Types"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	if insertRight || updateRight {
		buf.Add("<div class=\"cmd\">")

		if insertRight {
			urlStr := ctx.Cargo.HtmlUrl("/sensor_type_insert", "key", "pn")
			buf.Add("<a href=\"%s\" class=\"btn btn-primary btn-md\" title=\"New record.\">New</a>", urlStr)
		}

		buf.Add("<span id=\"lastUpdate\"></span>")

		buf.Add("</div>")
	}

	buf.Add("<table>")
	buf.Add("<thead>")
	buf.Add("<tr>")
	buf.Add("<th class=\"fixed-middle\">Name</th>")

	if updateRight || deleteRight {
		buf.Add("<th class=\"cmd cmd-table\">%s</th>", "Command")
	}

	buf.Add("</tr>")
	buf.Add("</thead>")

	buf.Add("<tbody>")

	if totalRows > 0 {
		sensorTypeList := sensor_type_lib.GetSensorTypePage(ctx, key, pageNo)

		var name string
		for _, row := range sensorTypeList {
			ctx.Cargo.SetInt("id", row.SensorTypeId)

			name = util.ScrStr(row.Name)

			if key != "" {
				name = content.Find(name, key)
			}

			buf.Add("<tr>")
			buf.Add("<td>%s</td>", name)

			if updateRight || deleteRight {
				buf.Add("<td class=\"cmd cmd-table\">")

				if updateRight {
					urlStr := ctx.Cargo.HtmlUrl("/sensor_type_update", "id", "key", "pn")
					buf.Add("<a href=\"%s\" class=\"btn btn-default btn-sm\" title=\"Edit record.\">Edit</a>", urlStr)
				}

				if deleteRight {
					urlStr := ctx.Cargo.HtmlUrl("/sensor_type_delete", "id", "key", "pn")
					buf.Add("<a href=\"%s\" class=\"btn btn-danger btn-sm\" title=\"Delete record.\">Delete</a>", urlStr)
				}

				buf.Add("</td>")
			}

			buf.Add("</tr>")
		}
	}

	buf.Add("</tbody>")
	buf.Add("</table>")

	if totalRows > 0 {
		ruler := ruler.NewRuler(totalRows, pageNo, ctx.Cargo.HtmlUrl("/sensor_type", "key"))
		buf.Add(ruler.Set(ctx))
	}

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())
}
