package sensor_type_lib

import (
	"nvnadmin/src/app"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
)

type SensorTypeRec struct {
	SensorTypeId int64
	Name         string
}

func GetSensorTypeRec(id int64) (*SensorTypeRec, error) {
	sqlStr := `select
					sensorType.sensorTypeId,
					sensorType.name
				from
					sensorType
				where
					sensorTypeId = ?`

	row := app.Db.QueryRow(sqlStr, id)

	rec := new(SensorTypeRec)
	err := row.Scan(&rec.SensorTypeId, &rec.Name)
	if err != nil {
		return nil, err
	}

	return rec, nil
}

func CountSensorType(key string) int64 {
	sqlBuf := util.NewBuf()
	sqlBuf.Add("select count(*) from sensorType")

	if key != "" {
		key = util.DbStr(key)
		sqlBuf.Add("where (sensorType.name like('%%%s%%'))", key)
	}

	row := app.Db.QueryRow(*sqlBuf.String())

	var rv int64
	err := row.Scan(&rv)
	if err != nil {
		panic(err)
	}

	return rv
}

func GetSensorTypePage(ctx *context.Ctx, key string, pageNo int64) []*SensorTypeRec {
	sqlBuf := util.NewBuf()
	sqlBuf.Add(`select
					sensorType.sensorTypeId,
					sensorType.name
				from
					sensorType`)

	if key != "" {
		key = util.DbStr(key)
		sqlBuf.Add("where (sensorType.name like('%%%s%%'))", key)
	}

	sqlBuf.Add("order by sensorType.name")

	start := (pageNo - 1) * ctx.Config.PageLen
	sqlBuf.Add("limit %d, %d", start, ctx.Config.PageLen)

	rows, err := app.Db.Query(*sqlBuf.String())
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	rv := make([]*SensorTypeRec, 0, 100)
	for rows.Next() {
		rec := new(SensorTypeRec)
		if err = rows.Scan(&rec.SensorTypeId, &rec.Name); err != nil {
			panic(err)
		}

		rv = append(rv, rec)
	}

	return rv
}
