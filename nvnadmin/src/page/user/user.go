package user

import (
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/combo"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/ruler"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/user/user_lib"
)

type statusRec struct {
	StatusKey  string
	StatusName string
}

func Browse(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("user:browse") {
		app.BadRequest()
	}

	if req.URL.Path != "/user" && req.URL.Path != "/" {
		app.NotFound()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddInt("roleId", -1)
	ctx.Cargo.AddStr("status", "default")
	ctx.ReadCargo()

	content.Include(ctx)

	browseMid(ctx)

	content.Default(ctx)

	content.Search(ctx, "/user", "roleId", "status")

	lmenu := left_menu.New()
	lmenu.Set(ctx, "user", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}

func browseMid(ctx *context.Ctx) {
	ctx.Include.Js("/asset/js/page/user/user.js")

	key := ctx.Cargo.Str("key")
	pageNo := ctx.Cargo.Int("pn")
	roleId := ctx.Cargo.Int("roleId")
	status := ctx.Cargo.Str("status")

	roleCombo := combo.NewCombo("select roleId, name from role", "All Roles")
	roleCombo.Set()

	if roleCombo.IsEmpty() {
		ctx.Warning("Role list is empty. You should enter at least one role first.")
	}

	statusCombo := combo.NewEnumCombo()
	statusCombo.Add("default", "All Statuses")
	statusCombo.Add("active", "Active")
	statusCombo.Add("blocked", "Blocked")

	totalRows := user_lib.CountUser(key, roleId, status)
	if totalRows == 0 {
		ctx.Warning("Empty list.")
	}

	pageNo = ctx.TouchPageNo(pageNo, totalRows)

	insertRight := ctx.IsRight("user:insert")
	updateRight := ctx.IsRight("user:update")
	confirmRight := ctx.IsRight("user:confirm")
	updatePassRight := ctx.IsRight("user:update_pass")
	deleteRight := ctx.IsRight("user:delete")
	roleBrowseRight := ctx.IsRight("user:role_browse")

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")
	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Users"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")
	buf.Add("<div class=\"cmd\">")

	if insertRight {
		urlStr := ctx.Cargo.HtmlUrl("/user_insert", "key", "pn", "roleId", "status")
		buf.Add("<a href=\"%s\" class=\"btn btn-primary btn-md\" title=\"New record.\">New</a>", urlStr)
	}

	//roleId form
	buf.Add("<form id=\"roleForm\" class=\"form form-inline\" action=\"/user\" method=\"get\">")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<select name=\"roleId\" id=\"roleId\" class=\"form-control\">")

	buf.Add(roleCombo.Format(roleId))

	buf.Add("</select>")
	buf.Add("</div>")
	buf.Add(content.HiddenCargo(ctx, "key", "status"))
	buf.Add("</form>")

	//status form
	buf.Add("<form id=\"statusForm\" class=\"form form-inline\" action=\"/user\" method=\"get\">")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<select name=\"status\" id=\"status\" class=\"form-control\">")

	buf.Add(statusCombo.Format(status))

	buf.Add("</select>")
	buf.Add("</div>")
	buf.Add(content.HiddenCargo(ctx, "key", "roleId"))
	buf.Add("</form>")
	buf.Add("</div>") //end of cmd

	buf.Add("<table>")

	buf.Add("<thead>")
	buf.Add("<tr>")
	buf.Add("<th>Name</th>")
	buf.Add("<th>Login</th>")
	buf.Add("<th>Email</th>")
	buf.Add("<th>Status</th>")

	if updateRight || roleBrowseRight || updatePassRight || deleteRight {
		buf.Add("<th class=\"cmd cmd-table\">Command</th>")
	}

	buf.Add("</tr>")
	buf.Add("</thead>")

	buf.Add("<tbody>")

	if totalRows > 0 {
		userList := user_lib.GetUser(ctx, key, pageNo, totalRows, roleId, status)

		var name, login, email string
		for _, row := range userList {
			if (row.Login == "superuser" || row.Login == "testuser") && !ctx.IsSuperUser() {
				continue
			}

			ctx.Cargo.SetInt("id", row.UserId)

			name = util.ScrStr(row.Name)
			login = util.ScrStr(row.Login)
			email = util.ScrStr(row.Email)

			if key != "" {
				name = content.Find(name, key)
				login = content.Find(login, key)
				email = content.Find(email, key)
			}

			buf.Add("<tr>")
			buf.Add("<td>%s</td>", name)
			buf.Add("<td>%s</td>", login)
			buf.Add("<td>%s</td>", email)

			buf.Add("<td>")
			switch row.Status {
			case "active":
				buf.Add("<span class=\"label label-success label-sm\">Active</span>")
			case "blocked":
				buf.Add("<span class=\"label label-danger label-sm\">Blocked</span>")
			default:
				buf.Add("<span class=\"label label-default label-sm\">Unknown</span>")
			}

			buf.Add("</td>")

			if updateRight || confirmRight || roleBrowseRight || updatePassRight || deleteRight {
				buf.Add("<td class=\"cmd cmd-table\">")

				if updateRight {
					urlStr := ctx.Cargo.HtmlUrl("/user_update", "id", "key", "pn", "roleId", "status")
					buf.Add("<a href=\"%s\" class=\"btn btn-default btn-sm\" title=\"Edit record.\">Edit</a>", urlStr)
				}

				if confirmRight {
					urlStr := ctx.Cargo.HtmlUrl("/user_confirm", "id", "key", "pn", "roleId", "status")
					buf.Add("<a href=\"%s\" class=\"btn btn-default btn-sm\" title=\"Confirm new registration.\">Confirm</a>", urlStr)
				}

				if roleBrowseRight {
					urlStr := ctx.Cargo.HtmlUrl("/user_role", "id", "key", "pn", "roleId", "status")
					buf.Add("<a href=\"%s\" class=\"btn btn-default btn-sm\" title=\"Edit user roles.\">User Roles</a>", urlStr)
				}

				if updatePassRight {
					urlStr := ctx.Cargo.HtmlUrl("/user_update_pass", "id", "key", "pn", "roleId", "status")
					buf.Add("<a href=\"%s\" class=\"btn btn-default btn-sm\" title=\"Change user password\">Change Pass</a>", urlStr)
				}

				if deleteRight {
					urlStr := ctx.Cargo.HtmlUrl("/user_delete", "id", "key", "pn", "roleId", "status")
					buf.Add("<a href=\"%s\" class=\"btn btn-danger btn-sm\" title=\"Delete record.\">Delete</a>", urlStr)
				}

				buf.Add("</td>")
			}

			buf.Add("</tr>")
		}
	}

	buf.Add("</tbody>")
	buf.Add("</table>")

	if totalRows > 0 {
		ruler := ruler.NewRuler(totalRows, pageNo, ctx.Cargo.HtmlUrl("/user", "key"))
		buf.Add(ruler.Set(ctx))
	}

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())
}
