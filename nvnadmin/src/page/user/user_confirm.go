package user

import (
	"database/sql"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/combo"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/user/user_lib"
	"strconv"

	"github.com/go-sql-driver/mysql"
)

func Confirm(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("user:update") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddInt("roleId", -1)
	ctx.Cargo.AddStr("status", "default")
	ctx.ReadCargo()

	id := ctx.Cargo.Int("id")
	rec, err := user_lib.GetUserRec(id)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Warning("Record could not be found.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
			return
		}

		panic(err)
	}

	if rec.Status != "user_confirmed" {
		ctx.Warning("This user has not confirmed his/her registration yet." +
			" Only 'User Confirmed' records can be confirmed by admin.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
		return
	}

	if ctx.Req.Method == "GET" {
		confirmForm(ctx, rec)
		return
	}

	roleId, err := strconv.ParseInt(ctx.Req.PostFormValue("roleId"), 10, 64)
	if err != nil {
		roleId = -1
	}

	if roleId == -1 {
		ctx.Warning("You have not select a role.")
		confirmForm(ctx, rec)
		return
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `update user set
					status = ?
				where
					userId = ?`

	res, err := tx.Exec(sqlStr, "active", id)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	if n, _ := res.RowsAffected(); n == 0 {
		tx.Rollback()
		ctx.Warning("You did not change the record.")
		confirmForm(ctx, rec)
		return
	}

	sqlStr = `insert ignore into
					userRole(userId, roleId)
					values(?, ?)`

	_, err = tx.Exec(sqlStr, id, roleId)
	if err != nil {
		tx.Rollback()
		if err, ok := err.(*mysql.MySQLError); ok {
			if err.Number == 1452 {
				ctx.Warning("Could not find parent record.")
				ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
				return
			} else if err.Number == 1062 {
				ctx.Warning("Duplicate record.")
				ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
				return
			}
		}

		panic(err)
	}

	tx.Commit()

	ctx.Success("Record has been changed.")
	ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
}

func confirmForm(ctx *context.Ctx, rec *user_lib.UserRec) {
	content.Include(ctx)

	var roleId int64 = -1
	if ctx.Req.Method == "POST" {
		roleId, _ = strconv.ParseInt(ctx.Req.PostFormValue("roleId"), 10, 64)
	}

	roleCombo := combo.NewCombo("select roleId, name from role", "Select Role")
	roleCombo.Set()
	if roleCombo.IsEmpty() {
		ctx.Warning("Role table is empty. You should enter at least one role first.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
		return
	}

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Users", "Confirm New User"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	urlStr := ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status")
	buf.Add("<a href=\"%s\" class=\"btn btn-warning btn-md\"\">Back</a>", urlStr)
	buf.Add("</div>")

	buf.Add("<table>")
	buf.Add("<caption>New user information:</caption>")
	buf.Add("<tbody>")
	buf.Add("<tr><th class=\"fixed-middle\">User Name:</th><td>%s</td></tr>", util.ScrStr(rec.Name))
	buf.Add("<tr><th>Login Name:</th><td>%s</td></tr>", util.ScrStr(rec.Login))
	buf.Add("<tr><th>Email:</th><td>%s</td></tr>", util.ScrStr(rec.Email))
	buf.Add("</tbody>")
	buf.Add("</table>")

	urlStr = ctx.Cargo.HtmlUrl("/user_confirm", "id", "key", "pn", "roleId", "status")
	buf.Add("<form action=\"%s\" method=\"post\">", urlStr)

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"roleId\">Role:</label>")
	buf.Add("<select name=\"roleId\" id=\"roleId\" class=\"form-control\" tabindex=\"1\" autofocus>")

	buf.Add(roleCombo.Format(roleId))

	buf.Add("</select>")
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<button type=\"submit\" class=\"btn btn-primary\" tabindex=\"2\">Submit</button>")
	buf.Add("<button type=\"reset\" class=\"btn btn-default\" tabindex=\"3\">Reset</button>")
	buf.Add("</div>")

	buf.Add("</form>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "user", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
