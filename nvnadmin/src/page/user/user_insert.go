package user

import (
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/combo"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"strconv"

	"github.com/go-sql-driver/mysql"
)

func Insert(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("user:insert") {
		app.BadRequest()
	}

	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddInt("roleId", -1)
	ctx.Cargo.AddStr("status", "default")
	ctx.ReadCargo()

	if ctx.Req.Method == "GET" {
		insertForm(ctx)
		return
	}

	name := ctx.Req.PostFormValue("name")
	login := ctx.Req.PostFormValue("login")
	email := ctx.Req.PostFormValue("email")
	password := ctx.Req.PostFormValue("password")

	formRoleId, err := strconv.ParseInt(ctx.Req.PostFormValue("formRoleId"), 10, 64)
	if err != nil {
		formRoleId = -1
	}

	formStatus := ctx.Req.PostFormValue("formStatus")

	if name == "" || login == "" || email == "" || password == "" ||
		formRoleId == -1 {
		ctx.Warning("You have left one or more fields empty.")
		insertForm(ctx)
		return
	}

	if err := util.IsValidEmail(email); err != nil {
		ctx.Warning(err.Error())
		insertForm(ctx)
		return
	}

	if err := util.IsValidPassword(password); err != nil {
		ctx.Warning(err.Error())
		insertForm(ctx)
		return
	}

	if formStatus != "active" && formStatus != "blocked" {
		ctx.Warning("Unkown user status: " + formStatus)
		insertForm(ctx)
		return
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	password = util.PasswordHash(password)

	sqlStr := `insert into
					user(userId, name, login, email, password, status)
					values(null, ?, ?, ?, ?, ?)`

	res, err := tx.Exec(sqlStr, name, login, email, password, formStatus)
	if err != nil {
		tx.Rollback()
		if err, ok := err.(*mysql.MySQLError); ok {
			if err.Number == 1062 {
				ctx.Warning("Duplicate record.")
				insertForm(ctx)
				return
			} else if err.Number == 1452 {
				ctx.Warning("Could not find parent record.")
				insertForm(ctx)
				return
			}
		}

		panic(err)
	}

	lastId, err := res.LastInsertId()
	if err != nil {
		panic(err)
	}

	sqlStr = `insert into
					userRole(userId, roleId)
					values(?, ?)`

	_, err = tx.Exec(sqlStr, lastId, formRoleId)
	if err != nil {
		tx.Rollback()
		if err, ok := err.(*mysql.MySQLError); ok {
			if err.Number == 1452 {
				ctx.Warning("Could not find parent record.")
				insertForm(ctx)
				return
			}
		}

		panic(err)
	}

	tx.Commit()

	ctx.Success("Record has been saved.")
	ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
}

func insertForm(ctx *context.Ctx) {
	content.Include(ctx)

	roleId := ctx.Cargo.Int("roleId")
	status := ctx.Cargo.Str("status")

	var name, login, email, password, formStatus string
	var formRoleId int64
	var err error
	if ctx.Req.Method == "POST" {
		name = ctx.Req.PostFormValue("name")
		login = ctx.Req.PostFormValue("login")
		email = ctx.Req.PostFormValue("email")
		password = ctx.Req.PostFormValue("password")

		formRoleId, err = strconv.ParseInt(ctx.Req.PostFormValue("formRoleId"), 10, 64)
		if err != nil {
			formRoleId = -1
		}

		formStatus = ctx.Req.PostFormValue("formStatus")
	} else {
		name = ""
		login = ""
		email = ""
		password = ""
		formRoleId = roleId
		formStatus = status
	}

	roleCombo := combo.NewCombo("select roleId, name from role", "Select Role")
	roleCombo.Set()

	if roleCombo.IsEmpty() {
		ctx.Warning("Role table is empty. You should enter at least one role first.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
		return
	}

	statusCombo := combo.NewEnumCombo()
	statusCombo.Add("default", "Select Status")
	statusCombo.Add("active", "Active")
	statusCombo.Add("blocked", "Blocked")

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Users", "New Record"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	urlStr := ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status")
	buf.Add("<a href=\"%s\" class=\"btn btn-warning btn-md\"\">Back</a>", urlStr)
	buf.Add("</div>")

	urlStr = ctx.Cargo.HtmlUrl("/user_insert", "key", "pn", "roleId", "status")
	buf.Add("<form action=\"%s\" method=\"post\">", urlStr)

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"name\">User Name:</label>")
	buf.Add("<input type=\"text\" name=\"name\" id=\"name\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"100\" tabindex=\"1\" autofocus>", util.ScrStr(name))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"login\">Login Name:</label>")
	buf.Add("<input type=\"text\" name=\"login\" id=\"login\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"100\" tabindex=\"2\">", util.ScrStr(login))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"email\">Email:</label>")
	buf.Add("<input type=\"text\" name=\"email\" id=\"email\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"100\" tabindex=\"3\">", util.ScrStr(email))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"password\">Password:</label>")
	buf.Add("<input type=\"text\" name=\"password\" id=\"password\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"30\" tabindex=\"4\">", util.ScrStr(password))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"formRoleId\">Role:</label>")
	buf.Add("<select name=\"formRoleId\" id=\"formRoleId\" class=\"form-control\" tabindex=\"5\">")

	buf.Add(roleCombo.Format(formRoleId))

	buf.Add("</select>")
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"formStatus\">Status:</label>")
	buf.Add("<select name=\"formStatus\" id=\"formStatus\" class=\"form-control\" tabindex=\"6\">")

	buf.Add(statusCombo.Format(formStatus))

	buf.Add("</select>")
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<button type=\"submit\" class=\"btn btn-primary\" tabindex=\"7\">Submit</button>")
	buf.Add("<button type=\"reset\" class=\"btn btn-default\" tabindex=\"8\">Reset</button>")
	buf.Add("</div>")

	buf.Add("</form>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "user", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
