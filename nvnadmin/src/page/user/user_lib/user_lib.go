package user_lib

import (
	"nvnadmin/src/app"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
)

type UserRec struct {
	UserId   int64
	Name     string
	Login    string
	Email    string
	Password string
	Status   string
}

func GetUserRec(id int64) (*UserRec, error) {
	sqlStr := `select
					user.userId,
					user.name,
					user.login,
					user.name,
					user.email,
					user.password,
					user.status
				from
					user
				where
					user.userId = ?`

	row := app.Db.QueryRow(sqlStr, id)

	rec := new(UserRec)
	err := row.Scan(&rec.UserId, &rec.Name, &rec.Login, &rec.Name, &rec.Email,
		&rec.Password, &rec.Status)
	if err != nil {
		return nil, err
	}

	return rec, nil
}

func CountUser(key string, roleId int64, status string) int64 {
	sqlBuf := util.NewBuf()
	sqlBuf.Add("select count(*)")

	fromBuf := util.NewBuf()
	fromBuf.Add("user")

	conBuf := util.NewBuf()
	if roleId != -1 {
		fromBuf.Add("userRole")
		conBuf.Add("(user.userId = userRole.userId)")
		conBuf.Add("(userRole.roleId = %d)", roleId)
	}

	if status != "default" {
		conBuf.Add("(user.status = '%s')", util.DbStr(status))
	}

	if key != "" {
		key = util.DbStr(key)
		conBuf.Add(`(user.name like('%%%s%%') or
					user.email like('%%%s%%') or
					user.login like('%%%s%%'))`, key, key, key)
	}

	sqlBuf.Add("from " + *fromBuf.StringSep(", "))

	if !conBuf.IsEmpty() {
		sqlBuf.Add("where " + *conBuf.StringSep(" and "))
	}

	row := app.Db.QueryRow(*sqlBuf.String())

	var rv int64
	err := row.Scan(&rv)
	if err != nil {
		panic(err)
	}

	return rv
}

func CountAdminUser() int64 {
	sqlStr := `select
					count(*)
				from
					user,
					userRole,
					role
				where
					user.userId = userRole.userId and
					userRole.roleId = role.roleId and
					role.name = 'Admin'`

	row := app.Db.QueryRow(sqlStr)

	var rv int64
	err := row.Scan(&rv)
	if err != nil {
		panic(err)
	}

	return rv
}

func IsAdminUser(id int64) bool {
	sqlStr := `select
					role.name
				from
					user,
					userRole,
					role
				where
					user.userId = ? and
					user.userId = userRole.userId and
					userRole.roleId = role.roleId`

	rows, err := app.Db.Query(sqlStr, id)
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	var name string
	for rows.Next() {
		err = rows.Scan(&name)
		if err != nil {
			panic(err)
		}

		if name == "Admin" {
			return true
		}
	}

	return false
}

func GetUser(ctx *context.Ctx, key string, pageNo, totalRows, roleId int64, status string) []*UserRec {

	sqlBuf := util.NewBuf()
	sqlBuf.Add("select user.userId, user.name, user.login, user.email, user.status")

	fromBuf := util.NewBuf()
	fromBuf.Add("user")

	conBuf := util.NewBuf()
	if roleId != -1 {
		fromBuf.Add("userRole")
		conBuf.Add("(user.userId = userRole.userId)")
		conBuf.Add("(userRole.roleId = %d)", roleId)
	}

	if status != "default" {
		conBuf.Add("(user.status = '%s')", util.DbStr(status))
	}

	if key != "" {
		key = util.DbStr(key)
		conBuf.Add(`(user.name like('%%%s%%') or
					user.email like('%%%s%%') or
					user.login like('%%%s%%'))`, key, key, key)
	}

	sqlBuf.Add("from " + *fromBuf.StringSep(", "))

	if !conBuf.IsEmpty() {
		sqlBuf.Add(" where " + *conBuf.StringSep(" and "))
	}

	sqlBuf.Add("order by user.name")

	start := (pageNo - 1) * ctx.Config.PageLen
	sqlBuf.Add("limit %d, %d", start, ctx.Config.PageLen)

	rows, err := app.Db.Query(*sqlBuf.String())
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	rv := make([]*UserRec, 0, 100)
	for rows.Next() {
		rec := new(UserRec)
		err = rows.Scan(&rec.UserId, &rec.Name, &rec.Login, &rec.Email, &rec.Status)
		if err != nil {
			panic(err)
		}

		rv = append(rv, rec)
	}

	return rv
}

func GetUserRecByLogin(name, password string) (*UserRec, error) {
	sqlStr := `select
					userId,
					name,
					login,
					name,
					email,
					status
				from
					user
				where
					login = ? and
					password = ?`

	row := app.Db.QueryRow(sqlStr, name, password)

	rec := new(UserRec)
	err := row.Scan(&rec.UserId, &rec.Name, &rec.Login, &rec.Name, &rec.Email, &rec.Status)
	if err != nil {
		return nil, err
	}

	return rec, nil
}

func GetUserRecByEmail(email string) (*UserRec, error) {
	sqlStr := `select
					userId,
					name,
					login,
					name,
					email,
					status
				from
					user
				where
					email = ?`

	row := app.Db.QueryRow(sqlStr, email)

	rec := new(UserRec)
	err := row.Scan(&rec.UserId, &rec.Name, &rec.Login, &rec.Name, &rec.Email, &rec.Status)
	if err != nil {
		return nil, err
	}

	return rec, nil
}
