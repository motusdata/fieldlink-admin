package user

import (
	"database/sql"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/combo"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/role/role_lib"
	"nvnadmin/src/page/user/user_lib"
)

func UserRole(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("user:role_browse") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddInt("userRoleId", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddInt("roleId", -1)
	ctx.Cargo.AddStr("status", "default")
	ctx.ReadCargo()

	id := ctx.Cargo.Int("id")
	rec, err := user_lib.GetUserRec(id)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Warning("Record could not be found.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
			return
		}

		panic(err)
	}

	if (rec.Login == "superuser" || rec.Login == "testuser") && !ctx.IsSuperUser() {
		ctx.Warning("'superuser' and 'testuser' account can not be updated.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
		return
	}

	displayUser(ctx, rec)
}

func displayUser(ctx *context.Ctx, userRec *user_lib.UserRec) {
	content.Include(ctx)

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Users", "User Roles"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	urlStr := ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status")
	buf.Add("<a href=\"%s\" class=\"btn btn-warning btn-md\">Back</a>", urlStr)
	buf.Add("</div>")

	//user info
	buf.Add("<table>")
	buf.Add("<caption>User Information:</caption>")
	buf.Add("<tbody>")
	buf.Add("<tr><th class=\"fixed-middle\">User Name:</th><td>%s</td></tr>", util.ScrStr(userRec.Name))
	buf.Add("<tr><th>Login:</th><td>%s</td></tr>", util.ScrStr(userRec.Login))
	buf.Add("<tr><th>Email:</th><td>%s</td></tr>", util.ScrStr(userRec.Email))
	buf.Add("</tbody>")
	buf.Add("</table>")

	//grant role
	if ctx.IsRight("user:role_grant") {
		roleCombo := combo.NewCombo("select roleId, name from role", "Select Role")
		roleCombo.Set()
		if roleCombo.IsEmpty() {
			ctx.Warning("Role list is empty. You should enter at least one role first.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
			return
		}

		buf.Add("<table>")
		buf.Add("<caption>Grant Role:</caption>")
		buf.Add("<tbody>")
		buf.Add("<tr>")
		buf.Add("<th class=\"fixed-middle\">Role:</th>")
		buf.Add("<td class=\"flex-container\">")

		urlStr = ctx.Cargo.HtmlUrl("/user_role_grant", "id", "key", "pn", "roleId", "status")
		buf.Add("<form class=\"form form-flex\" action=\"%s\" method=\"post\">", urlStr)

		buf.Add("<div class=\"form-group\">")
		buf.Add("<select name=\"userRoleId\" id=\"userRoleId\" class=\"form-control\">")

		buf.Add(roleCombo.Format(-1))

		buf.Add("</select>")
		buf.Add("</div>")

		buf.Add("<div class=\"form-group\">")
		buf.Add("<button type=\"submit\" class=\"btn btn-primary btn-md\">Grant</button>")
		buf.Add("</div>")

		buf.Add("</form>")

		buf.Add("</td>")
		buf.Add("</tr>")
	}

	revokeRight := ctx.IsRight("user:role_revoke")

	//user role list
	buf.Add("<table>")
	buf.Add("<caption>User Role List:</caption>")
	buf.Add("<thead>")
	buf.Add("<tr>")
	buf.Add("<th class=\"fixed-middle\">Role Name</th>")
	buf.Add("<th>Explanation</th>")

	if revokeRight {
		buf.Add("<th class=\"cmd cmd-table\">Command</th>")
	}

	buf.Add("</tr>")
	buf.Add("</thead>")
	buf.Add("<tbody>")

	userRoleList, err := role_lib.GetRoleByUser(userRec.UserId)
	if err != nil {
		panic(err)
	}

	if len(userRoleList) != 0 {
		for _, row := range userRoleList {
			buf.Add("<tr>")
			buf.Add("<td>%s</td><td>%s</td>", row.Name, row.Exp)

			if revokeRight {
				buf.Add("<td class=\"cmd cmd-table\">")
				ctx.Cargo.SetInt("userRoleId", row.RoleId)
				urlStr = ctx.Cargo.HtmlUrl("/user_role_revoke", "id", "userRoleId", "key", "pn", "roleId",
					"status")
				buf.Add("<a href=\"%s\" class=\"btn btn-danger btn-sm\">Revoke</a>", urlStr)
				buf.Add("</td>")
			}

			buf.Add("</tr>")
		}
	} else {
		buf.Add("<tr><td colspan=\"2\"><span class=\"label label-danger label-sd\">Empty</span></td></tr>")
	}

	buf.Add("</tbody>")
	buf.Add("</table>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "user", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
