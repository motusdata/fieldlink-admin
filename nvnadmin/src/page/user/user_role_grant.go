package user

import (
	"database/sql"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/page/user/user_lib"
	"strconv"

	"github.com/go-sql-driver/mysql"
)

func UserRoleGrant(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("user:role_grant") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddInt("roleId", -1)
	ctx.Cargo.AddStr("status", "default")
	ctx.ReadCargo()

	id := ctx.Cargo.Int("id")
	userRoleId, err := strconv.ParseInt(ctx.Req.PostFormValue("userRoleId"), 10, 64)
	if err != nil {
		userRoleId = -1
	}

	if userRoleId == -1 {
		ctx.Warning("You did not select a role.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/user_role", "id", "key", "pn", "roleId", "status"))
		return
	}

	if id == -1 {
		ctx.Warning("User record could not be found.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/user_role", "id", "key", "pn", "roleId", "status"))
		return
	}

	userRec, err := user_lib.GetUserRec(id)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Warning("Could not find user record.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
			return
		}

		panic(err)
	}

	if (userRec.Login == "superuser" || userRec.Login == "testuser") && !ctx.IsSuperUser() {
		ctx.Warning("'superuser' and 'testuser' account can not be updated.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
		return
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `insert into
					userRole(userId, roleId)
					values(?, ?)`

	_, err = tx.Exec(sqlStr, id, userRoleId)
	if err != nil {
		tx.Rollback()
		if err, ok := err.(*mysql.MySQLError); ok {
			if err.Number == 1452 {
				ctx.Warning("Could not find parent record.")
				ctx.Redirect(ctx.Cargo.HtmlUrl("/user_role", "id", "key", "pn", "roleId", "status"))
				return
			} else if err.Number == 1062 {
				ctx.Warning("Duplicate record.")
				ctx.Redirect(ctx.Cargo.HtmlUrl("/user_role", "id", "key", "pn", "roleId", "status"))
				return
			}
		}

		panic(err)
	}

	tx.Commit()

	ctx.Success("Role has been granted.")
	ctx.Redirect(ctx.Cargo.HtmlUrl("/user_role", "id", "key", "pn", "roleId", "status"))
}
