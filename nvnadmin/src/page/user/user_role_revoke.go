package user

import (
	"database/sql"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/page/role/role_lib"
	"nvnadmin/src/page/user/user_lib"
)

func UserRoleRevoke(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("user:role_revoke") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddInt("userRoleId", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddInt("roleId", -1)
	ctx.Cargo.AddStr("status", "default")
	ctx.ReadCargo()

	id := ctx.Cargo.Int("id")
	userRoleId := ctx.Cargo.Int("userRoleId")

	if id == -1 || userRoleId == -1 {
		ctx.Warning("Record could not be found.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/user_role", "id", "key", "pn", "roleId", "status"))
		return
	}

	userRec, err := user_lib.GetUserRec(id)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Warning("Record could not be found.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
			return
		}

		panic(err)
	}

	if (userRec.Login == "superuser" || userRec.Login == "testuser") && !ctx.IsSuperUser() {
		ctx.Warning("'superuser' and 'testuser' account can not be updated.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
		return
	}

	numRoles, err := role_lib.CountRoleByUser(id)
	if err != nil {
		panic(err)
	}

	if numRoles == 1 {
		ctx.Warning("Last user role can not be deleted.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/user_role", "id", "key", "pn", "roleId", "status"))
		return
	}

	numAdminUser := user_lib.CountAdminUser()
	if numAdminUser < 3 {
		roleRec, err := role_lib.GetRoleRec(userRoleId)
		if err != nil {
			if err == sql.ErrNoRows {
				ctx.Warning("Could not find role record.")
				ctx.Redirect(ctx.Cargo.HtmlUrl("/user_role", "id", "key", "pn", "roleId", "status"))
				return
			}

			panic(err)
		}

		if roleRec.Name == "Admin" {
			ctx.Warning("Last admin role can not be revoked.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/user_role", "id", "key", "pn", "roleId", "status"))
			return
		}
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `delete from
					userRole
				where
					userId = ? and
					roleId = ?`

	res, err := tx.Exec(sqlStr, id, userRoleId)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	if n, _ := res.RowsAffected(); n == 0 {
		tx.Rollback()
		ctx.Warning("Record could not be found.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/user_role", "id", "key", "pn", "roleId", "status"))
		return
	}

	tx.Commit()
	ctx.Success("Record has been deleted.")
	ctx.Redirect(ctx.Cargo.HtmlUrl("/user_role", "id", "key", "pn", "roleId", "status"))
}
