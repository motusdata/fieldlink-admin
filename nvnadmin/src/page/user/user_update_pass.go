package user

import (
	"database/sql"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/user/user_lib"
)

func UpdatePass(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("user:update_pass") {
		app.BadRequest()
	}

	ctx.Include.Js("/asset/js/page/user_update_pass.js")

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddInt("roleId", -1)
	ctx.Cargo.AddStr("status", "default")
	ctx.ReadCargo()

	id := ctx.Cargo.Int("id")
	rec, err := user_lib.GetUserRec(id)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Warning("Record could not be found.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
			return
		}

		panic(err)
	}

	if (rec.Login == "superuser" || rec.Login == "testuser") && !ctx.IsSuperUser() {
		ctx.Warning("'Superuser' and 'testuser' account can not be updated.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
		return
	}

	if ctx.Req.Method == "GET" {
		updatePassForm(ctx, rec)
		return
	}

	password := ctx.Req.PostFormValue("password")

	if password == "" {
		ctx.Warning("You have left one or more fields empty.")
		updatePassForm(ctx, rec)
		return
	}

	if err := util.IsValidPassword(password); err != nil {
		ctx.Warning(err.Error())
		updatePassForm(ctx, rec)
		return
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	password = util.PasswordHash(password)

	sqlStr := `update user set
					password = ?
				where
					userId = ?`

	res, err := tx.Exec(sqlStr, password, id)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	if n, _ := res.RowsAffected(); n == 0 {
		tx.Rollback()
		ctx.Warning("You did not change the record.")
		updatePassForm(ctx, rec)
		return
	}

	tx.Commit()

	ctx.Success("User password has been changed.")
	ctx.Redirect(ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status"))
}

func updatePassForm(ctx *context.Ctx, rec *user_lib.UserRec) {
	content.Include(ctx)

	ctx.Include.Js("/asset/js/page/user/user_update_pass.js")

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Users", "Update Password"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	urlStr := ctx.Cargo.HtmlUrl("/user", "key", "pn", "roleId", "status")
	buf.Add("<a href=\"%s\" class=\"btn btn-warning btn-md\"\">Back</a>", urlStr)
	buf.Add("</div>")

	urlStr = ctx.Cargo.HtmlUrl("/user_update_pass", "id", "key", "pn", "roleId", "status")
	buf.Add("<form action=\"%s\" method=\"post\">", urlStr)

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"login\">Login Name:</label>")
	buf.Add("<input type=\"text\" name=\"login\" id=\"login\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"30\" disabled>", util.ScrStr(rec.Login))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"password\">Password:</label>")
	buf.Add("<input type=\"password\" name=\"password\" id=\"password\" class=\"form-control\"" +
		" value=\"\" maxlength=\"30\" tabindex=\"1\">")
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<button type=\"submit\" class=\"btn btn-primary\" tabindex=\"2\">Submit</button>")
	buf.Add("<button type=\"reset\" class=\"btn btn-default\" tabindex=\"3\">Reset</button>")
	buf.Add("</div>")

	buf.Add("</form>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "user", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
