package well

import (
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/combo"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/ruler"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/well/well_lib"
)

func Browse(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if ctx.Req.URL.Path != "/" && ctx.Req.URL.Path != "/well" {
		app.NotFound()
	}

	if !ctx.IsRight("well:browse") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddStr("status", "default")
	ctx.Cargo.AddInt("operatorId", -1)
	ctx.Cargo.AddInt("wellTypeId", -1)
	ctx.Cargo.AddInt("countyId", -1)
	ctx.ReadCargo()

	content.Include(ctx)

	browseMid(ctx)

	content.Default(ctx)

	content.Search(ctx, "/well")

	lmenu := left_menu.New()
	lmenu.Set(ctx, "well", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}

func browseMid(ctx *context.Ctx) {
	ctx.Include.Js("/asset/js/page/well/well.js")

	key := ctx.Cargo.Str("key")
	pageNo := ctx.Cargo.Int("pn")
	operatorId := ctx.Cargo.Int("operatorId")
	wellTypeId := ctx.Cargo.Int("wellTypeId")
	countyId := ctx.Cargo.Int("countyId")
	status := ctx.Cargo.Str("status")

	operatorCombo := combo.NewCombo("select operatorId, name from operator order by name", "All Operators.")
	operatorCombo.Set()

	if operatorCombo.IsEmpty() {
		ctx.Warning("Operator list is empty. You should enter at least one operator.")
	}

	countyCombo := combo.NewCombo("select countyId, name from county order by name", "All Counties.")
	countyCombo.Set()

	if countyCombo.IsEmpty() {
		ctx.Warning("Well type list is empty. You should enter at least one county.")
	}

	wellTypeCombo := combo.NewCombo("select wellTypeId, name from wellType", "All Well Types")
	wellTypeCombo.Set()

	if wellTypeCombo.IsEmpty() {
		ctx.Warning("Well type list is empty. You should enter at least one well type.")
	}

	totalRows := well_lib.CountWell(key, operatorId, countyId, wellTypeId, status)
	if totalRows == 0 {
		ctx.Warning("Empty list.")
	}

	statusCombo := combo.NewEnumCombo()
	statusCombo.Add("default", "All Statuses")
	statusCombo.Add("active", "Active")
	statusCombo.Add("passive", "Passive")

	pageNo = ctx.TouchPageNo(pageNo, totalRows)

	insertRight := ctx.IsRight("well:insert")
	updateRight := ctx.IsRight("well:update")
	deleteRight := ctx.IsRight("well:delete")

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")
	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Wells"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	var urlStr string

	buf.Add("<div class=\"cmd\">")

	if insertRight {
		urlStr = ctx.Cargo.HtmlUrl("/well_insert", "key", "pn", "operatorId", "countyId", "wellTypeId", "status")
		buf.Add("<a href=\"%s\" class=\"btn btn-primary btn-md\" title=\"New record.\">New</a>", urlStr)
	}

	//operatorId form
	buf.Add("<form id=\"operatorForm\" class=\"form form-inline\" action=\"/well\" method=\"get\">")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<select name=\"operatorId\" id=\"operatorId\" class=\"form-control\" title=\"Select operator\">")

	buf.Add(operatorCombo.Format(operatorId))

	buf.Add("</select>")
	buf.Add("</div>")
	buf.Add(content.HiddenCargo(ctx, "key", "countyId", "wellTypeId", "status"))
	buf.Add("</form>")

	//countyId form
	buf.Add("<form id=\"countyForm\" class=\"form form-inline\" action=\"/well\" method=\"get\">")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<select name=\"countyId\" id=\"countyId\" class=\"form-control\" title=\"Select county\">")

	buf.Add(countyCombo.Format(countyId))

	buf.Add("</select>")
	buf.Add("</div>")
	buf.Add(content.HiddenCargo(ctx, "key", "operatorId", "wellTypeId", "status"))
	buf.Add("</form>")

	//wellTypeId form
	buf.Add("<form id=\"wellTypeForm\" class=\"form form-inline\" action=\"/well\" method=\"get\">")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<select name=\"wellTypeId\" id=\"wellTypeId\" class=\"form-control\" title=\"Select well type\">")

	buf.Add(wellTypeCombo.Format(wellTypeId))

	buf.Add("</select>")
	buf.Add("</div>")
	buf.Add(content.HiddenCargo(ctx, "key", "operatorId", "countyId", "status"))
	buf.Add("</form>")

	//status form
	buf.Add("<form id=\"statusForm\" class=\"form form-inline\" action=\"/well\" method=\"get\">")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<select name=\"status\" id=\"status\" class=\"form-control\">")

	buf.Add(statusCombo.Format(status))

	buf.Add("</select>")
	buf.Add("</div>")
	buf.Add(content.HiddenCargo(ctx, "key", "operatorId", "countyId", "wellTypeId"))
	buf.Add("</form>")

	buf.Add("</div>") //cmd

	buf.Add("<table>")
	buf.Add("<thead>")
	buf.Add("<tr>")

	buf.Add("<th class=\"fixed-middle\">Name</th>")
	buf.Add("<th>API#</th>")
	buf.Add("<th>Operator</th>")
	buf.Add("<th>Lease Name</th>")
	buf.Add("<th>Lease No</th>")
	buf.Add("<th class=\"fixed-zero\">Status</th>")

	if updateRight || deleteRight {
		buf.Add("<th class=\"cmd cmd-table\">Command</th>")
	}

	buf.Add("</tr>")
	buf.Add("</thead>")

	buf.Add("<tbody>")

	if totalRows > 0 {
		wellList := well_lib.GetWellPage(ctx, key, pageNo, operatorId, countyId, wellTypeId, status)

		var name, api, leaseName, operatorName, leaseNo string
		for _, row := range wellList {
			ctx.Cargo.SetInt("id", row.WellId)

			name = util.ScrStr(row.Name)
			api = util.ScrStr(row.Api)
			operatorName = util.ScrStr(row.OperatorName)
			leaseName = util.ScrStr(row.LeaseName)
			leaseNo = util.ScrStr(row.LeaseNo)

			if key != "" {
				name = content.Find(name, key)
				api = content.Find(api, key)
				operatorName = content.Find(operatorName, key)
			}

			buf.Add("<tr>")

			urlStr = ctx.Cargo.HtmlUrl("/well_display", "id", "key", "pn", "operatorId", "countyId", "wellTypeId", "status")
			buf.Add("<td><a href=\"%s\" title=\"Display record.\">%s</a></td>", urlStr, name)
			buf.Add("<td>%s</td>", api)
			buf.Add("<td>%s</td>", operatorName)
			buf.Add("<td>%s</td>", leaseName)
			buf.Add("<td>%s</td>", leaseNo)

			buf.Add("<td>%s</td>", well_lib.StatusToLabel(row.Status))

			if updateRight || deleteRight {
				buf.Add("<td class=\"cmd cmd-table\">")

				if updateRight {
					urlStr = ctx.Cargo.HtmlUrl("/well_update", "id", "key", "pn", "operatorId", "countyId", "wellTypeId", "status")
					buf.Add("<a href=\"%s\" class=\"btn btn-default btn-sm\" title=\"Edit record.\">Edit</a>", urlStr)
				}

				if deleteRight {
					urlStr = ctx.Cargo.HtmlUrl("/well_delete", "id", "key", "pn", "operatorId", "countyId", "wellTypeId", "status")
					buf.Add("<a href=\"%s\" class=\"btn btn-danger btn-sm\" title=\"Delete record.\">Delete</a>", urlStr)
				}

				buf.Add("</td>")
			}

			buf.Add("</tr>")
		}
	}

	buf.Add("</tbody>")
	buf.Add("</table>")

	if totalRows > 0 {
		ruler := ruler.NewRuler(totalRows, pageNo, ctx.Cargo.HtmlUrl("/well", "key", "operatorId", "countyId", "wellTypeId", "status"))
		buf.Add(ruler.Set(ctx))
	}

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())
}
