package well

import (
	"database/sql"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/well/well_lib"
)

func Delete(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("well:delete") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddStr("confirm", "no")
	ctx.Cargo.AddInt("operatorId", -1)
	ctx.Cargo.AddInt("countyId", -1)
	ctx.Cargo.AddInt("wellTypeId", -1)
	ctx.Cargo.AddStr("status", "default")
	ctx.ReadCargo()

	id := ctx.Cargo.Int("id")
	rec, err := well_lib.GetWellRec(id)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Warning("Record could not be found.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/well", "key", "pn", "operatorId", "countyId", "wellTypeId", "status"))
			return
		}

		panic(err)
	}

	if ctx.Cargo.Str("confirm") != "yes" {
		deleteConfirm(ctx, rec)
		return
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `delete from
					well
				where
					wellId = ?`

	res, err := tx.Exec(sqlStr, id)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	if n, _ := res.RowsAffected(); n == 0 {
		tx.Rollback()
		ctx.Warning("Record could not be found.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/well", "key", "pn", "operatorId", "countyId", "wellTypeId", "status"))
		return
	}

	tx.Commit()

	ctx.Success("Record has been deleted.")
	ctx.Redirect(ctx.Cargo.HtmlUrl("/well", "key", "pn", "operatorId", "countyId", "wellTypeId", "status"))
}

func deleteConfirm(ctx *context.Ctx, rec *well_lib.WellRec) {
	content.Include(ctx)

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Well", "Delete Record"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	urlStr := ctx.Cargo.HtmlUrl("/well", "key", "pn", "operatorId", "countyId", "wellTypeId", "status")
	buf.Add("<a href=\"%s\" class=\"btn btn-warning btn-md\"\">Back</a>", urlStr)
	buf.Add("</div>")

	buf.Add("<table>")
	buf.Add("<tbody>")
	buf.Add("<tr><th class=\"fixed-middle\">Name:</th><td>%s</td></tr>", util.ScrStr(rec.Name))
	buf.Add("<tr><th>API#:</th><td>%s</td></tr>", util.ScrStr(rec.Api))
	buf.Add("<tr><th>Operator:</th><td>%s</td></tr>", util.ScrStr(rec.OperatorName))
	buf.Add("<tr><th>Lease Name:</th><td>%s</td></tr>", util.ScrStr(rec.LeaseName))
	buf.Add("<tr><th>Lease No:</th><td>%s</td></tr>", util.ScrStr(rec.LeaseNo))
	buf.Add("<tr><th>Field Name:</th><td>%s</td></tr>", util.ScrStr(rec.FieldName))
	buf.Add("<tr><th>Status:</th><td>%s</td></tr>", well_lib.StatusToLabel(rec.Status))
	buf.Add("<tr>")

	buf.Add("</tr>")

	buf.Add("</tbody>")
	buf.Add("</table>")

	buf.Add("<div class=\"callout callout-danger\">")
	buf.Add("<h3>Please confirm:</h3>")
	buf.Add("<p>Do you ready want to delete this record?</p>")
	buf.Add("</div>")

	ctx.Cargo.SetStr("confirm", "yes")
	urlStr = ctx.Cargo.HtmlUrl("/well_delete", "id", "key", "pn", "confirm", "operatorId", "countyId", "wellTypeId", "status")

	buf.Add("<div class=\"cmd\">")
	buf.Add("<a href=\"%s\" class=\"btn btn-danger btn-md\">Yes</a>", urlStr)
	buf.Add("</div>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "well", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
