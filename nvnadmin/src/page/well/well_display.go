package well

import (
	"database/sql"
	"fmt"
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/well/well_lib"
)

func Display(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("well:delete") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddInt("operatorId", -1)
	ctx.Cargo.AddInt("countyId", -1)
	ctx.Cargo.AddInt("wellTypeId", -1)
	ctx.Cargo.AddStr("status", "default")
	ctx.ReadCargo()

	id := ctx.Cargo.Int("id")
	rec, err := well_lib.GetWellRec(id)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Warning("Record could not be found.")
			ctx.Redirect(ctx.Cargo.HtmlUrl("/well", "key", "pn", "operatorId", "countyId", "wellTypeId", "status"))
			return
		}

		panic(err)
	}

	displayWell(ctx, rec)
}

func displayWell(ctx *context.Ctx, rec *well_lib.WellRec) {
	content.Include(ctx)

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Well", "Display Record"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	urlStr := ctx.Cargo.HtmlUrl("/well", "key", "pn", "operatorId", "countyId", "wellTypeId", "status")
	buf.Add("<a href=\"%s\" class=\"btn btn-warning btn-md\"\">Back</a>", urlStr)
	buf.Add("</div>")

	buf.Add("<table>")
	buf.Add("<tbody>")
	buf.Add("<tr><th class=\"fixed-middle\">Name:</th><td>%s</td></tr>", util.ScrStr(rec.Name))
	buf.Add("<tr><th>API#:</th><td>%s</td></tr>", util.ScrStr(rec.Api))
	buf.Add("<tr><th>Operator:</th><td>%s</td></tr>", util.ScrStr(rec.OperatorName))
	buf.Add("<tr><th>Lease Name:</th><td>%s</td></tr>", util.ScrStr(rec.LeaseName))
	buf.Add("<tr><th>Lease No:</th><td>%s</td></tr>", util.ScrStr(rec.LeaseNo))
	buf.Add("<tr><th>Field Name:</th><td>%s</td></tr>", util.ScrStr(rec.FieldName))
	buf.Add("<tr><th>Closest City:</th><td>%s</td></tr>", util.ScrStr(rec.ClosestCity))
	buf.Add("<tr><th>Well Type:</th><td>%s</td></tr>", util.ScrStr(rec.WellTypeName))
	buf.Add("<tr><th>County:</th><td>%s</td></tr>", util.ScrStr(rec.CountyName))

	if rec.LatLong != "" {
		urlStr = fmt.Sprintf("https://www.google.com/maps/search/?api=1&query=%s", rec.LatLong)
		buf.Add("<th>Latitude/Longitude</th>")
		buf.Add("<td>%s <a href=\"%s\" target=_blank class=\"map-link\"><i class=\"fa fa-map-marker\"></i> View</a></td>",
			rec.LatLong, urlStr)
	}

	buf.Add("<tr><th>TVD:</th><td>%0.4f ft</td></tr>", rec.Tvd)
	buf.Add("<tr><th>Production:</th><td>%s</td></tr>", util.ScrStr(rec.Production))
	buf.Add("<tr><th>Status:</th><td>%s</td></tr>", well_lib.StatusToLabel(rec.Status))

	buf.Add("</tr>")

	buf.Add("</tbody>")
	buf.Add("</table>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "well", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
