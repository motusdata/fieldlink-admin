package well

import (
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/combo"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"strconv"

	"github.com/go-sql-driver/mysql"
)

func Insert(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if !ctx.IsRight("well:insert") {
		app.BadRequest()
	}

	ctx.Cargo.AddStr("key", "")
	ctx.Cargo.AddInt("pn", 1)
	ctx.Cargo.AddInt("operatorId", -1)
	ctx.Cargo.AddInt("countyId", -1)
	ctx.Cargo.AddInt("wellTypeId", -1)

	ctx.Cargo.AddStr("status", "default")
	ctx.ReadCargo()

	if ctx.Req.Method == "GET" {
		insertForm(ctx)
		return
	}

	name := ctx.Req.PostFormValue("name")
	api := ctx.Req.PostFormValue("api")
	leaseName := ctx.Req.PostFormValue("leaseName")
	leaseNo := ctx.Req.PostFormValue("leaseNo")
	fieldName := ctx.Req.PostFormValue("fieldName")
	closestCity := ctx.Req.PostFormValue("closestCity")

	formOperatorId, err := strconv.ParseInt(ctx.Req.PostFormValue("formOperatorId"), 10, 64)
	if err != nil {
		formOperatorId = -1
	}

	formWellTypeId, err := strconv.ParseInt(ctx.Req.PostFormValue("formWellTypeId"), 10, 64)
	if err != nil {
		formWellTypeId = -1
	}

	formCountyId, err := strconv.ParseInt(ctx.Req.PostFormValue("formCountyId"), 10, 64)
	if err != nil {
		formCountyId = -1
	}

	latLong := ctx.Req.PostFormValue("latLong")

	tvd, err := strconv.ParseFloat(ctx.Req.PostFormValue("tvd"), 64)
	if err != nil {
		formCountyId = 0
	}

	production := ctx.Req.PostFormValue("production")
	formStatus := ctx.Req.PostFormValue("formStatus")

	if formOperatorId == -1 || name == "" || api == "" || leaseName == "" ||
		leaseNo == "" || fieldName == "" || closestCity == "" || formWellTypeId == -1 ||
		formCountyId == -1 || formStatus == "default" {
		ctx.Warning("You have left one or more fields empty.")
		insertForm(ctx)
		return
	}

	tx, err := app.Db.Begin()
	if err != nil {
		panic(err)
	}

	sqlStr := `insert into
					well(wellId, operatorId, name, api, leaseName, leaseNo, fieldName,
					closestCity, wellTypeId, countyId, latLong, tvd, production, status)
					values(null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`

	_, err = tx.Exec(sqlStr, formOperatorId, name, api, leaseName, leaseNo, fieldName,
		closestCity, formWellTypeId, formCountyId, latLong, tvd, production, formStatus)
	if err != nil {
		tx.Rollback()
		if err, ok := err.(*mysql.MySQLError); ok {
			if err.Number == 1062 {
				ctx.Warning("Duplicate record.")
				insertForm(ctx)
				return
			}
		}

		panic(err)
	}

	tx.Commit()

	ctx.Success("Record has been saved.")
	ctx.Redirect(ctx.Cargo.HtmlUrl("/well", "key", "pn", "operatorId", "countyId", "wellTypeId", "status"))
}

func insertForm(ctx *context.Ctx) {
	content.Include(ctx)

	operatorId := ctx.Cargo.Int("operatorId")
	wellTypeId := ctx.Cargo.Int("wellTypeId")
	countyId := ctx.Cargo.Int("countyId")

	var name, api, leaseName, leaseNo, fieldName, closestCity, latLong,
		production, formStatus string
	var formOperatorId, formWellTypeId, formCountyId int64
	var tvd float64
	var err error

	if ctx.Req.Method == "POST" {
		formOperatorId, err = strconv.ParseInt(ctx.Req.PostFormValue("formOperatorId"), 10, 64)
		if err != nil {
			formOperatorId = -1
		}

		name = ctx.Req.PostFormValue("name")
		api = ctx.Req.PostFormValue("api")
		leaseName = ctx.Req.PostFormValue("leaseName")
		leaseNo = ctx.Req.PostFormValue("leaseNo")
		fieldName = ctx.Req.PostFormValue("fieldName")
		closestCity = ctx.Req.PostFormValue("closestCity")

		formWellTypeId, err = strconv.ParseInt(ctx.Req.PostFormValue("formWellTypeId"), 10, 64)
		if err != nil {
			formWellTypeId = -1
		}

		formCountyId, err = strconv.ParseInt(ctx.Req.PostFormValue("formCountyId"), 10, 64)
		if err != nil {
			formCountyId = -1
		}

		latLong = ctx.Req.PostFormValue("latLong")

		tvd, err = strconv.ParseFloat(ctx.Req.PostFormValue("tvd"), 64)
		if err != nil {
			formCountyId = 0
		}

		production = ctx.Req.PostFormValue("production")
		formStatus = ctx.Req.PostFormValue("formStatus")
	} else {
		name = ""
		formOperatorId = operatorId
		api = ""
		leaseName = ""
		leaseNo = ""
		fieldName = ""
		closestCity = ""
		formWellTypeId = wellTypeId
		formCountyId = countyId
		latLong = ""
		tvd = 0
		production = ""
		formStatus = "active"
	}

	operatorCombo := combo.NewCombo("select operatorId, name from operator order by name", "Select Operator")
	operatorCombo.Set()

	if operatorCombo.IsEmpty() {
		ctx.Warning("Operator table is empty. You should enter at least one operator first.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/well", "key", "pn", "operatorId", "countyId", "wellTypeId", "status"))
		return
	}

	wellTypeCombo := combo.NewCombo("select wellTypeId, name from wellType", "Select Well Type")
	wellTypeCombo.Set()

	if wellTypeCombo.IsEmpty() {
		ctx.Warning("Well type table is empty. You should enter at least one well type first.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/well", "key", "pn", "operatorId", "countyId", "wellTypeId", "status"))
		return
	}

	statusCombo := combo.NewEnumCombo()
	statusCombo.Add("default", "Select Status")
	statusCombo.Add("active", "Active")
	statusCombo.Add("passive", "Passive")

	countyCombo := combo.NewCombo("select countyId, name from county order by name", "Select County")
	countyCombo.Set()

	if countyCombo.IsEmpty() {
		ctx.Warning("County table is empty. You should enter at least one county first.")
		ctx.Redirect(ctx.Cargo.HtmlUrl("/well", "key", "pn", "operatorId", "countyId", "wellTypeId", "status"))
		return
	}

	buf := util.NewBuf()
	buf.Add("<div class=\"panel panel-primary\">")

	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Well", "New Record"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	buf.Add("<div class=\"cmd\">")
	urlStr := ctx.Cargo.HtmlUrl("/well", "key", "pn", "operatorId", "countyId", "wellTypeId", "status")
	buf.Add("<a href=\"%s\" class=\"btn btn-warning btn-md\"\">Back</a>", urlStr)
	buf.Add("</div>")

	urlStr = ctx.Cargo.HtmlUrl("/well_insert", "key", "pn", "operatorId", "countyId", "wellTypeId", "status")
	buf.Add("<form action=\"%s\" method=\"post\">", urlStr)

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"formOperatorId\">Operator:</label>")
	buf.Add("<select name=\"formOperatorId\" id=\"formOperatorId\" class=\"form-control\" tabindex=\"1\" autofocus>")

	buf.Add(operatorCombo.Format(formOperatorId))

	buf.Add("</select>")
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"name\">Name:</label>")
	buf.Add("<input type=\"text\" name=\"name\" id=\"name\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"100\" tabindex=\"2\">", util.ScrStr(name))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"api\">API#:</label>")
	buf.Add("<input type=\"text\" name=\"api\" id=\"api\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"50\" tabindex=\"3\">", util.ScrStr(api))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"leaseName\">Lease Name:</label>")
	buf.Add("<input type=\"text\" name=\"leaseName\" id=\"leaseName\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"100\" tabindex=\"4\">", util.ScrStr(leaseName))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"leaseNo\">Lease No:</label>")
	buf.Add("<input type=\"text\" name=\"leaseNo\" id=\"leaseNo\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"50\" tabindex=\"5\">", util.ScrStr(leaseNo))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"fieldName\">Field Name:</label>")
	buf.Add("<input type=\"text\" name=\"fieldName\" id=\"fieldName\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"100\" tabindex=\"6\">", util.ScrStr(fieldName))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"fieldName\">Closest City:</label>")
	buf.Add("<input type=\"text\" name=\"closestCity\" id=\"closestCity\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"50\" tabindex=\"7\">", util.ScrStr(closestCity))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"formWellTypeId\">Well Type:</label>")
	buf.Add("<select name=\"formWellTypeId\" id=\"formWellTypeId\" class=\"form-control\" tabindex=\"8\">")

	buf.Add(wellTypeCombo.Format(formWellTypeId))

	buf.Add("</select>")
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"formCountyId\">Role:</label>")
	buf.Add("<select name=\"formCountyId\" id=\"formCountyId\" class=\"form-control\" tabindex=\"9\">")

	buf.Add(countyCombo.Format(formCountyId))

	buf.Add("</select>")
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label for=\"latLong\">Latitude/Longitude:</label>")
	buf.Add("<input type=\"text\" name=\"latLong\" id=\"latLong\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"100\" tabindex=\"10\">", util.ScrStr(latLong))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label for=\"tvd\">TVD:</label>")
	buf.Add("<input type=\"text\" name=\"tvd\" id=\"tvd\" class=\"form-control\""+
		" value=\"%0.4f\" maxlength=\"50\" tabindex=\"11\">", tvd)
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label for=\"production\">Production:</label>")
	buf.Add("<input type=\"text\" name=\"production\" id=\"production\" class=\"form-control\""+
		" value=\"%s\" maxlength=\"50\" tabindex=\"12\">", util.ScrStr(production))
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<label class=\"required\" for=\"formStatus\">Status:</label>")
	buf.Add("<select name=\"formStatus\" id=\"formStatus\" class=\"form-control\" tabindex=\"13\">")

	buf.Add(statusCombo.Format(formStatus))

	buf.Add("</select>")
	buf.Add("</div>")

	buf.Add("<div class=\"form-group\">")
	buf.Add("<button type=\"submit\" class=\"btn btn-primary\" tabindex=\"14\">Submit</button>")
	buf.Add("<button type=\"reset\" class=\"btn btn-default\" tabindex=\"15\">Reset</button>")
	buf.Add("</div>")

	buf.Add("</form>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "well", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}
