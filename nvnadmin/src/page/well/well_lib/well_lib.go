package well_lib

import (
	"nvnadmin/src/app"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
)

type WellRec struct {
	WellId       int64
	OperatorId   int64
	OperatorName string
	Name         string
	Api          string
	LeaseName    string
	LeaseNo      string
	FieldName    string
	ClosestCity  string
	WellTypeId   int64
	WellTypeName string
	CountyId    int64
	CountyName  string
	LatLong      string
	Tvd          float64
	Production   string
	Status       string
}

func GetWellRec(id int64) (*WellRec, error) {
	sqlStr := `select
					well.wellId,
					well.operatorId,
					operator.name,
					well.name,
					well.api,
					well.leaseName,
					well.leaseNo,
					well.fieldName,
					well.closestCity,
					well.wellTypeId,
					wellType.name as wellTypeName,
					well.countyId,
					county.name,
					well.latLong,
					well.tvd,
					well.production,
					well.status
				from
					wellType,
					well,
					operator,
					county
				where
					well.wellId = ? and
					well.wellTypeId = wellType.wellTypeId and
					well.operatorId = operator.operatorId and
					well.countyId = county.countyId`

	row := app.Db.QueryRow(sqlStr, id)

	rec := new(WellRec)
	err := row.Scan(&rec.WellId, &rec.OperatorId, &rec.OperatorName, &rec.Name,
		&rec.Api, &rec.LeaseName, &rec.LeaseNo, &rec.FieldName, &rec.ClosestCity,
		&rec.WellTypeId, &rec.WellTypeName, &rec.CountyId, &rec.CountyName,
		&rec.LatLong, &rec.Tvd, &rec.Production, &rec.Status)
	if err != nil {
		return nil, err
	}

	return rec, nil
}

func CountWell(key string, operatorId, countyId, wellTypeId int64, status string) int64 {
	sqlBuf := util.NewBuf()
	sqlBuf.Add(`select
					count(*)
				from
					well`)

	conBuf := util.NewBuf()

	if operatorId != -1 {
		conBuf.Add("(well.operatorId = %d)", operatorId)
	}

	if countyId != -1 {
		conBuf.Add("(well.countyId = %d)", countyId)
	}

	if wellTypeId != -1 {
		conBuf.Add("(well.wellTypeId = %d)", wellTypeId)
	}

	if status != "default" {
		conBuf.Add("(well.Status = '%s')", status)
	}

	if key != "" {
		key = util.DbStr(key)
		conBuf.Add("(well.name like('%%%s%%')"+
			" or well.operator like('%%%s%%')"+
			" or well.api like('%%%s%%')"+
			" or well.leaseName like('%%%s%%'))",
			key, key, key)
	}

	if !conBuf.IsEmpty() {
		sqlBuf.Add("where")
		sqlBuf.Add(*conBuf.StringSep(" and "))
	}

	row := app.Db.QueryRow(*sqlBuf.String())

	var rv int64
	err := row.Scan(&rv)
	if err != nil {
		panic(err)
	}

	return rv
}

func GetWellPage(ctx *context.Ctx, key string, pageNo, operatorId, countyId, wellTypeId int64, status string) []*WellRec {
	sqlBuf := util.NewBuf()
	sqlBuf.Add(`select
					well.wellId,
					well.operatorId,
					operator.name,
					well.name,
					well.api,
					well.leaseName,
					well.leaseNo,
					well.fieldName,
					well.closestCity,
					well.wellTypeId,
					wellType.name,
					well.countyId,
					county.name,
					well.latLong,
					well.tvd,
					well.production,
					well.status
				from
					wellType,
					well,
					operator,
					county
				where
					well.wellTypeId = wellType.wellTypeId and
					well.operatorId = operator.operatorId and
					well.countyId = county.countyId`)

	conBuf := util.NewBuf()

	if operatorId != -1 {
		conBuf.Add("(well.operatorId = %d)", operatorId)
	}

	if countyId != -1 {
		conBuf.Add("(well.countyId = %d)", countyId)
	}

	if wellTypeId != -1 {
		conBuf.Add("(well.wellTypeId = %d)", wellTypeId)
	}

	if status != "default" {
		conBuf.Add("(well.Status = '%s')", status)
	}

	if key != "" {
		key = util.DbStr(key)
		conBuf.Add("(well.name like('%%%s%%')"+
			" or well.api like('%%%s%%')"+
			" or well.leaseName like('%%%s%%'))",
			key, key, key)
	}

	if !conBuf.IsEmpty() {
		sqlBuf.Add("and")
		sqlBuf.Add(*conBuf.StringSep(" and "))
	}

	sqlBuf.Add("order by well.name")

	start := (pageNo - 1) * ctx.Config.PageLen
	sqlBuf.Add("limit %d, %d", start, ctx.Config.PageLen)

	rows, err := app.Db.Query(*sqlBuf.String())
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	rv := make([]*WellRec, 0, 100)
	for rows.Next() {
		rec := new(WellRec)
		err := rows.Scan(&rec.WellId, &rec.OperatorId, &rec.OperatorName, &rec.Name,
			&rec.Api, &rec.LeaseName, &rec.LeaseNo, &rec.FieldName, &rec.ClosestCity,
			&rec.WellTypeId, &rec.WellTypeName, &rec.CountyId, &rec.CountyName,
			&rec.LatLong, &rec.Tvd, &rec.Production, &rec.Status)
		if err != nil {
			panic(err)
		}

		rv = append(rv, rec)
	}

	return rv
}

func StatusToLabel(status string) string {
	switch status {
	case "active":
		return "<span class=\"label label-success label-sm\">Active</span>"
	case "passive":
		return "<span class=\"label label-danger label-sm\">Passive</span>"
	}

	return "<span class=\"label label-default label-sm\">Unknown</span>"
}
