package well_type

import (
	"net/http"
	"nvnadmin/src/app"
	"nvnadmin/src/content"
	"nvnadmin/src/content/left_menu"
	"nvnadmin/src/content/top_menu"
	"nvnadmin/src/lib/context"
	"nvnadmin/src/lib/util"
	"nvnadmin/src/page/well_type/well_type_lib"
)

func Browse(rw http.ResponseWriter, req *http.Request) {
	ctx := context.NewContext(rw, req)

	if ctx.Req.URL.Path != "/" && ctx.Req.URL.Path != "/well_type" {
		app.NotFound()
	}

	if !ctx.IsRight("well_type:browse") {
		app.BadRequest()
	}

	ctx.Cargo.AddInt("id", -1)
	ctx.ReadCargo()

	content.Include(ctx)

	browseMid(ctx)

	content.Default(ctx)

	lmenu := left_menu.New()
	lmenu.Set(ctx, "well_type", true)

	tmenu := top_menu.New()
	tmenu.Set(ctx, "root", false)

	ctx.Render("default.html")
}

func browseMid(ctx *context.Ctx) {
	insertRight := ctx.IsRight("well_type:insert")
	updateRight := ctx.IsRight("well_type:update")
	deleteRight := ctx.IsRight("well_type:delete")

	buf := util.NewBuf()

	buf.Add("<div class=\"panel panel-primary\">")
	buf.Add("<div class=\"panel-heading\">")
	buf.Add(content.PanelTitle("Well Types"))
	buf.Add("</div>")

	buf.Add("<div class=\"panel-body\">")

	if insertRight || updateRight {
		buf.Add("<div class=\"cmd\">")

		if insertRight {
			urlStr := ctx.Cargo.HtmlUrl("/well_type_insert")
			buf.Add("<a href=\"%s\" class=\"btn btn-primary btn-md\" title=\"New record.\">New</a>", urlStr)
		}

		buf.Add("<span id=\"lastUpdate\"></span>")

		buf.Add("</div>")
	}

	buf.Add("<table>")
	buf.Add("<thead>")
	buf.Add("<tr>")
	buf.Add("<th class=\"fixed-middle\">Name</th>")

	if updateRight || deleteRight {
		buf.Add("<th class=\"cmd cmd-table\">%s</th>", "Command")
	}

	buf.Add("</tr>")
	buf.Add("</thead>")

	buf.Add("<tbody>")

	wellTypeList := well_type_lib.GetWellTypePage(ctx)

	var name string
	for _, row := range wellTypeList {
		ctx.Cargo.SetInt("id", row.WellTypeId)

		name = util.ScrStr(row.Name)

		buf.Add("<tr>")
		buf.Add("<td>%s</td>", name)

		if updateRight || deleteRight {
			buf.Add("<td class=\"cmd cmd-table\">")

			if updateRight {
				urlStr := ctx.Cargo.HtmlUrl("/well_type_update", "id")
				buf.Add("<a href=\"%s\" class=\"btn btn-default btn-sm\" title=\"Edit record.\">Edit</a>", urlStr)
			}

			if deleteRight {
				urlStr := ctx.Cargo.HtmlUrl("/well_type_delete", "id")
				buf.Add("<a href=\"%s\" class=\"btn btn-danger btn-sm\" title=\"Delete record.\">Delete</a>", urlStr)
			}

			buf.Add("</td>")
		}

		buf.Add("</tr>")
	}

	buf.Add("</tbody>")
	buf.Add("</table>")

	buf.Add("</div>")
	buf.Add("</div>")

	ctx.AddFormat("midContent", buf.String())
}
