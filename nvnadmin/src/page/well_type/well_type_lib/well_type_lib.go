package well_type_lib

import (
	"nvnadmin/src/app"
	"nvnadmin/src/lib/context"
)

type WellTypeRec struct {
	WellTypeId int64
	Name       string
}

func GetWellTypeRec(id int64) (*WellTypeRec, error) {
	sqlStr := `select
					wellType.wellTypeId,
					wellType.name
				from
					wellType
				where
					wellTypeId = ?`

	row := app.Db.QueryRow(sqlStr, id)

	rec := new(WellTypeRec)
	err := row.Scan(&rec.WellTypeId, &rec.Name)
	if err != nil {
		return nil, err
	}

	return rec, nil
}

func CountWellType() int64 {
	sqlStr := "select count(*) from wellType"

	row := app.Db.QueryRow(sqlStr)

	var rv int64
	err := row.Scan(&rv)
	if err != nil {
		panic(err)
	}

	return rv
}

func GetWellTypePage(ctx *context.Ctx) []*WellTypeRec {
	sqlStr := `select
					wellType.wellTypeId,
					wellType.name
				from
					wellType
				order by wellType.name`

	rows, err := app.Db.Query(sqlStr)
	if err != nil {
		panic(err)
	}

	defer rows.Close()

	rv := make([]*WellTypeRec, 0, 100)
	for rows.Next() {
		rec := new(WellTypeRec)
		if err = rows.Scan(&rec.WellTypeId, &rec.Name); err != nil {
			panic(err)
		}

		rv = append(rv, rec)
	}

	return rv
}
