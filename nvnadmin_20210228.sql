CREATE DATABASE  IF NOT EXISTS `nvnadmin` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `nvnadmin`;
-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: localhost    Database: nvnadmin
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `configId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `enum` int(11) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `type` varchar(10) COLLATE utf8_bin NOT NULL,
  `value` varchar(250) COLLATE utf8_bin NOT NULL,
  `title` varchar(100) COLLATE utf8_bin NOT NULL,
  `exp` varchar(250) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`configId`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (1,10,'pageLen','int','20','Page Lenght:','Total number of records listed in a page.'),(4,20,'rulerLen','int','3','Ruler Length:','Number of links in a ruler that is shown in some pages.'),(5,40,'smtpHost','string','mail.nginworks.com','SMTP Server Host Name:','All notification email messages will be sent using this email host.'),(6,50,'smtpPort','int','587','SMTP Port Number:','Port number of the SMTP server.'),(7,60,'smtpUser','string','test@nginworks.com','SMTP User Name:','All emails will be sent via this email box and this user name will be shown in the \'From\' part of the email messages.'),(8,70,'smtpPassword','string','Ankara12','SMTP Password:','SMTP user\'s password.');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `county`
--

DROP TABLE IF EXISTS `county`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `county` (
  `countyId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`countyId`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `county`
--

LOCK TABLES `county` WRITE;
/*!40000 ALTER TABLE `county` DISABLE KEYS */;
INSERT INTO `county` VALUES (1,'ANDERSON'),(2,'ANDREWS'),(3,'ANGELINA'),(5,'ARCHER'),(6,'ARMSTRONG'),(7,'ATANCOSA'),(8,'AUSTIN'),(9,'BAILEY'),(10,'BANDERA'),(11,'BAYLOR'),(12,'BELL'),(13,'BEXAR'),(14,'CHEROKEE'),(15,'COLLIN'),(16,'COLORADO'),(17,'CORYELL'),(18,'DEAF SMITH'),(19,'HARDIN'),(20,'HUNT'),(21,'JACKSON'),(22,'JEFF DAVIS'),(23,'JEFFERSON'),(24,'KAUFMAN'),(25,'MADISON'),(26,'MAVERICK'),(27,'STEPHENS'),(28,'TARRANT'),(29,'TERRY'),(30,'WILSON'),(31,'ZAPATA');
/*!40000 ALTER TABLE `county` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gateway`
--

DROP TABLE IF EXISTS `gateway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gateway` (
  `gatewayId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `wellId` int(11) unsigned NOT NULL,
  `gatewayMnfId` int(11) unsigned NOT NULL,
  `sku` varchar(50) COLLATE utf8_bin NOT NULL,
  `mac` varchar(50) COLLATE utf8_bin NOT NULL,
  `version` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`gatewayId`),
  KEY `fk_gateway_1_idx` (`wellId`),
  KEY `fk_gateway_2_idx` (`gatewayMnfId`),
  CONSTRAINT `fk_gateway_1` FOREIGN KEY (`wellId`) REFERENCES `well` (`wellId`),
  CONSTRAINT `fk_gateway_2` FOREIGN KEY (`gatewayMnfId`) REFERENCES `gatewayMnf` (`gatewayMnfId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gateway`
--

LOCK TABLES `gateway` WRITE;
/*!40000 ALTER TABLE `gateway` DISABLE KEYS */;
INSERT INTO `gateway` VALUES (1,24,1,'testr','trtr','tr6tr'),(2,31,2,'test sku','test mac','sample version'),(3,25,1,'sample sku','sample mac','sample version'),(8,25,3,'ytyt','ytyt','ytyt');
/*!40000 ALTER TABLE `gateway` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gatewayMnf`
--

DROP TABLE IF EXISTS `gatewayMnf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gatewayMnf` (
  `gatewayMnfId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`gatewayMnfId`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gatewayMnf`
--

LOCK TABLES `gatewayMnf` WRITE;
/*!40000 ALTER TABLE `gatewayMnf` DISABLE KEYS */;
INSERT INTO `gatewayMnf` VALUES (3,'ASUS Tinker Board RK3288'),(1,'BeableBone Black'),(2,'Odroid-XU4'),(6,'SunCloud BeagleBone Enhanced Industrial'),(5,'SunCloud BeagleBone Enhanced WiFi 1G'),(4,'SunCloud BeagleBone Enhanced WiFi 512');
/*!40000 ALTER TABLE `gatewayMnf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operator`
--

DROP TABLE IF EXISTS `operator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operator` (
  `operatorId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `status` enum('active','passive') COLLATE utf8_bin NOT NULL DEFAULT 'active',
  PRIMARY KEY (`operatorId`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operator`
--

LOCK TABLES `operator` WRITE;
/*!40000 ALTER TABLE `operator` DISABLE KEYS */;
INSERT INTO `operator` VALUES (24,'XTO ENERGY INC.','active'),(25,'DEVON ENERGY PRODUCTION CO, L.P.','active'),(26,'LEWIS PETRO PROPERTIES, INC.','active'),(27,'BP AMERICA PRODUCTION COMPANY','active'),(28,'CIMAREX ENERGY CO.','active'),(29,'APACHE CORPORATION','active'),(30,'TEP BARNETT USA, LLC','active'),(31,'EOG RESOURCES, INC.','active'),(32,'ROCKCLIFF ENERGY OPERATING LLC','active'),(33,'ANADARKO E&P ONSHORE LLC','active'),(34,'SM ENERGY COMPANY','active'),(35,'BLACKBEARD OPERATING, LLC','active'),(36,'CHEVRON U. S. A. INC.','active'),(37,'SABINE OIL & GAS CORPORATION','active'),(40,'SN EF MAVERICK, LLC','active'),(41,'HILCORP ENERGY COMPANY','active'),(42,'WPX ENERGY PERMIAN, LLC','active'),(43,'COMSTOCK OIL & GAS, LLC','active'),(44,'CCI EAST TEXAS UPSTREAM LLC','active'),(45,'ROSETTA RESOURCES OPERATING LP','active'),(46,'UPP OPERATING, LLC','active'),(47,'SILVERBOW RESOURCES OPER, LLC','active'),(48,'MARATHON OIL EF LLC','active'),(49,'BPX OPERATING COMPANY','active'),(50,'MERIT ENERGY COMPANY','active'),(54,'FDL OPERATING, LLC','active'),(55,'REPSOL OIL & GAS USA, LLC','active'),(56,'BEDROCK PRODUCTION, LLC','active'),(57,'CHESAPEAKE OPERATING, L.L.C.','active'),(58,'BURLINGTON RESOURCES O & G CO LP','active'),(59,'TANOS EXPLORATION II, LLC','active'),(60,'UNIT PETROLEUM COMPANY','active'),(62,'ENDEAVOR ENERGY RESOURCES L.P.','active'),(65,'QEP ENERGY COMPANY','active');
/*!40000 ALTER TABLE `operator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `roleId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `exp` varchar(250) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`roleId`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (2,'Admin','Fieldlinksys admin role'),(5,'Operator','Fieldlinksys operator role');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roleRight`
--

DROP TABLE IF EXISTS `roleRight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roleRight` (
  `roleId` int(11) unsigned NOT NULL,
  `pageRight` varchar(100) COLLATE utf8_bin NOT NULL,
  UNIQUE KEY `roleRightUnique` (`pageRight`,`roleId`),
  KEY `fk_roleRight_1_idx` (`roleId`),
  CONSTRAINT `fk_roleRight_1` FOREIGN KEY (`roleId`) REFERENCES `role` (`roleId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roleRight`
--

LOCK TABLES `roleRight` WRITE;
/*!40000 ALTER TABLE `roleRight` DISABLE KEYS */;
INSERT INTO `roleRight` VALUES (2,'asset:browse'),(2,'asset:delete'),(2,'asset:insert'),(2,'asset:update'),(2,'config:browse'),(2,'config:set'),(2,'gateway:browse'),(2,'gateway:delete'),(2,'gateway:insert'),(2,'gateway:update'),(2,'profile:browse'),(2,'profile:update'),(2,'profile:update_password'),(2,'role:browse'),(2,'role:delete'),(2,'role:insert'),(2,'role:role_right'),(2,'role:update'),(2,'user:browse'),(2,'user:confirm'),(2,'user:delete'),(2,'user:insert'),(2,'user:role_browse'),(2,'user:role_grant'),(2,'user:role_revoke'),(2,'user:update'),(2,'user:update_pass'),(5,'field:browse'),(5,'field:delete'),(5,'field:insert'),(5,'field:update'),(5,'profile:browse'),(5,'profile:update'),(5,'profile:update_password');
/*!40000 ALTER TABLE `roleRight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensorMnf`
--

DROP TABLE IF EXISTS `sensorMnf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensorMnf` (
  `sensorMnfId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`sensorMnfId`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensorMnf`
--

LOCK TABLES `sensorMnf` WRITE;
/*!40000 ALTER TABLE `sensorMnf` DISABLE KEYS */;
INSERT INTO `sensorMnf` VALUES (8,'ABB'),(9,'Honeywell'),(7,'Noven'),(10,'Texas Instrument');
/*!40000 ALTER TABLE `sensorMnf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensorMonit`
--

DROP TABLE IF EXISTS `sensorMonit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensorMonit` (
  `sensorMonitId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `readTime` int(11) unsigned NOT NULL,
  `rawMv` int(11) NOT NULL DEFAULT '0',
  `rawUvVoltage` int(11) NOT NULL DEFAULT '0',
  `calibratedDataLbf` double NOT NULL DEFAULT '0',
  `loadCell` double NOT NULL DEFAULT '0',
  `casingPressure` double NOT NULL DEFAULT '0',
  `tubingPressure` double NOT NULL DEFAULT '0',
  `tankLevel` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`sensorMonitId`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensorMonit`
--

LOCK TABLES `sensorMonit` WRITE;
/*!40000 ALTER TABLE `sensorMonit` DISABLE KEYS */;
INSERT INTO `sensorMonit` VALUES (75,1610572888,0,0,0,0,0,0,0),(76,1610572913,6349,1065,17390.106449592986,17390.106449592986,0,0,0),(77,1610572946,0,0,0,0,0,0,0),(78,1610572971,6304,423,4328.674290554849,4328.674290554849,0,0,0),(79,1610573004,0,0,0,0,0,0,0),(80,1610573029,6306,2154,20000,20000,0,0,0),(81,1610573062,0,0,0,0,0,0,0),(82,1610573087,6304,1515,20000,20000,0,0,0),(83,1610573119,0,0,0,0,0,0,0),(84,1610573144,6309,2063,20000,20000,0,0,0),(85,1610573177,0,0,0,0,0,0,0),(86,1610573202,6348,2181,20000,20000,0,0,0),(87,1610573235,0,0,0,0,0,0,0),(88,1610573260,6351,1388,20000,20000,0,0,0);
/*!40000 ALTER TABLE `sensorMonit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensorType`
--

DROP TABLE IF EXISTS `sensorType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensorType` (
  `sensorTypeId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`sensorTypeId`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensorType`
--

LOCK TABLES `sensorType` WRITE;
/*!40000 ALTER TABLE `sensorType` DISABLE KEYS */;
INSERT INTO `sensorType` VALUES (14,'Casing Pressure'),(12,'Inclinometer'),(13,'Load Cell'),(16,'Tank Level'),(15,'Tubing Pressure');
/*!40000 ALTER TABLE `sensorType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `sessionId` varchar(40) COLLATE utf8_bin NOT NULL,
  `expires` int(11) unsigned NOT NULL,
  `data` text COLLATE utf8_bin,
  PRIMARY KEY (`sessionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` VALUES ('9BBybyEzz9zRojl2n6aEhBt05Sq0kuB',1610476773,'{\"ErrorList\":null,\"WarningList\":null,\"InfoList\":null,\"SuccessList\":null,\"UserId\":30}'),('BFRsCVUJNHe5JYthHUeAWGW4tSX1F2A',1610577651,'{\"ErrorList\":null,\"WarningList\":null,\"InfoList\":null,\"SuccessList\":null,\"UserId\":30}'),('MDb4w8JWPggsREVoXss40SowYrjL93X',1610491189,'{\"ErrorList\":null,\"WarningList\":null,\"InfoList\":null,\"SuccessList\":null,\"UserId\":30}'),('kpHU80CD7YcUzn7c3gUIpSLV0ExlKA',1610545740,'{\"ErrorList\":null,\"WarningList\":null,\"InfoList\":null,\"SuccessList\":null,\"UserId\":30}'),('mOA2UhLcDAfqOyl07VGIIKtq7Pilqyng',1610706205,'{\"ErrorList\":null,\"WarningList\":null,\"InfoList\":null,\"SuccessList\":null,\"UserId\":30,\"SensorReadTime\":1610573260}'),('vhYx1dqJrH0XurJEQ8SP4u9yULqxibo3',1610562217,'{\"ErrorList\":null,\"WarningList\":null,\"InfoList\":null,\"SuccessList\":null,\"UserId\":30}');
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `login` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(100) COLLATE utf8_bin NOT NULL,
  `status` enum('active','blocked') CHARACTER SET utf8 NOT NULL DEFAULT 'active',
  PRIMARY KEY (`userId`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (30,'superuser','superuser','superuser@fieldlinksys.com','DD40DD149AF4CD43FCAC82F3FE400922A2329B3E','active'),(69,'testuser','testuser','testuser@fieldlinksys.com','DD40DD149AF4CD43FCAC82F3FE400922A2329B3E','active'),(97,'Colin Doyle','flsadmin','colindoyle@fieldlinksys.com','A63BA45DA9C9C75B46C8D645CD939A1A4621F8A8','active'),(98,'Payton Alford','flsoperator','paytonalford@fieldlinksys.com','DD40DD149AF4CD43FCAC82F3FE400922A2329B3E','active');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userRole`
--

DROP TABLE IF EXISTS `userRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userRole` (
  `userId` int(11) unsigned NOT NULL,
  `roleId` int(11) unsigned NOT NULL,
  UNIQUE KEY `user_role_unique` (`userId`,`roleId`) USING BTREE,
  KEY `fk_user_role_2_idx` (`roleId`),
  CONSTRAINT `fk_userRole_1` FOREIGN KEY (`roleId`) REFERENCES `role` (`roleId`),
  CONSTRAINT `fk_userRole_2` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userRole`
--

LOCK TABLES `userRole` WRITE;
/*!40000 ALTER TABLE `userRole` DISABLE KEYS */;
INSERT INTO `userRole` VALUES (30,2),(97,2),(98,2),(69,5),(97,5);
/*!40000 ALTER TABLE `userRole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `well`
--

DROP TABLE IF EXISTS `well`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `well` (
  `wellId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `operatorId` int(11) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `api` varchar(50) COLLATE utf8_bin NOT NULL,
  `leaseName` varchar(100) CHARACTER SET utf8 NOT NULL,
  `leaseNo` varchar(50) COLLATE utf8_bin NOT NULL,
  `fieldName` varchar(100) CHARACTER SET utf8 NOT NULL,
  `closestCity` varchar(50) CHARACTER SET utf8 NOT NULL,
  `wellTypeId` int(11) unsigned NOT NULL,
  `countyId` int(11) unsigned NOT NULL,
  `latLong` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `tvd` double NOT NULL,
  `status` enum('active','passive') COLLATE utf8_bin NOT NULL DEFAULT 'active',
  `production` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`wellId`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_well_1_idx` (`wellTypeId`),
  KEY `fk_well_2_idx` (`countyId`),
  KEY `fk_well_3_idx` (`operatorId`),
  CONSTRAINT `fk_well_1` FOREIGN KEY (`wellTypeId`) REFERENCES `wellType` (`wellTypeId`),
  CONSTRAINT `fk_well_2` FOREIGN KEY (`countyId`) REFERENCES `county` (`countyId`),
  CONSTRAINT `fk_well_3` FOREIGN KEY (`operatorId`) REFERENCES `operator` (`operatorId`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `well`
--

LOCK TABLES `well` WRITE;
/*!40000 ALTER TABLE `well` DISABLE KEYS */;
INSERT INTO `well` VALUES (24,54,' Abalone Deep 1','42-245-31503','ADAMEK UNIT','3434','Field 345 test','ttt',2,19,'30.013415295253242, -96.0007632323161',0,'active',''),(25,62,'Allaru 1 ST1','42-245-31504','AFFLERBACH 01','89877','DE WITT (EAGLE FORD SHALE)','closest city name',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(26,49,'Anderdon 1','42-245-31505','ALEXANDER GAS UNIT 1','55','field name','closest city',2,1,'30.013415295253242, -96.0007632323161',0,'passive',''),(29,30,'Anderdon 1A','42-245-31506','ALEXANDER GAS UNIT 1','lease no','field name','closest city name',2,1,'30.013415295253242, -96.0007632323161',0,'active',''),(30,24,'Anderdon 1A ST1','42-245-31507','ALEXANDER 01','','','',2,1,'30.013415295253242, -96.0007632323161',0,'active',''),(31,24,'Anson 1','42-245-31508','ALEXANDER-WESSENDORFF 1 (SA) A3 A','','','',2,1,'30.013415295253242, -96.0007632323161',0,'active',''),(32,24,'Anson North 1','42-245-31509','ALEXANDER-WESSENDORFF 1 (SA) A3 A','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(33,24,'Argus 1','42-245-31510','DILWORTH GAS UNIT','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(34,24,'Argus 2 ST1','42-245-31511','DROMGOOLE \"A\" UNIT','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(35,24,'Arunta 1','42-245-31512','F. MALEK UNIT L','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(36,24,'Asterias 1','42-245-31513','ALEXANDER-WESSENDORFF 1 (SA) A5','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(37,24,'Asterias 1 ST1','42-245-31514','FAGAN','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(38,24,'Audacious 1','42-245-31515','ESTHER JACOBS GAS UNIT # 2','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(39,24,'Audacious 2','42-245-31516','DEBILL 01','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(40,24,'Audacious 3','42-245-31517','FALKS GAS UNIT 1','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(42,24,'Audacious 4','42-245-31518','DUNWOODY, CHARLES S. (PF)','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(43,24,'Augustus 1','42-245-31519','FLAME GAS UNIT ','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(44,24,'Auriga West 1','42-245-31520','ALEXANDER-WESSENDORFF 1 (SA) A3 A','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(45,24,'Bassett 1','42-245-31521','ESCONDIDO CRK-EVANGELINE (SA) B2','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(46,24,'Bassett 1A','42-245-31522','FOSTER UNIT AC','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(47,24,'Birch 1','42-245-31523','BARBOZA UNIT','67776','field name','closest city',1,20,'30.013415295253242, -96.0007632323161',0,'active',''),(48,24,'Birch 1 ST1','42-245-31524','ALEXANDER-WESSENDORFF 1 (SA) A5','8787','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(49,24,'Bodacious 1','42-245-31525','BARBOZA UNIT','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(50,24,'Bodacious 1 St1','42-245-31526','CLEMENT HEARD','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(51,24,'Bodacious 1A','42-245-31527','CRAVENS, LLC FEE','','','',2,1,'30.013415295253242, -96.0007632323161',0,'active',''),(52,24,'Brontosaurus 1','42-245-31528','FOSTER UNIT AC','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(53,24,'Brontosaurus 1 ST1','42-245-31529','ALEXANDER-WESSENDORFF 1 (SA) A5','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(54,24,'Brow Gannet 1','42-245-31530','FRANK MALEK 01','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(55,24,'Calytrix 1','42-245-31531','FRED BUCHEL GAS UNIT NO 1','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(56,24,'Calytrix 1 ST1','42-245-31532','G. J. TIPS GAS UNIT','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(57,24,'Capricious 1','42-245-31533','GAMBLE GULLY GAS UNIT #5 ','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(58,24,'Carrier 1','42-245-31534','','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(59,24,'Cash 1','42-245-31535','G KLEIN C D-WSCH A ULW A','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(60,24,'Cassini 1','42-245-31536','HAMILTON GAS UNIT NO 1 ','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(61,24,'Coversham 1','42-245-31537','CRAVENS, LLC FEE','','','',2,1,'30.013415295253242, -96.0007632323161',0,'active',''),(62,24,'Circinus 1','42-245-31538','GERHARDT & OTTILLIA BRUNS','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(63,24,'Circinus 1 ST1','42-245-31539','BARBOZA UNIT','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(64,24,'Clairault 1','42-245-31540','GARRETT','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(65,24,'Columba 1','42-245-31541','GAYLE, ALBERTA ','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(66,24,'Columba 1 ST1','42-245-31542','FOX, W. J. \"A\" ','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(67,24,'Discorbis 1','42-245-31543','BUTLER A-304-BRYSCH USW B','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(68,24,'Douglas','42-245-31544','BUNDICK ','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(69,24,'Fish River 1','42-245-31545','BARGMANN TRUST UNIT B','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(70,24,'Hadrosaurus 1','42-245-31546','BROOKING-SEALE UNIT ','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(71,24,'Jabiru 1','42-245-31547','CARMICHAEL','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(72,24,'Jaburi 10','42-245-31548','GAYLE, ALBERTA ','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(73,24,'Jaburi11','42-245-31549','JERRY HOUSE UNIT A','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(74,24,'Jaburi 12','42-245-31550','CLEMENT HEARD ','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(75,24,'Jaburi 19','42-245-31551','GERHARDT & OTTILLIA BRUNS','5656','field name test','closest city test',2,1,'30.013415295253242, -96.0007632323161',0,'active',''),(76,24,'Jaburi 14','42-245-31552','HEARD','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(77,24,'Katandra 1 ST1','42-245-31553','CRAVENS, LLC FEE','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(78,24,'Kimberley 1','42-245-31554','BARGMANN TRUST UNIT B','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(79,24,'Kingtree 1','42-245-31555','CHUMCHAL UNIT','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(80,24,'Laminaria 1','42-245-31556','CLEMENT HEARD','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(81,24,'Lminaria 2','42-245-31557','CARMICHAEL','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(82,24,'Laminaria Pilot South 1','42-245-31558','HARVEY RENGER GAS UNIT','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(83,24,'Mount Ashmore 1B ST1','42-245-31559','BARGMANN TRUST UNIT B','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(84,24,'Octavius 1 ST2','42-245-31560','CLEMENT HEARD','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(85,24,'Octavius 1 ST1','42-245-31561','CARMICHAEL','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(86,24,'Octavius 1 ST3','42-245-31562','CRAVENS, LLC FEE','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(87,24,'Octavius 1 ST4','42-245-31563','CHILDREN\'S-WESTON UNIT AC ','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(88,24,'Octavius 2','42-245-31564','CHUMCHAL UNIT','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(89,24,'Padthaway 1','42-245-31565','DAVILA-GRAHAM UNIT','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(90,24,'Pengana 1','42-245-31566','GARRETT','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(91,24,'Saucepan 1','42-245-31567','BARBOZA UNIT','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(92,24,'Sebring 1','42-245-31568','CHILDREN\'S-WESTON UNIT AC','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(93,24,'Yering 1','42-245-31569','CHUMCHAL UNIT','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(94,24,'Yering 2','42-245-31570','DEBILL 01','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(95,24,'Vulcan 1','42-245-31571','BARGMANN TRUST UNIT B','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(96,24,'Vulcan 1A','42-245-31572','GARRETT','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(97,24,'Vulcan 1B','42-245-31573','CRAVENS, LLC FEE','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(98,24,'Warb 1','42-245-31574','BARGMANN TRUST UNIT B','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(99,24,'Warb 1A','42-245-31575','CARMICHAEL','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(100,24,'Taltarni 1','42-245-31576','BARGMANN TRUST UNIT B','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(101,24,'Tancred 1','42-245-31577','GERHARDT & OTTILLIA BRUNS','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(102,24,'Snipe 1','42-245-31578','HAMILTON GAS UNIT NO 1','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(103,24,'Snipe 1 ST1','42-245-31579','HANCOCK, EVA RUTH ','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(104,24,'Snowmass 1','42-245-31580','HEARD, FANNIE V.W. ','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(105,24,'Spruce 1','42-245-31581','HOUDMANN-BUXKAMPER GAS UNIT ','','','',1,1,'30.013415295253242, -96.0007632323161',0,'active',''),(106,29,'test name','test api','test lease name','test lease no','test field name','test colosest city',1,16,'30.013415295253242, -96.0007632323161',0,'active',''),(109,56,'name','api','lease name','lease no','field name','closest city',1,21,'30.013415295253242, -96.0007632323161',0,'passive',''),(113,29,'name66','api','leas name','lease no','field name','closest city',1,21,'30.013415295253242, -96.0007632323161',0,'passive',''),(115,33,'name777','api','lease name','lease no','field name','closest city',1,16,'30.013415295253242, -96.0007632323161',0,'active',''),(116,33,'name222','api','lease name','lease no','field name','closest city',1,18,'',6.23,'active',''),(117,29,'name227','api','lease name','lease no','field name','closest city',1,19,'',0,'active','5 BOPD (est)');
/*!40000 ALTER TABLE `well` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wellType`
--

DROP TABLE IF EXISTS `wellType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wellType` (
  `wellTypeId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`wellTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wellType`
--

LOCK TABLES `wellType` WRITE;
/*!40000 ALTER TABLE `wellType` DISABLE KEYS */;
INSERT INTO `wellType` VALUES (1,'Oil'),(2,'Gas'),(3,'Oil&Gas');
/*!40000 ALTER TABLE `wellType` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-28 16:36:59
