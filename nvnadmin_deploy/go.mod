module deploy

go 1.14

require (
	github.com/wingedpig/loom v0.0.0-20150915155823-16fa1dc3c32a
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
)
