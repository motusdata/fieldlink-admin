package main

import (
	"fmt"
	"os"

	"deploy/src/loom"
)

var config = loom.Config{
	User:     "progfargo",
	Password: "Mali12Blue",
	Host:     "138.197.185.183",

	DisplayOutput: true,
	AbortOnError:  false,
}

var appName = "nvnadmin"
var remoteHome = "/home/progfargo/nvnadmin"
var localHome = "/home/erdem/project/nvn/nvnadmin/nvnadmin"

func main() {

	os.Chdir(localHome)

	//stop process
	fmt.Println("\nstopping process.")
	config.Sudo(fmt.Sprintf("systemctl stop %s.service", appName))

	//asset
	fmt.Println("\ncreating asset.tar.gz")
	config.Local("tar -cvzf asset.tar.gz ./asset")

	fmt.Println("\ndeleting remote asset folder")
	config.Run(fmt.Sprintf("rm -rf %s/asset", remoteHome))

	fmt.Println("\ncopy asset.tar.gz to remote")
	config.Put("asset.tar.gz", remoteHome)

	fmt.Println("\nremoving local asset.tar.gz")
	config.Local("rm asset.tar.gz")

	fmt.Println("\ninflating remote assets")
	config.Run(fmt.Sprintf("cd %s; tar xvzf asset.tar.gz; rm asset.tar.gz", remoteHome))

	//view
	fmt.Println("\ncreating view.tar.gz")
	config.Local("tar -cvzf view.tar.gz ./view")

	fmt.Println("\ndeleting remote view folder")
	config.Run(fmt.Sprintf("rm -rf %s/view", remoteHome))

	fmt.Println("\ncopy view.tar.gz to remote")
	config.Put("view.tar.gz", remoteHome)

	fmt.Println("\nremoving local view.tar.gz")
	config.Local("rm view.tar.gz")

	fmt.Println("\ninflating view assets")
	config.Run(fmt.Sprintf("cd %s; tar xvzf view.tar.gz; rm view.tar.gz", remoteHome))

	//executable
	fmt.Println(fmt.Sprintf("\ncopy %s to remote", appName))
	config.Put(appName, remoteHome)

	//start process
	fmt.Println("\nstarting process.")
	config.Sudo(fmt.Sprintf("systemctl start %s.service", appName))

}
